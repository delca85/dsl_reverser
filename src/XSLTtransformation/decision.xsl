<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	decision.xsl

	Adds @Decision annotation wrapping each if body and add an @IfEval annotation wraps if condition too.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- @Decision -->

<!-- adds @Decision annotation around if block in method's impl (not closing if the block is inside an else if body with else branch too ) -->
<xsl:template name="addDecisionAnnotationIf" match="src:function//src:if">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Decision</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-if', count(preceding::src:if) + count(ancestor::src:if))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:elseif/following-sibling::*[1][self::src:else])">
<xsl:text>}//decision closed (if)&#10;</xsl:text></xsl:if>
</xsl:template>

<!-- closing @Decision annotation around if block in method's impl if the block is inside an else if body with else branch too -->
<xsl:template name="addDecisionAnnotationElseIfWithElseElseBranch" match="src:function//src:else[(preceding-sibling::*[1][self::src:elseif])]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}//decision closed (elseif with else)&#10;</xsl:text> 
</xsl:template>

<!-- adds @Decision annotation around if block in method's impl (not closing if the block is inside an else if body with else branch too ) -->
<xsl:template name="addDecisionAnnotationIfCons" match="src:constructor//src:if">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Decision</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-if', count(preceding::src:if) + count(ancestor::src:if))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:elseif/following-sibling::*[1][self::src:else])">
<xsl:text>}//decision closed (if)&#10;</xsl:text></xsl:if>
</xsl:template>

<!-- closing @Decision annotation around if block in method's impl if the block is inside an else if body with else branch too -->
<xsl:template name="addDecisionAnnotationElseIfWithElseElseBranchCons" match="src:constructor//src:else[(preceding-sibling::*[1][self::src:elseif])]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}//decision closed (elseif with else)&#10;</xsl:text> 
</xsl:template>

<!-- add @ifEval annotation -->
<xsl:template name="addIfEval" match="src:if/src:condition">
<xsl:text>(</xsl:text><annotation>@<name>IfEval</name></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>})</xsl:text>
</xsl:template>


<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>