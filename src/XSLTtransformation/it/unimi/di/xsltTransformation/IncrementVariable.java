package it.unimi.di.xsltTransformation;

public class IncrementVariable{
	private static Integer currentValue = new Integer(0);

	public static Integer nextValue(){
		return ++currentValue;
	}
}