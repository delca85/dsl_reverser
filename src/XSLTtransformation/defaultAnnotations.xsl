<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	defaultAnnotations.xsl

	Adds all the Reverser annotations used for ActivityDiagram at each element in the code.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- @ActivityDiagram -->

<!-- add @ActivityDiagram annotation before the first child of block element -->
<xsl:template name="addActivityAnnotation" match="src:function/src:block/*[1]">   
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:function/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:function/src:type"/><xsl:text>[</xsl:text><xsl:value-of select="ancestor::src:function/src:parameter_list/src:parameter"/><xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- add "}" after the last child of block element -->
<xsl:template match="src:function/src:block[last()]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>


<!-- add "@Return" before return statement -->
<xsl:template name="addReturn" match="src:return">  
<annotation>@<name>Return</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:function/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:function/src:type"/><xsl:text>[</xsl:text><xsl:value-of select="ancestor::src:function/src:parameter_list/src:parameter"/><xsl:text>].return</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- @CallAction -->

<!-- adds @CallAction annotation around all declaration statements but not the ones within loop -->
<xsl:template name="declStatement" match="src:function//src:decl_stmt[src:decl[src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
 <annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init', count(preceding::src:decl[src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all method call and assignment statement-->
<xsl:template name="methodCall" match="src:function//src:expr_stmt">   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-expr_stmt', count(preceding::src:expr_stmt))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all method calls that are among another method's arguments -->
<xsl:template name="methodCallAsMethodArgs" match="src:argument/src:expr/src:call">   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-call', count(preceding::src:call))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all method calls that are among another method's arguments (except within an annotation) -->
<xsl:template name="operatorAsMethodArgs" match="src:argument/src:expr[src:name][src:operator]">  
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:annotation)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-operator', count(preceding::src:expr[src:operator]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:annotation)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- added to avoid @CallAnnotation application for simply strings concatenation -->
<xsl:template  match="src:argument/src:expr[src:name][src:operator][src:literal]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- @Decision -->
<!-- adds @Decision annotation -->
<xsl:template name="addDecisionAnnotation" match="src:if">
<annotation>@<name>Decision</name></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- add @ifEval annotation -->
<xsl:template name="addIfEval" match="src:if/src:condition">
<xsl:text>(</xsl:text><annotation>@<name>IfEval</name></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>})</xsl:text>
</xsl:template>

<!-- @Loop -->
<!-- adds @While annotation for while loop -->
<xsl:template name="addWhileAnnotation" match="src:while">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @Loop annotation -->
<xsl:template name="addForAnnotation" match="src:for">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>