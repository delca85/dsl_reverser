<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	import.xsl

	Adds import statement in order to be able to use reverser annotations.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- add import at the beginning of the file -->
<xsl:template name="addImportAnnotation" match="src:import[1]">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<xsl:template name="addImportAnnotationNoImportInFile" match="src:class">
<xsl:if test="not(preceding::src:import)">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>


</xsl:stylesheet>