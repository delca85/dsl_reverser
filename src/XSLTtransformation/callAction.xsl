<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	callaction.xsl

	Adds @CallAction annotation at each statement excluding ones within main body

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- adds @CallAction annotation around all declaration statements in a method's impl but not the ones within loop -->
<xsl:template name="declStatement" match="src:function//src:decl_stmt[src:decl[src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
 <annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init', count(preceding::src:decl[src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a method's impl (different array declaration) -->
<xsl:template name="declStatementArraySecondWay" match="src:function//src:decl_stmt[src:decl[src:type/src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array1-', count(preceding::src:decl[src:type/src:name/src:index][src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all declaration statements in a constructor's impl but not the ones within loop -->
<xsl:template name="declStatementConstructor" match="src:constructor//src:decl_stmt[src:decl[src:init]]" >
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init', count(preceding::src:decl[src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a constructor's impl -->
<xsl:template name="declStatementArrayConstructor" match="src:constructor//src:decl_stmt[src:decl[src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array-', count(preceding::src:decl[src:name/src:index]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a constructor's impl (different array declaration) -->
<xsl:template name="declStatementArraySecondWayConstructor" match="src:constructor//src:decl_stmt[src:decl[src:type/src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array1-', count(preceding::src:decl[src:type/src:name/src:index][src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all declaration statements in a constructor's impl but not the ones within loop -->
<xsl:template name="declStatementConstructor" match="src:constructor//src:decl_stmt[src:decl[src:init]]">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init', count(preceding::src:decl[src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all method call and assignment statement in method's impl-->
<xsl:template name="methodCall" match="src:function//src:expr_stmt">   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:catch)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-expr_stmt', count(preceding::src:expr_stmt))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:catch)"><xsl:text>}</xsl:text></xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all method call and assignment statement in constructor's impl (except around super constructor invocation) and adds @ActivityDiagram after super constructor invocation -->
<xsl:template name="methodCallConstructor" match="src:constructor//src:expr_stmt">   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:choose>
<xsl:when test="not(ancestor::src:catch) and not(descendant::src:expr[src:call[src:name[.='super']]])">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-expr_stmt', count(preceding::src:expr_stmt))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="(descendant::src:expr[src:call[src:name[.='super']]])">
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:constructor/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:constructor/src:type"/><xsl:text>[</xsl:text><xsl:value-of select="ancestor::src:constructor/src:parameter_list/src:parameter"/><xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="not(ancestor::src:catch) and not(descendant::src:expr[src:call[src:name[.='super']]])"><xsl:text>}</xsl:text></xsl:if>
</xsl:template>


<!-- adds @CallAction annotation around all method calls that are among another method's arguments -->
<!-- <xsl:template name="methodCallAsMethodArgs" match="src:argument/src:expr/src:call"> 
<xsl:if test="not(preceding::src:operator[.='new'] or ancestor::src:catch)">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-call', count(preceding::src:call))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(preceding::src:operator[.='new'] or ancestor::src:catch)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template> -->

<!-- adds @CallAction annotation around all method calls that are among another method's arguments (except within an annotation) -->
<!-- <xsl:template name="operatorAsMethodArgs" match="src:argument/src:expr[src:name][src:operator]">  
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:annotation or ancestor::src:catch)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-operator', count(preceding::src:expr[src:operator]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:annotation or ancestor::src:catch)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template> -->

<!-- added to avoid @CallAnnotation application for simply strings concatenation -->
<xsl:template match="src:argument/src:expr[src:name][src:operator][src:literal]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- added to avoid @CallAnnotation application inside catch blocks -->
<xsl:template match="src:catch/*">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>


<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>