<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	defaultAnnotations.xsl

	Adds all the Reverser annotations used for ActivityDiagram at each element in the code.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- add import at the beginning of the file -->

<xsl:template name="addImportAnnotation" match="src:import[1]">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<xsl:template name="addImportAnnotationNoImportInFile" match="src:class">
<xsl:if test="not(preceding::src:import)">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- @ActivityDiagram -->

<!-- add @ActivityDiagram annotation before a method's implementation -->
<xsl:template name="addActivityAnnotation" match="src:function">
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:type"/><xsl:text>[</xsl:text><xsl:value-of select="child::src:parameter_list/src:parameter"/><xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- add @ActivityDiagram annotation before a constructor's implementation -->
<!-- <xsl:template name="addActivityAnnotationConstructor" match="src:constructor">   
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:name"/><xsl:text>.[</xsl:text><xsl:value-of select="child::src:parameter_list//src:type/src:name"/><xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template> -->

<!-- @CallAction -->

<!-- adds @CallAction annotation around all declaration statements in a method's impl but not the ones within loop -->
<xsl:template name="declStatement" match="src:function//src:decl_stmt[src:decl[src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
 <annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init', count(preceding::src:decl[src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a method's impl -->
<xsl:template name="declStatementArray" match="src:function//src:decl_stmt[src:decl[src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array-', count(preceding::src:decl[src:name/src:index]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a method's impl (different array declaration) -->
<xsl:template name="declStatementArraySecondWay" match="src:function//src:decl_stmt[src:decl[src:type/src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array1-', count(preceding::src:decl[src:type/src:name/src:index][src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all declaration statements in a constructor's impl but not the ones within loop -->
<xsl:template name="declStatementConstructor" match="src:constructor//src:decl_stmt[src:decl[src:init]]" >
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init', count(preceding::src:decl[src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:while or ancestor::src:for)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a constructor's impl -->
<xsl:template name="declStatementArrayConstructor" match="src:constructor//src:decl_stmt[src:decl[src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array-', count(preceding::src:decl[src:name/src:index]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a constructor's impl (different array declaration) -->
<xsl:template name="declStatementArraySecondWayConstructor" match="src:constructor//src:decl_stmt[src:decl[src:type/src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array1-', count(preceding::src:decl[src:type/src:name/src:index][src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @CallAction annotation around all method call and assignment statement in method's impl-->
<xsl:template name="methodCall" match="src:function//src:expr_stmt">   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:catch)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-expr_stmt', count(preceding::src:expr_stmt))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:catch)"><xsl:text>}</xsl:text></xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all method call and assignment statement in constructor's impl (except around super constructor invocation) -->
<xsl:template name="methodCallConstructor" match="src:constructor//src:expr_stmt">   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:choose>
<xsl:when test="not(ancestor::src:catch) and not(descendant::src:expr[src:call[src:name[.='super']]])">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-expr_stmt', count(preceding::src:expr_stmt))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="not(ancestor::src:catch) and not(descendant::src:expr[src:call[src:name[.='super']]])"><xsl:text>}</xsl:text></xsl:if>
</xsl:template>

<!-- adds @CallAction annotation around all method calls that are among another method's arguments -->
<!-- <xsl:template name="methodCallAsMethodArgs" match="src:argument/src:expr/src:call"> 
<xsl:if test="not(preceding::src:operator[.='new'] or ancestor::src:catch)">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-call', count(preceding::src:call))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(preceding::src:operator[.='new'] or ancestor::src:catch)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template> -->

<!-- adds @CallAction annotation around all method calls that are among another method's arguments (except within an annotation) -->
<!-- <xsl:template name="operatorAsMethodArgs" match="src:argument/src:expr[src:name][src:operator]">  
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:if test="not(ancestor::src:annotation or ancestor::src:catch)">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-operator', count(preceding::src:expr[src:operator]))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:annotation or ancestor::src:catch)">
<xsl:text>}</xsl:text>
</xsl:if>
</xsl:template> -->

<!-- added to avoid @CallAnnotation application for simply strings concatenation -->
<xsl:template match="src:argument/src:expr[src:name][src:operator][src:literal]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- added to avoid @CallAnnotation application inside catch blocks -->
<xsl:template match="src:catch/*">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- @Decision -->

<!-- adds @Decision annotation around if block in method's impl (not closing if the block is inside an else if body with else branch too ) -->
<xsl:template name="addDecisionAnnotationIf" match="src:function//src:if">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Decision</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-if', count(preceding::src:if) + count(ancestor::src:if))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:elseif/following-sibling::*[1][self::src:else])">
<xsl:text>}//decision closed (if)&#10;</xsl:text></xsl:if>
</xsl:template>

<!-- closing @Decision annotation around if block in method's impl if the block is inside an else if body with else branch too -->
<xsl:template name="addDecisionAnnotationElseIfWithElseElseBranch" match="src:function//src:else[(preceding-sibling::*[1][self::src:elseif])]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}//decision closed (elseif with else)&#10;</xsl:text> 
</xsl:template>

<!-- adds @Decision annotation around if block in method's impl (not closing if the block is inside an else if body with else branch too ) -->
<xsl:template name="addDecisionAnnotationIfCons" match="src:constructor//src:if">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Decision</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-if', count(preceding::src:if) + count(ancestor::src:if))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:if test="not(ancestor::src:elseif/following-sibling::*[1][self::src:else])">
<xsl:text>}//decision closed (if)&#10;</xsl:text></xsl:if>
</xsl:template>

<!-- closing @Decision annotation around if block in method's impl if the block is inside an else if body with else branch too -->
<xsl:template name="addDecisionAnnotationElseIfWithElseElseBranchCons" match="src:constructor//src:else[(preceding-sibling::*[1][self::src:elseif])]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}//decision closed (elseif with else)&#10;</xsl:text> 
</xsl:template>

<!-- add @ifEval annotation -->
<xsl:template name="addIfEval" match="src:if/src:condition">
<xsl:text>(</xsl:text><annotation>@<name>IfEval</name></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<xsl:text>})</xsl:text>
</xsl:template>

<!-- @Loop -->
<!-- adds @While annotation for while loop in method's impl -->
<xsl:template name="addWhileAnnotation" match="src:function//src:while">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @While annotation for while loop in constructor's impl -->
<xsl:template name="addWhileAnnotationConstructor" match="src:constructor//src:while">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @Loop annotation in method's impl -->
<xsl:template name="addForAnnotation" match="src:function//src:for">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @Loop annotation in constructor's impl -->
<xsl:template name="addForAnnotationConstructor" match="src:constructor//src:for">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>


<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>