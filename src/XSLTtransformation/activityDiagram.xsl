<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	activityTransform.xsl

	Adds @ActivityDiagram annotation wrapping each method body.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- add import at the beginning of the file -->

<xsl:template name="addImportAnnotation" match="src:import[1]">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<xsl:template name="addImportAnnotationNoImportInFile" match="src:class">
<xsl:if test="not(preceding::src:import)">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- @ActivityDiagram -->

<!-- add @ActivityDiagram annotation before a method's implementation -->
<xsl:template name="addActivityAnnotation" match="src:function">
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:type"/><xsl:text>[</xsl:text><xsl:value-of select="child::src:parameter_list/src:parameter"/><xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- add @ActivityDiagram annotation before a constructor's implementation -->
<!-- <xsl:template name="addActivityAnnotationConstructor" match="src:constructor">   
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:name"/><xsl:text>.[</xsl:text><xsl:value-of select="child::src:parameter_list//src:type/src:name"/><xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template> -->


<!-- add "@Return" around return expr, different if return is empty or not -->
<!-- <xsl:template name="addReturnVoid" match="src:return"> 
<xsl:choose>
<xsl:when test="not(descendant::src:expr)">
<xsl:text>return </xsl:text>
<annotation>@<name>Return</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:constructor/src:name"/><xsl:text>[</xsl:text><xsl:value-of select="ancestor::src:constructor/src:parameter_list/src:parameter"/><xsl:text>].return</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{}</xsl:text>
<xsl:text>;</xsl:text>
</xsl:when>
<xsl:otherwise>
<xsl:variable name="expr" select="(descendant::src:expr)" />
<xsl:text>return </xsl:text>
<annotation>@<name>Return</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:constructor/src:name"/><xsl:text>[</xsl:text><xsl:value-of select="ancestor::src:constructor/src:parameter_list/src:parameter"/><xsl:text>].return</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:value-of select="concat($expr, '};')"/>
</xsl:otherwise>
</xsl:choose>
</xsl:template> -->

<!-- adds @CallAction annotation around all method call and assignment statement in constructor's impl (except around super constructor invocation) -->
<xsl:template name="methodCallConstructor" match="src:constructor//src:expr_stmt">   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:choose>
<xsl:when test="not(ancestor::src:catch) and not(descendant::src:expr[src:call[src:name[.='super']]])">
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-expr_stmt', count(preceding::src:expr_stmt))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:otherwise>
</xsl:choose>
<xsl:if test="not(ancestor::src:catch) and not(descendant::src:expr[src:call[src:name[.='super']]])"><xsl:text>}</xsl:text></xsl:if>
</xsl:template>


<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>