<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	loop.xsl

	Adds @Loop annotation wrapping each while and for body.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" standalone="yes"/>

<!-- adds @While annotation for while loop in method's impl -->
<xsl:template name="addWhileAnnotation" match="src:function//src:while">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @While annotation for while loop in constructor's impl -->
<xsl:template name="addWhileAnnotationConstructor" match="src:constructor//src:while">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @Loop annotation in method's impl -->
<xsl:template name="addForAnnotation" match="src:function//src:for">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>

<!-- adds @Loop annotation in constructor's impl -->
<xsl:template name="addForAnnotationConstructor" match="src:constructor//src:for">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:template>
<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>