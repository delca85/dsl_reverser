<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="2.0">
<!--
	activityTransform.xsl

	Add @CallAction annotation at each statement excluding ones within main body

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- add annotation  -->
<xsl:template name="declStatement" match="src:function//src:decl[src:init]" xmlns:incVar="java:it.unimi.di.xsltTransformation.IncrementVariable">   <!-- add annotation around all declaration statements -->
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:choose>
<xsl:when test="$ancestorName = 'main'">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call</xsl:text><xsl:value-of select="incVar:nextValue()"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="methodCall" match="src:function//src:expr_stmt" xmlns:incVar="java:it.unimi.di.xsltTransformation.IncrementVariable">   <!-- add annotation around all method call and assignment statement-->
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:choose>
<xsl:when test="$ancestorName = 'main'">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call</xsl:text><xsl:value-of select="incVar:nextValue()"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="methodCallAsMethodArgs" match="src:argument/src:expr/src:call" xmlns:incVar="java:it.unimi.di.xsltTransformation.IncrementVariable">   <!-- add annotation around all method calls that are among another method's arguments -->
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:choose>
<xsl:when test="$ancestorName = 'main'">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call</xsl:text><xsl:value-of select="incVar:nextValue()"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template name="operatorAsMethodArgs" match="src:argument/src:expr[src:operator]" xmlns:incVar="java:it.unimi.di.xsltTransformation.IncrementVariable">   <!-- add annotation around all method calls that are among another method's arguments -->
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:choose>
<xsl:when test="$ancestorName = 'main'">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:when>
<xsl:otherwise>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call</xsl:text><xsl:value-of select="incVar:nextValue()"/>&quot;</literal></expr></argument>)</argument_list></annotation><xsl:text>{</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy><xsl:text>}</xsl:text>
</xsl:otherwise>
</xsl:choose>
</xsl:template>



<!-- identity copy -->
<xsl:template match="@*|node()"><xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy></xsl:template>

</xsl:stylesheet>