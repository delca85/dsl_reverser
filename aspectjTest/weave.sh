#!/bin/bash
cd src/
CLASSPATH=$CLASSPATH:.

# BCEL=/home/reverser_guest/sharedFolder/sf_TesiM/DSL/atjavaTest/lib/bcel-5.2.jar
ASPECTJ=/home/reverser_guest/sharedFolder/sf_TesiM/DSL/aspectjTest/lib/aspectjrt-1.7.3.jar:/home/reverser_guest/sharedFolder/sf_TesiM/DSL/aspectjTest/lib/aspectjtools-1.7.3.jar

#BOOTCP=.:/home/reverser_guest/sharedFolder/sf_TesiM/DSL/atjavaTest/lib/commons-logging-1.1.2.jar:/home/reverser_guest/sharedFolder/sf_TesiM/DSL/atjavaTest/lib/commons-configuration-1.9.jar:/home/reverser_guest/sharedFolder/sf_TesiM/DSL/atjavaTest/lib/commons-lang-2.6.jar:..:/home/reverser_guest/sharedFolder/sf_TesiM/DSL/atjavaTest/ataspectj/build/classes:$ASPECTJ

CLASSPATH=$CLASSPATH:$ASPECTJ

# Compile the sources
echo "Compiling..."
java -cp $CLASSPATH org.aspectj.tools.ajc.Main -source 1.5 $*
