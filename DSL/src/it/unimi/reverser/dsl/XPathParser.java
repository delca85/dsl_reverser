package it.unimi.reverser.dsl;

import it.unimi.reverser.dsl.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com on 08/03/17.
 * this class receives a java code portion and transform it in an xpath expression.
 */
public class XPathParser {
    private String xpathTransformation;
    private String xpathTransformationDir;
    private DSLParser dslParser;
    private Map<String, Integer> pointcutTagSibling;

    public XPathParser(DSLParser dslParser) {
        this.dslParser = dslParser;
        this.xpathTransformationDir = dslParser.getUserTransformationDir();
        this.xpathTransformation = dslParser.getUserTransformationDir() + "/xpath.xsl";
        pointcutTagSibling = new HashMap();
        writeXpathFile();
    }

    public void singleStatement(String pointcut, String tag1) {
        String[] matchSequence1;
        matchSequence1 = getXSLTFileContent(pointcut, tag1);
        createAndApplySingleStatement(pointcut, tag1, matchSequence1);
    }

    public void strictSequence(String pointcut, String tag1, String tag2) {
        String[] matchSequence1, matchSequence2;
        int siblingCounter = 0;;
        matchSequence1 = getXSLTFileContent(pointcut, tag1);
        matchSequence2 = getXSLTFileContent(pointcut, tag2);
        Pattern siblingPat = Pattern.compile("\\]\\[following-sibling::\\*\\[(\\d+)\\]\\[");
        Matcher siblingMatcher = siblingPat.matcher(Util.removeApexesSibling(matchSequence1[0]));
        while (siblingMatcher.find())
            siblingCounter = Integer.parseInt(siblingMatcher.group(1));
        createAndApplyStrictSequence(pointcut, tag1, tag2, matchSequence1, matchSequence2, siblingCounter +1);
    }

    public void looseSequence(String pointcut, String tag1, String tag2) {
        String[] matchSequence1, matchSequence2;
        matchSequence1 = getXSLTFileContent(pointcut, tag1);
        matchSequence2 = getXSLTFileContent(pointcut, tag2);
        createAndApplyLooseSequence(pointcut, tag1, tag2, matchSequence1, matchSequence2);
    }

    public void ifBlock(String pointcut, String conditionTag, String thenBlockTag, String elseBlockTag) {
        String matchSequence1;
        String[] matchSequence2, matchSequence3;
        matchSequence1 = getXSLTFileContent(pointcut, conditionTag)[0];
        if (matchSequence1.startsWith("src:expr_stmt"))
            matchSequence1 = matchSequence1.substring("src:expr_stmt[".length()).replace("][src:expr", "]")
                    .replaceAll("(\\]+)\\]", "$1");
        matchSequence1 = "src:if[src:condition[" + matchSequence1 + "]]";
        matchSequence2 = (thenBlockTag.equals("_"))? null : getXSLTFileContent(pointcut, thenBlockTag);
        matchSequence3 = (elseBlockTag.equals("_"))? null : getXSLTFileContent(pointcut, elseBlockTag);
        createAndApplyIfBlock(pointcut, conditionTag, thenBlockTag, elseBlockTag, matchSequence1, matchSequence2,
                matchSequence3);
    }

    public void ifBlockLengthOne(String pointcut, String conditionTag, String thenBlockTag, String elseBlockTag) {
        String matchSequence1;
        String[] matchSequence2, matchSequence3;
        matchSequence1 = getXSLTFileContent(pointcut, conditionTag)[0];
        if (matchSequence1.startsWith("src:expr_stmt"))
            matchSequence1 = matchSequence1.substring("src:expr_stmt[".length()).replace("][src:expr", "]")
                    .replaceAll("(\\]+)\\]", "$1");
        matchSequence1 = "src:if[src:condition[" + matchSequence1 + "]]";
        matchSequence2 = (thenBlockTag.equals("_"))? null : getXSLTFileContent(pointcut, thenBlockTag);
        matchSequence3 = (elseBlockTag.equals("_"))? null : getXSLTFileContent(pointcut, elseBlockTag);
        createAndApplyIfBlockLengthOne(pointcut, conditionTag, thenBlockTag, elseBlockTag, matchSequence1, matchSequence2,
                matchSequence3);
    }


    public void loopBlock(String pointcut, String conditionTag, String blockTag) {
        String matchSequence1;
        String[] matchSequence2;
        matchSequence1 = getXSLTFileContent(pointcut, conditionTag)[0];
        matchSequence2 = (blockTag.equals("_")) ? null : getXSLTFileContent(pointcut, blockTag);
        createAndApplyLoopBlock(pointcut, conditionTag, blockTag, matchSequence1, matchSequence2);
    }

    public void loopBlockLengthOne(String pointcut, String conditionTag, String blockTag) {
        String matchSequence1;
        String[] matchSequence2;
        matchSequence1 = getXSLTFileContent(pointcut, conditionTag)[0];
        matchSequence2 = (blockTag.equals("_")) ? null : getXSLTFileContent(pointcut, blockTag);
        createAndApplyLoopBlockLengthOne(pointcut, conditionTag, blockTag, matchSequence1, matchSequence2);
    }

    public DSLParser getDslParser() {
        return dslParser;
    }

    private String fromJavaToXpath(String pointcut, String tagName) {
        Process p;
        String filename = xpathTransformationDir + "/" + pointcut + "_" + tagName;
        Util.writeFile("XPathParser", filename + ".java",
                dslParser.getPortionCode(tagName) + "\n");
        try{
            p = Runtime.getRuntime().exec("srcml " + filename + ".java -o " +
                    filename + ".xml");
            Util.printOutputAndError("srcml", p);
            System.out.println("[XPathParser]Created file " + filename + ".xml");
        } catch (IOException e) {
            System.out.println("[XPathParser]Error in srcml from java to xml transformation of file "
                    + filename + ".java");
            e.printStackTrace();
        }try{
            p = Runtime.getRuntime().exec("srcml --xslt " + xpathTransformation + " " +
                    filename + ".xml -o " +
                    filename + "_xpath.xml");
            Util.printOutputAndError("srcml", p);
        } catch (IOException e) {
            System.out.println("[XPathParser]Error in srcml applying xpath.xsl transformation to " + filename + ".xml file");
            e.printStackTrace();
        }
        return filename + "_xpath.xml";
    }

    private String[] parseXPathFile(File file){
        String content=null;
        Stream<String> lines = Util.readFile("XPathParser", file.getAbsolutePath());
        for (String line : lines.filter(s->!s.startsWith("/unit[@")).collect(Collectors.toList()))
            content += line + "\n";
        return parseContent(content);
    }

    private String[] parseContent(String content){
        String xpathString = content.trim().substring(content.indexOf("/")).replaceAll("('[^']*')|[\t\\(\\),\\{\\}]+","$1");
        xpathString = xpathString.replaceAll("(/if/|'if'|/else/|'else'|/while/|'while'|(\"[^\"]+\"))|(if|else|while)\\s", "$1")
                .replace(";\n", ";");
        String xpathNode, oldRoot, typeKey;
        String[] matchAndRoot, xpathNodeAndSibling;
        xpathNode = parseOneNode(xpathString.split(";")[0])[0];
        oldRoot = parseOneNode(xpathString.split(";")[0])[1];
        xpathNodeAndSibling = new String[3];
        xpathNodeAndSibling[1] = null;
        int siblingCounter = 0, index;
        Map<String, Integer> indexType = new HashMap();
        if (xpathString.split(";").length <= 1)
            xpathNodeAndSibling[0] = xpathNode.replaceAll("\\[(src:)([0-9]+)\\]", "").replace("]=[", "][").
                    replace("[text()='&quot;[src:", "[text()='&quot;[");
        else {
            for (int i = 1; i < xpathString.split(";").length; i++) {
                if (!xpathString.split(";")[i].trim().equals("")) {
                    matchAndRoot = parseOneNode(xpathString.split(";")[i]);
                    typeKey = matchAndRoot[0].substring(0, matchAndRoot[0].indexOf('['));
                    if (!indexType.keySet().contains(typeKey))
                        indexType.put(typeKey, 1);
                    index = indexType.get(typeKey);
                    // same root, so is not a sibling but a child
                    if (oldRoot.equals(matchAndRoot[1]) && !(matchAndRoot[1].contains("expr_stmt") || matchAndRoot[1].contains("decl_stmt"))) {
                        if (siblingCounter == 0)                                        // root
                            xpathNode += StringUtils.removeFirst(matchAndRoot[0], Pattern.quote(matchAndRoot[1]));
                        else {
                            xpathNode += "[following-sibling::" + typeKey
                                    + "[" + index + "][self::" + matchAndRoot[0] + "]]";
                        }
                    } else {
                        xpathNode += "[following-sibling::" + typeKey
                                + "[" + index + "][self::" + matchAndRoot[0] + "]]";
                        indexType.put(typeKey, index++);
                        oldRoot = matchAndRoot[1];
                    }
                }
            }
            // remove index inside xpath element (in order to maintain them, replace with "\\[$2\\]”
            xpathNode = xpathNode.replace("]=[", "][").
                    replace("[text()='&quot;[src:", "[text()='&quot;[");
            xpathNodeAndSibling[0] = xpathNode;
            xpathNode = parseOneNode(xpathString.split(";")[xpathString.split(";").length-1])[0];
            oldRoot = parseOneNode(xpathString.split(";")[xpathString.split(";").length-1])[1];
            siblingCounter = 0;
            indexType = new HashMap();
            for (int i = xpathString.split(";").length-2; i >= 0; i--) {
                if (!xpathString.split(";")[i].trim().equals("")) {
                    matchAndRoot = parseOneNode(xpathString.split(";")[i]);
                    typeKey = matchAndRoot[0].substring(0, matchAndRoot[0].indexOf('['));
                    if (!indexType.keySet().contains(typeKey))
                        indexType.put(typeKey, 1);
                    index = indexType.get(typeKey);
                    // same root, so sibling counter is not to be incremented
                    if (oldRoot.equals(matchAndRoot[1]) && !(matchAndRoot[1].contains("expr_stmt") || matchAndRoot[1].contains("decl_stmt"))) {
                        if (siblingCounter == 0)                                        // root
                            xpathNode += StringUtils.removeFirst(matchAndRoot[0], Pattern.quote(matchAndRoot[1]));
                        else
                            xpathNode += "[preceding-sibling::" + typeKey
                                    + "[" + index + "][self::" + matchAndRoot[0] + "]]";
                    } else {
                        siblingCounter++;
                        xpathNode += "[preceding-sibling::" + typeKey
                                + "[" + index + "][self::" + matchAndRoot[0] + "]]";
                        indexType.put(typeKey, index++);
                        oldRoot = matchAndRoot[1];
                    }
                }
            }
            // remove index inside xpath element (in order to maintain them, replace with "\\[$2\\]”
            xpathNode = xpathNode.replace("]=[", "][").
                    replace("[text()='&quot;[src:", "[text()='&quot;[");
            xpathNodeAndSibling[1] = xpathNode;
            Pattern siblingPat = Pattern.compile("\\]\\[following-sibling::src[^\\[]+\\[(\\d+)\\]");
            Matcher siblingMatcher = siblingPat.matcher(xpathNodeAndSibling[0]);
            int count=0;
            while (siblingMatcher.find())
                count++;
            xpathNodeAndSibling[2] = String.valueOf(count);
        }
        return xpathNodeAndSibling;
    }

    private String[] parseOneNode(String node) {
        String subXPath;
        String[] matchAndRoot = new String[2];
        matchAndRoot[0] = "";
        matchAndRoot[1] = null;
        for (String xpath : node.split("\n")) {
            xpath = xpath.replace("/unit", "");
            xpath = (xpath.startsWith("[") || xpath.startsWith("]")) ? xpath.substring(1) : xpath;          // for array access
            int subElement = StringUtils.countMatches(xpath, '/') - StringUtils.countMatches(xpath, "'/");
            xpath = xpath.replaceAll("(?<!')/", "[") + StringUtils.repeat(']', subElement - 1);
            subXPath = xpath.replaceFirst("\\[", "src:");
            if (matchAndRoot[1] == null) {
                if (Character.isDigit(subXPath.charAt(subXPath.indexOf('[')+1)))
                    matchAndRoot[1] = subXPath.trim().split("\\]")[0] + "]";
                else
                    matchAndRoot[1] = subXPath.trim().split("\\[")[0];
                matchAndRoot[1] = matchAndRoot[1].replaceAll("('[^']*')|\\[\\d+\\]", "$1");
            }
            else
                subXPath = StringUtils.removeFirst(subXPath, Pattern.quote(matchAndRoot[1]));
            Pattern typePat;
            // removing no then branch specification for if and while block
            subXPath = subXPath.replaceAll("('[^']*')|\\[\\d+\\]", "$1").
                    replace("[", "[src:").replaceAll("('[^']*')|[;\\s]+","$1").
                    replace("&", "&amp;").
                    replace("'<'", "'&lt;'").replace("'>'", "'&gt;'").
                    replace("[src:then[src:block='']]", "").replace("[src:block='']", "");
            if (subXPath.contains("\""))
                subXPath = literalString(subXPath);
            typePat = Pattern.compile("src:@type=('[\\w]+')");
            Matcher typeMatcher = typePat.matcher(subXPath);
            while (typeMatcher.find())
                subXPath = subXPath.replace(typeMatcher.group(), "contains(@type, " + typeMatcher.group(1) + ")");
            if (!subXPath.contains("[contains(@type, 'pseudo')]"))
                matchAndRoot[0] += subXPath;
        }
        return matchAndRoot;
    }

    private String literalString(String subXPath) {
        Pattern stringPat = Pattern.compile("\"[^\"]*\"");
        Matcher stringMatcher = stringPat.matcher(subXPath);
        String match;
        if (stringMatcher.find()){
            match = stringMatcher.group();
            subXPath = subXPath.replace("='" + match +"'", "[text()='&quot;" + match.substring(1, match.length()-1) + "&quot;']");
        }
        return subXPath;
    }

    private String[] getXSLTFileContent(String pointcut, String tag) {
        String xpathFile, key;
        String[] matchSequence = new String[2];
        matchSequence[1] = null;
        // tag is obtained by an already computed expression
        if (!dslParser.isFinal(pointcut, tag)) {
            matchSequence[0] = "src:tag[contains(src:name, '" + pointcut + "_" + tag.replace(pointcut+"_", "")
                    .replace("_start", "") + "_start')]";
            return matchSequence;
        }
        xpathFile = fromJavaToXpath(pointcut, tag);
        matchSequence = parseXPathFile(new File(xpathFile));
        // removing wildcards xpath from the node
        if (dslParser.hasWildcards(tag))
            for (String wildcard : dslParser.getWildcards(tag)) {
                key = extractKey(wildcard, matchSequence[0]);
                matchSequence[0] = matchSequence[0].replace("[" + key + "]", "");
                matchSequence[1] = (matchSequence[1] == null) ? null : matchSequence[1].
                        replace("[" + key + "]", "");
            }
        return matchSequence;
    }

    private void createAndApplySingleStatement(String pointcut, String tag, String[] matchSequence) {
        String transformationFile;
        String[] matchWithContext = new String[2];
        matchWithContext[0] = matchSequence[0];
        matchWithContext[1] = null;
        transformationFile = createXSLTTransformationFile(pointcut, tag, matchWithContext, false);
        String[] transfInfo = Util.getLastName(dslParser.getFileName());
        applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
    }

    private void createAndApplyStrictSequence(String pointcut, String tag1, String tag2, String[] matchSequence1,
                                              String[] matchSequence2, int sibling) {
        Util.printMap("XPathParser", "pointcutTagSibling", pointcutTagSibling);
        String[] match1WithContext, match2WithContext;
        String transformationFile, pointcutTag1, pointcutTag2;
        pointcutTag1 = pointcut + "_" + tag1.replace(pointcut+"_", "").replace("_start", "");
        pointcutTag2 = pointcut + "_" + tag2.replace(pointcut+"_", "").replace("_start", "");
        match1WithContext = new String[2];
        match2WithContext = new String[2];
        match1WithContext[1] = null;
        match2WithContext[1] = null;
        sibling = (matchSequence1[1] == null) ? sibling : sibling + Integer.parseInt(matchSequence1[2]);
        match1WithContext[0] = matchSequence1[0] + "[following-sibling::*[not(self::src:comment) and not(self::src:tag" +
                "[not(starts-with(src:name, '" + pointcutTag2 + "_start'))])]["
                + sibling + "][self::" + matchSequence2[0] + "]]";
        if (matchSequence1[1] != null)
            match1WithContext[1] = matchSequence1[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                    "not(self::src:tag)][" + matchSequence1[2] + "][self::" +
                    match1WithContext[0] + "]]";
        int siblingCounter = sibling+1;
        match2WithContext[0] = matchSequence2[0] + "[preceding-sibling::*" +
                "[not(self::src:comment) and not(self::src:tag[not(starts-with(src:name, '" + pointcutTag1 + "_start'))])]" +
                "[" + siblingCounter + "][self::src:tag[starts-with(src:name, '" + pointcutTag1 + "_start')]]]";
        if (matchSequence2[1] != null)
            match2WithContext[1] = matchSequence2[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                    "not(self::src:tag)][" + matchSequence2[2] + "][self::" +
                    match2WithContext[0] + "]]";
        // adding pointcutTag1 length to itself, if pointcut is not yet in pointcutTagSibling
        if (!pointcutTagSibling.containsKey(pointcutTag1))
            pointcutTagSibling.put(pointcutTag1, sibling);
        int siblingTag2 = pointcutTagSibling.get(pointcutTag1);
        // adding pointcutTag2 length to the pointcutTag1 one
        if (!pointcutTagSibling.containsKey(pointcutTag2))
            if (matchSequence2[1] != null)
                pointcutTagSibling.put(pointcutTag1, pointcutTagSibling.get(pointcutTag1) +
                        Integer.parseInt(matchSequence2[2]) + 1);
            else
                pointcutTagSibling.put(pointcutTag1, pointcutTagSibling.get(pointcutTag1) + 1);
        else
            pointcutTagSibling.put(pointcutTag1, pointcutTagSibling.get(pointcutTag1) + pointcutTagSibling.get(pointcutTag2));
        String[] transfInfo = null;
//        if (dslParser.isFinal(pointcut, tag1) && dslParser.isFinal(pointcut, tag2))
        dslParser.addToPointcutTagCouple(pointcut, tag2, tag1);
        if (dslParser.isFinal(pointcut, tag1)) {
            transfInfo = Util.getLastName(dslParser.getFileName());
            transformationFile = createXSLTTransformationFile(pointcut, tag1, match1WithContext, true);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        else {     // must be checked again even if is not final
            Matcher numberMatcher = Pattern.compile("\\d+").matcher(match1WithContext[0]);
            String siblingToBeAdded = null;
            if (numberMatcher.find())
                siblingToBeAdded = numberMatcher.group();
            checkFinalTag(match1WithContext[0].replaceFirst(siblingToBeAdded, String.valueOf(siblingTag2+1)),
                    pointcut, tag1);
        }
        if (dslParser.isFinal(pointcut, tag2)) {
            if (!dslParser.isFinal(pointcut, tag1))
                match2WithContext[0] = match2WithContext[0].replaceFirst("(?s)\\d+(?!.*?\\d+)",
                        String.valueOf(siblingTag2+1));
            transformationFile = createXSLTTransformationFile(pointcut, tag2, match2WithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        else       // must be checked again even if is not final
            checkFinalTag(match2WithContext[0].replaceFirst("(?s)\\d+(?!.*?\\d+)",
                    String.valueOf(siblingTag2+1)), pointcut, tag2);
    }


    private void createAndApplyLooseSequence(String pointcut, String tag1, String tag2, String[] matchSequence1,
                                             String[] matchSequence2) {
        String[] match1WithContext, match2WithContext;
        String transformationFile, pointcutTag1;
        pointcutTag1 = pointcut + "_" + tag1.replace(pointcut+"_", "").replace("_start", "");
        match1WithContext = new String[2];
        match2WithContext = new String[2];
        match1WithContext[1] = match2WithContext[1] = null;
        match1WithContext[0] = matchSequence1[0] + "[following-sibling::" + matchSequence2[0] + "]";
        if (matchSequence1[1] != null)
            match1WithContext[1] = matchSequence1[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                    "not(self::src:tag)][" + matchSequence1[2] + "][self::" +
                    match1WithContext[0] + "]]";
        match2WithContext[0] = matchSequence2[0] + "[preceding-sibling::src:tag[starts-with(src:name, '" +
                "" + pointcutTag1 + "_start')]]";
        if (matchSequence2[1] != null)
            match2WithContext[1] = matchSequence2[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                    "not(self::src:tag)][" + matchSequence2[2] + "][self::" +
                    match2WithContext[0] + "]]";
        String[] transfInfo = null;
        dslParser.addToPointcutTagCouple(pointcut, tag2, tag1);
        if (dslParser.isFinal(pointcut, tag1)) {
            transfInfo = Util.getLastName(dslParser.getFileName());
            transformationFile = createXSLTTransformationFile(pointcut, tag1, match1WithContext, true);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        else       // must be checked again even if is not final
            checkFinalTag(match1WithContext[0], pointcut, tag1);
        if (dslParser.isFinal(pointcut, tag2)) {
            if (!dslParser.isFinal(pointcut, tag1)){

            }
            transformationFile = createXSLTTransformationFile(pointcut, tag2, match2WithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        else       // must be checked again even if is not final
            checkFinalTag(match2WithContext[0], pointcut, tag2);
    }


    private void createAndApplyIfBlock(String pointcut, String conditionTag, String thenBlockTag,
                                       String elseBlockTag, String matchCondition, String[] matchThen,
                                       String[] matchElse) {
        String matchConditionWithContext, transformationFile, pointcutTagCondition, pointcutTagThen = "";
        pointcutTagCondition = pointcut + "_" + conditionTag.replace(pointcut+"_", "").
                replace("_start", "");
        String[] matchThenWithContext = null, matchElseWithContext = null;
        matchConditionWithContext = matchCondition;
        if (!thenBlockTag.equals("_")) {
            matchConditionWithContext += "[src:then[src:block[" + matchThen[0] + "]]]";
            pointcutTagThen = pointcut + "_" + thenBlockTag.replace(pointcut+"_", "").
                    replace("_start", "");
            dslParser.addToPointcutTagCouple(pointcut, thenBlockTag, conditionTag);
            matchThenWithContext = new String[2];
            matchThenWithContext[1] = null;
            matchThenWithContext[0] = matchThen[0] + "[ancestor::src:if[self::src:if[preceding-sibling::src:tag[1][self::" +
                    "src:tag[starts-with(src:name, '" + pointcutTagCondition + "_start')]]]]]";
            /*if (!elseBlockTag.equals("_"))
                matchThenWithContext[0] += "[ancestor::src:if[self::src:if[src:else[src:block[" + matchElse[0] + "]]]]]";*/
            if (matchThen[1] != null)
                matchThenWithContext[1] = matchThen[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                        "not(self::src:tag)][" + matchThen[2] + "][self::" +
                        matchThenWithContext[0] + "]]";
        }
        if (!elseBlockTag.equals("_")) {
            matchConditionWithContext += "[src:else[src:block[" + matchElse[0] + "]]]";
            dslParser.addToPointcutTagCouple(pointcut, elseBlockTag, conditionTag);
            matchElseWithContext = new String[2];
            matchElseWithContext[1] = null;
            matchElseWithContext[0] = matchElse[0] + "[ancestor::src:if[self::src:if[preceding-sibling::src:tag" +
                    "[1][self::" + "src:tag[starts-with(src:name, '" + pointcutTagCondition + "_start')]]]]]";
/*            if (!thenBlockTag.equals("_"))
                matchElseWithContext[0] += "[ancestor::src:if[self::src:if[src:then[src:block[" +
                        "src:tag[starts-with(src:name, '" + pointcutTagThen + "_start')]]]]]]";*/
            if (matchElse[1] != null)
                matchElseWithContext[1] = matchElse[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                        "not(self::src:tag)][" + matchElse[2] + "][self::" +
                        matchElseWithContext[0] + "]]";
        }
        String[] transfInfo = null;
        if (dslParser.isFinal(pointcut, conditionTag)) {
            transfInfo = Util.getLastName(dslParser.getFileName());
            transformationFile = createXSLTTransformationFile(pointcut, conditionTag, new String[]{matchConditionWithContext, null}, false);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (dslParser.isFinal(pointcut, thenBlockTag)) {
            transformationFile = createXSLTTransformationFile(pointcut, thenBlockTag, matchThenWithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (!dslParser.isFinal(pointcut, thenBlockTag) && !thenBlockTag.equals("_")) {       // must be checked again even if is not final
//            matchThenWithContext[0] = matchThen[0] + "[ancestor::src:if[self::src:if[preceding-sibling::src:tag[1][self::" +
//                    "src:tag[starts-with(src:name, '" + pointcutTagCondition + "_start')]]]]]";
            checkFinalTag(matchThenWithContext[0], pointcut, thenBlockTag);
        }
        if (dslParser.isFinal(pointcut, elseBlockTag)) {
            transformationFile = createXSLTTransformationFile(pointcut, elseBlockTag, matchElseWithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (!dslParser.isFinal(pointcut, elseBlockTag) && !elseBlockTag.equals("_"))       // must be checked again even if is not final
            checkFinalTag(matchElseWithContext[0], pointcut, elseBlockTag);
    }


    private void createAndApplyIfBlockLengthOne(String pointcut, String conditionTag, String thenBlockTag,
                                                String elseBlockTag, String matchCondition, String[] matchThen,
                                                String[] matchElse) {
        String matchConditionWithContext, transformationFile, branchesCount;
        String[] matchThenWithContext = null, matchElseWithContext = null;
        branchesCount = "[src:then[src:block[count(child::*)=1]]][src:else[src:block[count(child::*)=1]]]";
        matchConditionWithContext = matchCondition + branchesCount;
        if (!thenBlockTag.equals("_")) {
            matchConditionWithContext += "[src:then[src:block[" + matchThen[0] + "]]]";
            dslParser.addToPointcutTagCouple(pointcut, thenBlockTag, conditionTag);
            matchThenWithContext = new String[2];
            matchThenWithContext[1] = null;
            matchThenWithContext[0] = matchThen[0] + "[ancestor::src:if[self::src:if" + branchesCount +
                    "[preceding-sibling::*[1][self::" +
                    "src:tag[starts-with(src:name, '" + pointcut + "_" +
                    conditionTag + "_start')]]]]]";
/*            if (!elseBlockTag.equals("_"))
                matchThenWithContext[0] += "[ancestor::src:if[self::src:if" + branchesCount +
                        "[src:else[src:block[" + matchElse[0] + "]]]]]";*/
        }
        if (!elseBlockTag.equals("_")) {
            matchConditionWithContext += "[src:else[src:block[" + matchElse[0] + "]]]";
            dslParser.addToPointcutTagCouple(pointcut, elseBlockTag, conditionTag);
            matchElseWithContext = new String[2];
            matchElseWithContext[1] = null;
            matchElseWithContext[0] = matchElse[0] + "[ancestor::src:if[self::src:if" + branchesCount +
                    "[preceding-sibling::src:tag" +
                    "[1][self::" + "src:tag[starts-with(src:name, '" + pointcut + "_" +
                    conditionTag.replace(pointcut + "_", "").replace("_start", "") + "_start')]]]]]";
            /*if (!thenBlockTag.equals("_"))
                matchElseWithContext[0] += "[ancestor::src:if[1][self::src:if" + branchesCount +
                        "[src:then[src:block[" +
                        "src:tag[starts-with(src:name, '" + pointcut + "_" +
                        thenBlockTag.replace(pointcut + "_", "").replace("_start", "") + "_start')]]]]]]";*/
        }
        String[] transfInfo = null;
        dslParser.addToPointcutTagCouple(pointcut, thenBlockTag, conditionTag);
        dslParser.addToPointcutTagCouple(pointcut, elseBlockTag, conditionTag);
        if (dslParser.isFinal(pointcut, conditionTag)) {
            transfInfo = Util.getLastName(dslParser.getFileName());
            transformationFile = createXSLTTransformationFile(pointcut, conditionTag, new String[]{matchConditionWithContext, null}, false);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (dslParser.isFinal(pointcut, thenBlockTag)) {
            transformationFile = createXSLTTransformationFile(pointcut, thenBlockTag, matchThenWithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (!dslParser.isFinal(pointcut, thenBlockTag) && !thenBlockTag.equals("_"))      // must be checked again even if is not final
            checkFinalTag(matchThenWithContext[0], pointcut, thenBlockTag);
        if (dslParser.isFinal(pointcut, elseBlockTag)) {
            transformationFile = createXSLTTransformationFile(pointcut, elseBlockTag, matchElseWithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (!dslParser.isFinal(pointcut, elseBlockTag) && !elseBlockTag.equals("_"))       // must be checked again even if is not final
            checkFinalTag(matchElseWithContext[0], pointcut, elseBlockTag);
    }


    private void createAndApplyLoopBlock(String pointcut, String conditionTag, String blockTag,
                                         String matchCondition, String[] matchBlock) {
        String matchConditionWithContext, transformationFile, pointcutTagCondition;
        String[] matchBlockWithContext = null;
        pointcutTagCondition = conditionTag.replace(pointcut + "_", "").replace("_start", "");
        matchConditionWithContext = matchCondition;
        if (!blockTag.equals("_")) {
            matchConditionWithContext += "[src:block[" + matchBlock[0] + "]]";
            dslParser.addToPointcutTagCouple(pointcut, blockTag, conditionTag);
            matchBlockWithContext = new String[2];
            matchBlockWithContext[1] = null;
            String forOrWhile = matchCondition.substring(0, matchCondition.indexOf('['));
            matchBlockWithContext[0] = matchBlock[0] + "[ancestor::" + forOrWhile +
                    "[self::" + forOrWhile + "[preceding-sibling::src:tag[1][self::" +
                    "src:tag[starts-with(src:name, '" + pointcut + "_" +
                    pointcutTagCondition + "_start')]]]]]";
            if (matchBlock[1] != null)
                matchBlockWithContext[1] = matchBlock[1] + "[preceding-sibling::*[not(self::src:comment) and " +
                        "not(self::src:tag)][" + matchBlock[2] + "][self::" +
                        matchBlockWithContext[0] + "]]";
        }
        String[] transfInfo = null;
        if (dslParser.isFinal(pointcut, conditionTag)) {
            transfInfo = Util.getLastName(dslParser.getFileName());
            transformationFile = createXSLTTransformationFile(pointcut, conditionTag, new String[]
                    {matchConditionWithContext, null}, false);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (dslParser.isFinal(pointcut, blockTag)) {
            transformationFile = createXSLTTransformationFile(pointcut, blockTag, matchBlockWithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (!dslParser.isFinal(pointcut, blockTag) && !blockTag.equals("_"))       // must be checked again even if is not final
            checkFinalTag(matchBlockWithContext[0], pointcut, blockTag);
    }

    private void createAndApplyLoopBlockLengthOne(String pointcut, String conditionTag, String blockTag,
                                                  String matchCondition, String[] matchBlock) {
        String matchConditionWithContext, transformationFile, branchesCount;
        String[] matchBlockWithContext = null;
        branchesCount = "[src:block[count(child::*)=1]]";
        matchConditionWithContext = matchCondition + branchesCount;
        if (!blockTag.equals("_")) {
            matchConditionWithContext += "[src:block[" + matchBlock[0] + "]]";
            dslParser.addToPointcutTagCouple(pointcut, blockTag, conditionTag);
            matchBlockWithContext = new String[2];
            matchBlockWithContext[1] = null;
            String forOrWhile = matchCondition.substring(0, matchCondition.indexOf('['));
            matchBlockWithContext[0] = matchBlock[0] + "[ancestor::" + forOrWhile +
                    "[self::" + forOrWhile + branchesCount +"[preceding-sibling::src:tag[1][self::" +
                    "src:tag[starts-with(src:name, '" + pointcut + "_" +
                    conditionTag.replace(pointcut + "_", "").replace("_start", "") + "_start')]]]]]";
        }
        String[] transfInfo = null;
        if (dslParser.isFinal(pointcut, conditionTag)) {
            transfInfo = Util.getLastName(dslParser.getFileName());
            transformationFile = createXSLTTransformationFile(pointcut, conditionTag, new String[]{matchConditionWithContext, null}, false);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
        if (dslParser.isFinal(pointcut, blockTag)) {
            transformationFile = createXSLTTransformationFile(pointcut, blockTag, matchBlockWithContext, true);
            transfInfo = (transfInfo == null)? Util.getLastName(dslParser.getFileName()) : Util.getLastName(transfInfo[1]);
            applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
        }
    }

    private String createXSLTTransformationFile(String pointcut, String tag, String[] matchWithContext, boolean isSequence) {
        String transformationFileName, prologue, epilogue, template, pointcutTag, nameStart, nameEnd;
        pointcutTag = pointcut + "_" + tag;
        transformationFileName = pointcutTag + "_transf.xsl";
        if (Files.exists(new File(transformationFileName).toPath()))
            return transformationFileName;
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";

        nameStart = "<xsl:value-of select=\"concat('" + pointcutTag + "', '_start', count(preceding::" + matchWithContext[0] + "))\"/>";
        nameEnd = "<xsl:value-of select=\"concat('" + pointcutTag + "', '_end', count(preceding::" + matchWithContext[0] + "))\"/>";
        template = "<!-- Applying tag @" + pointcutTag + " before first statement -->";
        if (isSequence)
            if (isMultiline(tag)) {
                template += "\n<xsl:template match = \"" + matchWithContext[0] + "\">\n" + "<tag>@<name>" + nameStart +
                        "</name></tag><xsl:text> </xsl:text>\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                        "</xsl:template>\n";
                if (matchWithContext[1] != null) {
                    nameEnd = "<xsl:value-of select=\"concat('" + pointcutTag + "', '_end', count(preceding::" +
                            matchWithContext[1] + "))\"/>";
                    template += "\n\n<!-- Applying tag @" + pointcutTag + " after last statement -->\n" +
                            "<xsl:template match = \"" + matchWithContext[1] + "\">\n" +
                            "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                            "<tag>@<name>" +  nameEnd + "</name></tag><xsl:text> </xsl:text>\n" +
                            "</xsl:template>\n";
                }
            }
            else
                // loop or if node in sequence
                if (matchWithContext[0].startsWith("src:if") || matchWithContext[0].startsWith("src:while") ||
                        matchWithContext[0].startsWith("src:for")) {
                    template += "\n<xsl:template match = \"" + matchWithContext[0] + "\">\n" + "<tag>@<name>" + nameStart +
                            "</name></tag><xsl:text> </xsl:text>\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                            "</xsl:template>\n" +
                            "\n<xsl:template match = \"" + "src:condition[ancestor::" + matchWithContext[0].substring(0, matchWithContext[0].
                            indexOf('[')) + "[1][self::" +
                            matchWithContext[0] + "]]\">\n" + "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                            "<tag>@<name>" + nameEnd + "</name></tag><xsl:text> </xsl:text>\n" +
                            "</xsl:template>\n";
                }
                else
                    template += "\n<xsl:template match = \"" + matchWithContext[0] + "\">\n" + "<tag>@<name>" + nameStart +
                            "</name></tag><xsl:text> </xsl:text>\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                            "<tag>@<name>" + nameEnd + "</name></tag>" +
                            "<xsl:text> </xsl:text></xsl:template>\n";
        else
            template += "\n<xsl:template match = \"" + matchWithContext[0] + "\">\n" + "<tag>@<name>" + nameStart  +
                    "</name></tag><xsl:text> </xsl:text>\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "<tag>@<name>" + nameEnd + "</name></tag><xsl:text> </xsl:text>\n" +
                    "</xsl:template>\n";
       epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        Util.writeFile("XPathParser", xpathTransformationDir  + "/" + transformationFileName,
                prologue + template + epilogue);
        return transformationFileName;
    }

    private void checkFinalTag(String matchWithContext, String pointcut, String tag) {
        String[] transfInfo;
        String transformationFile;
        matchWithContext = matchWithContext.substring(0, matchWithContext.indexOf(']')+1)
                 + "[not(" + matchWithContext.substring(matchWithContext.indexOf("]") + 2,
                        matchWithContext.length()-1) + ")]";
        transfInfo = Util.getLastName(dslParser.getFileName());
        tag = tag.replace("_start", "").replace(pointcut + "_", "");
        boolean transfExists = Files.exists(new File(pointcut + "_" + tag + "_removing_transf.xsl").
                toPath());
        transformationFile = transfExists ? pointcut + "_" + tag + "_removing_transf.xsl" :
                createXSLTTransformationRemovingFile(pointcut, tag, matchWithContext);
        applyTransformation(transformationFile, transfInfo[0], transfInfo[1]);
    }

    private String createXSLTTransformationRemovingFile(String pointcut, String tag, String matchWithContext) {
        String transformationFileName, prologue, epilogue, template, pointcutTag;
        pointcutTag = pointcut + "_" + tag;
        transformationFileName = pointcutTag + "_removing_transf.xsl";
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        template = "<!-- Removing tag @" + pointcutTag + "_start" + " not being in the right context -->" +
                "\n<xsl:template match = \"" + matchWithContext + "\"/>";
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        Util.writeFile("XPathParser", xpathTransformationDir  + "/" + transformationFileName,
                prologue + template + epilogue);
        return transformationFileName;
    }


    private String applyTransformation(String transformationFile, String fileToBeTransformed, String destination) {
        Util.xsltTransformation("XPathParser", xpathTransformationDir + "/" + transformationFile,
                fileToBeTransformed, destination);
        return destination;
    }

    private boolean isMultiline(String tag) {
        return (dslParser.getPortionCode(tag).contains("\n"));
    }

    // method that writes xpath.xsl in userTransformationDir
    private void writeXpathFile() {
        String content = "<xsl:stylesheet version=\"1.0\"  xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\n" +
                "    <xsl:output omit-xml-declaration=\"yes\" indent=\"yes\"/>\n" +
                "    <xsl:strip-space elements=\"*\"/>\n" +
                "\n" +
                "    <xsl:variable name=\"vApos\">'</xsl:variable>\n" +
                "\n" +
                "    <xsl:template match=\"*[@* or not(*)] \">\n" +
                "      <xsl:if test=\"not(*)\">\n" +
                "         <xsl:apply-templates select=\"ancestor-or-self::*\" mode=\"path\"/>\n" +
                "         <xsl:value-of select=\"concat('=',$vApos,.,$vApos)\"/>\n" +
                "         <xsl:text>&#xA;</xsl:text>\n" +
                "        </xsl:if>\n" +
                "        <xsl:apply-templates select=\"@*|*\"/>\n" +
                "    </xsl:template>\n" +
                "\n" +
                "    <xsl:template match=\"*\" mode=\"path\">\n" +
                "        <xsl:value-of select=\"concat('/',name())\"/>\n" +
                "        <xsl:variable name=\"vnumPrecSiblings\" select=\n" +
                "         \"count(preceding-sibling::*[name()=name(current())])\"/>\n" +
                "        <xsl:if test=\"$vnumPrecSiblings\">\n" +
                "            <xsl:value-of select=\"concat('[', $vnumPrecSiblings +1, ']')\"/>\n" +
                "        </xsl:if>\n" +
                "    </xsl:template>\n" +
                "\n" +
                "    <xsl:template match=\"@*\">\n" +
                "        <xsl:apply-templates select=\"../ancestor-or-self::*\" mode=\"path\"/>\n" +
                "        <xsl:value-of select=\"concat('[@', name(), '=',$vApos,.,$vApos,']')\"/>\n" +
                "        <xsl:text>&#xA;</xsl:text>\n" +
                "    </xsl:template>\n" +
                "</xsl:stylesheet>";
        Util.writeFile("XPathParser", xpathTransformation, content);
    }

    private String extractKey(String wildcard, String xpath) {
        Pattern keyPat = Pattern.compile("\\[src:name='" + wildcard + "'(\\]+)");
        Matcher keyMatcher = keyPat.matcher(xpath);
        String key = null;
        int count, index, endMatch;
        if (keyMatcher.find()) {
            count = keyMatcher.group(1).length();
            endMatch = keyMatcher.end();
            index = endMatch;
            for (; count > 0; count--)
                index = xpath.substring(0, index).lastIndexOf("[");
            key = xpath.substring(index + 1, endMatch - 1);
        }
        count = 0;
        // wildcard is the only one argument in a call
        if (key.contains("][")) {
            key = key.substring(key.indexOf("][") + 2);
            for (int charIndex = 0; charIndex < key.length(); charIndex++) {
                if (key.charAt(charIndex) == '[')
                    count++;
                if (key.charAt(charIndex) == ']')
                    count--;
            }
            if (count < 0)
                key = key.substring(0, key.length() + count);
        }
        return key;
    }
}
