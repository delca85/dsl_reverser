package it.unimi.reverser.dsl.util;

import java.util.Objects;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com on 31/03/17.
 */
public class Pair<K, V> {
    private K key;
    private V value;

    public Pair(K key, V value){
        this.key = key;
        this.value = value;
    }

    @Override
    public boolean equals(Object o){
        if (!(o instanceof Pair))
            return  false;
        Pair oPair = (Pair) o;
        return (oPair.getKey().equals(this.key) && oPair.getValue().equals(this.value));
    }

    public K getKey(){
        return key;
    }

    public V getValue(){
        return value;
    }

    @Override
    public int hashCode(){
        return Objects.hash(this.key, this.value);
    }

    @Override
    public String toString(){
        return "(" + key + ", " + value + ")";
    }
}
