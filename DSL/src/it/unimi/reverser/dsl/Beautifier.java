package it.unimi.reverser.dsl;

import com.sun.tools.javac.util.Assert;
import it.unimi.reverser.dsl.util.Util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;


/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com
 * This script, which will be invoked by an ant script, makes a first beautification of Java source code
 * stored in the folder passed as argument. The output of this script execution is a file, in a subfolder (src_beautified),
 * for each one in the folder passed as argument.
 */
public class Beautifier {

    // uncrustify configuration file
    private static final String uncrustifyConf = "/Users/bibi/Documents/UniMi/TesiM/DSL/DSL/uncrustifyConf/sun.cfg";

    // complete path of a dir where to find all the Java source code files
    protected static String srcJavaDir;

    // subfolder to be put in main folder src
    protected static final String src_beautified = "src_beautified";

    public static void main(String[] args) {
        if (args.length != 1){
            System.out.println("USAGE: java it.unimi.reverser.dsl.Beautifier javaSrcCodeFolder");
            System.exit(0);
        }
        srcJavaDir = (Util.toAbsolutePath(args[0]).contains(" ")) ? Util.toAbsolutePath(args[0]).split("\\s")[0] :
                Util.toAbsolutePath(args[0]);
        check_environment(srcJavaDir);
        executer(new File(srcJavaDir));
    }

    // methodsAndConstructors checks if the srcJavaDir exists and that no src_beautified subfolder is in it
    private static void check_environment(String srcJavaDir) {
        if (Files.notExists(Paths.get(srcJavaDir)) || Files.exists(Paths.get(srcJavaDir+ "/"+ src_beautified))){
            System.out.println("[Beautifier]Please, give to this script a folder with source code with no subfolder named src_beautfied ");
            System.exit(0);
        }
        String beautifiedDir = srcJavaDir + "/" + src_beautified;
        try {
            Process p = Runtime.getRuntime().exec("mkdir " +  beautifiedDir);
            Util.printOutputAndError("mkdir", p);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Assert.check(Files.exists(Paths.get(beautifiedDir)));
        System.out.println("[Beautifier]Dir " +  beautifiedDir + " created.");

    }

    public static void executer(File path) {
        for (File f: path.listFiles()){
            if (f.isDirectory() && !f.getName().equals(src_beautified))
                executer(f);
            else
                if (f.getName().endsWith(".java"))
                    uncrustified(f.getName(), path);
                else {                              // copy optionally resources needed by application
                    try {
                        Process p;
                        String destFolder = srcJavaDir + "/" + src_beautified + path.getAbsolutePath().replace(srcJavaDir, "");
                        Util.createDir(destFolder);
                        String srcFile =  path + "/" + f.getName();
                        p = Runtime.getRuntime().exec("cp " + srcFile + " " + destFolder);
                        Util.printOutputAndError("uncrustify", p);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
        }
    }

    /*
    method that beautifies java source code in order to execute all the following transformations without receiving any error
    */
    private static void uncrustified(String originalSrcCode, File path) {
        String destFolder = srcJavaDir + "/" + src_beautified + path.getAbsolutePath().replace(srcJavaDir, "");
        Util.uncrustify("Beautifier", uncrustifyConf, path+"/"+originalSrcCode,
                destFolder + "/" +originalSrcCode);
    }

}


