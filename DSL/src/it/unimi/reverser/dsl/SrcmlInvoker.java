package it.unimi.reverser.dsl;

import it.unimi.reverser.dsl.util.Util;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 *
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com
 * This script, invoked after Beautifier execution, makes these actions:
 * 1. convert through srcML Java source code to xml files
 * 2. applies defaultAnnotations.xsl transformation to obtained xml
 * 3. reconvert to Java file the transformed xml files
 * 4. apply Java beautifier to each obtained Java files
 */
public class SrcmlInvoker {

    // xslt transformation adding default annotations
    private static String xsltTransformation = null;

    // uncrustify configuration file
    private static String uncrustifyConf = "/Users/bibi/Documents/UniMi/TesiM/DSL/DSL/uncrustifyConf/empty.cfg";

    // complete path of a dir where to find all the Java source code files
    private static String srcJavaDir;

    // complete path of a dir where to store all the files obtained through scrML and uncrustify transformation from Java source code
    private static String xmlDir = null;

    // complete path of a dir where to store all the xml files obtained after scrML transformation through xsl transformation file
    private static String transformedDir = null;

    // dslparser instance
    private static DSLParser dslParser;

    // complete path of dir with user transformation files
    private static String userTransformationDir = null;

    // complete path of dir where to put final transformations result
    private static String resultDir;

    /* args contains:
     0. folder with compiled source code
     1. folder with java source code to be transformed by srcml
     2. destination folder where obtained xml must be at the end of srcml process
    */
    public static void main(String[] args) {
        if (args.length != 4){
            System.out.println("USAGE: java it.unimi.reverser.dsl.SrcmlInvoker javaSrcCodeFolder transformationDestinationFolder " +
                    "XSLTTransformationFile userTransformationDir");
            System.exit(0);
        }
        srcJavaDir = Util.toAbsolutePath(args[0]);
        xsltTransformation = Util.toAbsolutePath(args[2]);
        checkEnvironment(Util.toAbsolutePath(args[1]));
        userTransformationDir = args[3];
        dslParser = new DSLParser(userTransformationDir + "/transformations.txt");
        executer(new File(srcJavaDir));
    }


    public static void executer(File path) {
        String xmlFile, xmlTransformed, dslParsed, transformedByUser, annotationAdjusted;
        for (File f: path.listFiles()){
            if (f.isDirectory())
                executer(f);
            else
                if (f.getName().endsWith(".java")){
                    // 1. Xml Conversion
                    xmlFile = toXml(new File(f.getAbsolutePath()));
                    // 2. Dsl Parsing and adding of tags on xml file
                    Object[] dslParsedParser = parsingWithDSL(xmlFile);
                    dslParsed = (String) dslParsedParser[0];
                    // 3. Applying CallBehavior, Chain and other defaultAnnotations
                    xmlTransformed = (dslParsed == null) ? applyTransformation(xmlFile, path) : applyTransformation(dslParsed, path);
                    // 4. Applying transformation defined by user if any
                    transformedByUser = (dslParsed == null) ? xmlTransformed : applyUserTransformation(xmlTransformed,
                            (DSLParser) dslParsedParser[1]);
                    // 5. Annotation parenthesis applied
                    annotationAdjusted = adjustingAnnotation(transformedByUser);
                    // 6. xmlTransformed converted back to Java and beautify
                    backToJavaAndBeautified(annotationAdjusted, path);
                }

        }
    }

    // methods that applies a transformation that adds a curly braces at the beginning and at the end
    // of each annotations
    private static String adjustingAnnotation(String xmlFile) {
        String destination, prologue, template, epilogue, transformationFile, annotationsSet;
        transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/')) +
                xmlFile.substring(xmlFile.lastIndexOf('/')).replace(".xml", "_closingAnnotations.xsl");
        destination = Util.getLastName(xmlFile)[1];
        annotationsSet = "((contains(., 'Call'))" +
                " or (contains(., 'Chain')) or (contains(., 'Loop')) or (contains(., 'IfEval'))" +
                " or (contains(., 'Decision')))";
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        template = "\n\n<!-- add a { after each beginning annotation -->\n" +
                "<xsl:template name=\"openingAnnotation\" match=\"src:annotation[" +
                "src:name[(substring(., string-length(.) - 2 ) != 'End') and " +
                annotationsSet + "]]\">\n" +
                "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>" +
                "<xsl:text>{</xsl:text></xsl:template>\n";
        template += "\n\n<!-- add a } instead of each end annotation -->\n" +
                "<xsl:template name=\"endingAnnotation\" match=\"src:annotation[" +
                "src:name[(substring(., string-length(.) - 2 ) = 'End') and " +
                annotationsSet + "]]\">\n" +
                "<xsl:text>}</xsl:text></xsl:template>\n";
        Util.writeFile("SrcmlInvoker", transformationFile, prologue + template + epilogue);
        Util.xsltTransformation("SrcmlInvoker", transformationFile, xmlFile, destination);
        return destination;
    }

    /* method checking xml destination folder existence and if it is empty.
       after that, it creates three folders:
       1. a folder where to put xml files after first transformation from Java source code
       2. a folder to store xml files after xslt transformation
       3. a folder to memoize java beautified file
     */
    private static void checkEnvironment(String destDir) {
        if (Files.notExists(Paths.get(destDir))){
            System.out.println("[SrcmlInvoker]" + destDir + " does not exist.");
            System.out.println("[SrcmlInvoker]Please, provide to the script a folder where obtained file will be stored!");
            System.exit(0);
        }
        if (xmlDir == null) {
            xmlDir = destDir + "/xml";
            transformedDir = destDir + "/transformed";
            resultDir = destDir + "/finalResult";
            Util.createDir(xmlDir);
            Util.createDir(transformedDir);
            Util.createDir(resultDir);
            System.out.println("[SrcmlInvoker]Dirs " + xmlDir + ", " + transformedDir + "," +
                    " and " + resultDir + " created.");
        }
    }

    // method invoking srcML to do the conversion from Java to xml files. It returns xml obtained.
    private static String toXml(File f) {
        String fileName, packageDir;
        packageDir = f.getAbsolutePath().substring(0, f.getAbsoluteFile().toString().lastIndexOf('/'))
                .toString().replace(srcJavaDir, "");
        Util.createDir(xmlDir+packageDir);
        fileName = xmlDir + packageDir + "/" +  f.getName().replace(".java", ".xml");
        try {
            Util.srcmlConversion("SrcmlInvoker", f.getCanonicalPath(), fileName);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("[SrcmlInvoker]Unable to obtain " + f + " canonical path");
        }
        return fileName;
    }

    // method applying callBehaviorChain.xsl transformation (if exists) and then defaultAnnotations.xsl
    // it returns xml transformed file complete path
    private static String applyTransformation(String xmlFile, File path) {
        String transformedXml, completeTransformedXml, packageDir, callBehaviorChain;
        transformedXml = xmlFile.substring(0, xmlFile.lastIndexOf('/')) +
                xmlFile.substring(xmlFile.lastIndexOf('/')).replaceAll("\\*\\d+\\.xml", ".xml");
        packageDir = path.toString().replace(srcJavaDir, "");
        callBehaviorChain = xsltTransformation.substring(0, xsltTransformation.lastIndexOf('/') + 1) +
                "callBehaviorChain" + packageDir.replace("/", "_") + "_" +
                transformedXml.substring(transformedXml.lastIndexOf('/') + 1).replace(".xml", ".xsl");
        Util.createDir(transformedDir + packageDir);
        if (Files.exists(Paths.get(callBehaviorChain))){
            transformedXml = transformedDir + packageDir + transformedXml.substring(transformedXml.lastIndexOf('/'),
                    transformedXml.lastIndexOf(".")) + "_firstXSLT.xml";
            // applying callBehaviorChain xslt transformation
            Util.xsltTransformation("SrcmlInvoker", callBehaviorChain, xmlFile, transformedXml);
            completeTransformedXml = transformedXml.substring(0, transformedXml.lastIndexOf('/')) +
                    transformedXml.substring(transformedXml.lastIndexOf('/')).replace("firstXSLT", "defaultAnnotations");
        }
        else {
            transformedXml = xmlFile;
            completeTransformedXml = transformedDir + packageDir + xmlFile.substring(xmlFile.lastIndexOf('/')).
                    replaceAll("\\*\\d+.xml", "").replaceAll(".xml", "") + "_defaultAnnotations.xml";
        }
        Util.xsltTransformation("SrcmlInvoker", xsltTransformation, transformedXml, completeTransformedXml);
        return completeTransformedXml;
    }

    // method that invokes DSLParser using userTransformationDir
    private static Object[] parsingWithDSL(String f) {
        Object[] fileDSLParser = new Object[2];
        String resultingFile = null;
        dslParser.setToBeTransformed(f);
        //DSLParser dslParser = new DSLParser(transformationFile, f, userTransformationDir);
        if (Files.exists((new File(userTransformationDir + "/transformations.txt")).toPath()))
            resultingFile = dslParser.applyTransformation();
        fileDSLParser[0] = resultingFile;
        fileDSLParser[1] = dslParser;
        return fileDSLParser;
    }

    // method that invokes DSLParser in order to apply transformations requested by user
    private static String applyUserTransformation(String xmlFile, DSLParser dslParser) {
        return dslParser.adviceApplication(xmlFile);
    }



    // method converting xml to java file (with srcML) and beautify it with uncrustify
    // it returns java obtained file complete path
    private static String backToJavaAndBeautified(String xmlTransformed, File path) {
        String javaFile, javaBeautifiedFile, packageDir;
        packageDir = path.toString().replace(srcJavaDir, "");
        javaFile = xmlTransformed.replace(".xml", ".java");
        javaBeautifiedFile = resultDir + packageDir + javaFile.substring(javaFile.lastIndexOf('/'),
                javaFile.lastIndexOf("_")) + ".java";
        Util.srcmlConversion("SrcmlInvoker", xmlTransformed, javaFile);
        Util.uncrustify("SrcmlInvoker", uncrustifyConf, javaFile, javaBeautifiedFile);
        return javaBeautifiedFile;
    }



}
