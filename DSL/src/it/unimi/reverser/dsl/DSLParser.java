package it.unimi.reverser.dsl;

import it.unimi.reverser.dsl.util.Pair;
import it.unimi.reverser.dsl.util.Util;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com on 10/03/17.
 * DSLParser receives a txt file as argument and applies transformations inside txt file to the second file
 * received as argument.
 */
public class DSLParser {
    // map that bounds each pointcut name to an expression
    private Map<String, String> pointcutExpression;
    // map that bounds each pointcut name to a list of portions code names
    private Map<String, List<String>> pointcutCodeTag;
    // map that bounds its portion code name to the portion code
    private Map<String, String> portionCode;
    // map that bounds each pointcut to a list of special nodes of that expression
    private Map<String, List<String>> specialNode;
    // map that bounds each pointcut to a list of advices related to it
    private Map<String, List<String>> advice;
    // map that bounds each tag code to a list of variables used as a kind of wildcards
    private Map<String, List<String>> variables;
    // map that bounds each variable + tag to a its working code

    // map that bounds each pointcut to a list of couple composed by tagName and a list of wildcard id
    private Map<String, List<Pair>> pointcutVariablesTagCouple;
    // map that bounds each pointcut or tag name to a list of constraintsPointcutAndTag related to variables map
    private Map<String, List<String>> constraintsPointcutAndTag;
    // map that bounds each pointcut to a list of couple composed by a couple of tag and the constraint related to them
    private Map<String, List<Pair>> constraintsPointcut;
    // map that bounds each tag of a pointcut with wildcards, with a list of strings of matching lines
    private Map<String, List<String>> wildcardMatches;
    // map that bounds each pointcut to a list of file where pointcut must be searched
    private Map<String, List<String>> within;
    // map that bounds each pointcut to a list of file where pointcut must not be searched
    private Map<String, List<String>> notWithin;
    // map that bounds each pointcut to a list of pair, in the pairs, each key is a tag and each value is
    // the tag which to the key is related
    private Map<String, List<Pair>> pointcutTagCouple;
    // file on which apply transformation
    private String toBeTransformed;
    // transformations file
    private String transformationFile;
    // transformation dir
    private String userTransformationDir;
    // transformation file must be read just one time for all source code passed as resources to be transformed
    private boolean alreadyRead = false;

    public DSLParser(String transformationFile) {
        this.transformationFile = transformationFile;
        this.userTransformationDir = transformationFile.substring(0, transformationFile.lastIndexOf('/'));
        pointcutExpression = new LinkedHashMap<>();
        pointcutCodeTag = new HashMap<>();
        variables = new LinkedHashMap<>();
        constraintsPointcutAndTag = new LinkedHashMap<>();
        constraintsPointcut = new LinkedHashMap<>();
        pointcutVariablesTagCouple = new HashMap<>();
        wildcardMatches = new LinkedHashMap<>();
        pointcutTagCouple = new LinkedHashMap<>();
        within = new HashMap<>();
        notWithin = new HashMap<>();
        portionCode = new HashMap<>();
        specialNode = new HashMap<>();
        advice = new LinkedHashMap<>();
    }

    // this method parses transformation file, in order to populate all maps and launches XPathParser that applies
    // pointcut tag on xml files. Then it removes not special pointcut tag.
    public String applyTransformation() {
        if (!alreadyRead) {
            readFile();
            alreadyRead = true;
        }
        System.out.println("[DSLParser]Applying transformations in " + transformationFile + " to file " + toBeTransformed);
        for (String pointcut : pointcutExpression.keySet()) {
            if (applyingPointcut(pointcut)) {
                ExpressionParser parser = new ExpressionParser(this);
                parser.parse(pointcut, pointcutExpression.get(pointcut));
            }
        }
        Util.printMap("DSLParser", "pointcutTagCouple", pointcutTagCouple);
        String xmlFile = removeUnmatched(Util.getLastName(toBeTransformed));
        extractWildcards(Util.getLastName(toBeTransformed));
        Util.printMap("DSLParser", "wildcardMatches", wildcardMatches);
        //xmlFile = wildcardHandling(Util.getLastName(toBeTransformed)[0]);
        xmlFile = wildcardHandling(xmlFile);
        xmlFile = removeNotMatchedAnyMore(Util.getLastName(xmlFile));
        xmlFile = removeNotSpecials(Util.getLastName(xmlFile));
        return xmlFile;
    }

    private boolean applyingPointcut(String pointcut) {
        if (!within.keySet().contains(pointcut) && !notWithin.keySet().contains(pointcut))
            return true;
        if (within.keySet().contains(pointcut) && within.get(pointcut).stream().anyMatch(v -> toBeTransformed.endsWith(
                v.replace(".java", ".xml"))))
            return true;
        if (notWithin.keySet().contains(pointcut) && !notWithin.get(pointcut).stream().anyMatch(v ->
                toBeTransformed.endsWith(v.replace(".java", ".xml"))))
            return true;
        return false;
    }

    public void addToPointcutTagCouple(String pointcut, String tag1, String tag2){
        if (!pointcutTagCouple.containsKey(pointcut))
            pointcutTagCouple.put(pointcut, new ArrayList<>());
        List<Pair> couplesList = pointcutTagCouple.get(pointcut);
        Pair toBeAdded = new Pair(tag1.replace(pointcut+"_", "").
                replace("_start", ""), tag2.replace(pointcut+"_", "").
                replace("_start", ""));
        if (!couplesList.contains(toBeAdded))
            couplesList.add(toBeAdded);
    }

    public String getPortionCode(String tag) {
        return portionCode.get(tag);
    }

    public String getTransformationFile() {
        return transformationFile;
    }

    public String getFileName() {
        return toBeTransformed;
    }

    public boolean isFinal(String pointcut, String tag) {
        return pointcutCodeTag.get(pointcut).contains(tag);
    }

    public String getUserTransformationDir() {
        return userTransformationDir;
    }

    // method that applies advice and then remove tags
    public String adviceApplication(String xmlFile) {
        for (String pointcut : advice.keySet())
            applyAdvices(pointcut, xmlFile);
        String tagsRemoved = removingTags(Util.getLastName(xmlFile));
        return tagsRemoved;
    }

    public boolean hasWildcards(String tag) {
        return variables.containsKey(tag);
    }

    public List<String> getWildcards(String tag) {
        return variables.get(tag);
    }

    public void setToBeTransformed(String toBeTransformed) {
        this.toBeTransformed = toBeTransformed;
    }

    // method that populates wildcardVariables maps, checks constraintsPointcutAndTag
    private String wildcardHandling(String xmlFile) {
        if (wildcardMatches.size() == 0)
            return xmlFile;
        String portion, templateToReEnumerate = "";
        List<String> wildcardMatchList;
        Map wildcardVariables;
        Map<String, Integer> tagIndex;
        String destination = Util.getLastName(xmlFile)[1];
        List<String> tagToBeRemoved = new ArrayList<>();
        for (String pointcut : pointcutCodeTag.keySet()) {
            tagIndex = new HashMap<>();
            // if no tag defined for this pointcut has a wildcard, this pointcut is not interesting
            if (pointcutCodeTag.get(pointcut).stream().noneMatch(this::hasWildcards) ||
                    !applyingPointcut(pointcut))
                continue;
            wildcardVariables = new HashMap<String, List<Pair>>();
            for (String tag : pointcutCodeTag.get(pointcut).stream().filter(tag -> hasWildcards(tag) &&
                    wildcardMatches.get(tag) != null).
                    collect(Collectors.toList())) {
                templateToReEnumerate += "\n\n<!-- renaming " + pointcut + "_" + tag + "_start* " +
                        "according to tag number preceding -->\n" +
                        "<xsl:template match=\"src:tag/src:name[starts-with(current(), " +
                        "'" + pointcut + "_" + tag + "_start')]\">\n" +
                        "<name><xsl:value-of select=\"concat('" + pointcut + "_" + tag + "_start', " +
                        "count(preceding::src:tag[starts-with(src:name, '" + pointcut + "_" + tag +
                        "_start')]) + count(ancestor::src:tag[starts-with(src:name, '" + pointcut +
                        "_" + tag + "_start')]) - 1)\"/></name>\n</xsl:template>" +
                        "\n\n<!-- renaming " + pointcut + "_" + tag + "_end* " +
                        "according to tag number preceding -->\n" +
                        "<xsl:template match=\"src:tag/src:name[starts-with(current(), " +
                        "'" + pointcut + "_" + tag + "_end')]\">\n" +
                        "<name><xsl:value-of select=\"concat('" + pointcut + "_" + tag + "_end', " +
                        "count(preceding::src:tag[starts-with(src:name, '" + pointcut + "_" + tag +
                        "_end')]) + count(ancestor::src:tag[starts-with(src:name, '" + pointcut +
                        "_" + tag + "_end')]) - 1)\"/></name>\n</xsl:template>";
                portion = portionCode.get(tag);
                // portion must be modified if is an if condition
                portion = (!(portion.startsWith("if") || portion.startsWith("while") || portion.startsWith("for"))
                        && !portion.endsWith(";")) ? "if (" + portion + ")" : portion;
                wildcardMatchList = wildcardMatches.get(tag);
                tagIndex.put(tag, wildcardMatchList.size());
                List<List<String>> diffs;
                // each portion code defined through wildcard, can match more than one group of java lines
                for (String wildcardMatch : wildcardMatchList) {
                    String wcPlaceHolder = "$";
                    Map<String, String> helperWcMap = new HashMap();
                    for (String wc : variables.get(tag)){
                        portion = portion.replace(wc, wcPlaceHolder);
                        helperWcMap.put(wcPlaceHolder, wc);
                        wcPlaceHolder += "$";
                    }
                    diffs = getAllDiffs(portion, wildcardMatch);
                    List<String> diffsHelper = new ArrayList();
                    for (String wcDiffs : diffs.get(0))
                        diffsHelper.add(helperWcMap.get(wcDiffs));
                    diffs.set(0, diffsHelper);
                    List<String> portionDiffList, wildcardDiffList;
                    List<Pair> pairsList = new ArrayList<>();
                    portionDiffList = diffs.get(0);
                    wildcardDiffList = diffs.get(1);
                    for (int i = 0; i < wildcardDiffList.size(); i++)
                        pairsList.add(new Pair(portionDiffList.get(i), wildcardDiffList.get(i)));
                    wildcardVariables.put(tag+ wildcardMatchList.indexOf(wildcardMatch), pairsList);
                }
            }
            wildcardVariables = checkConstraints(pointcut, wildcardVariables);
            Util.printMap("DSLParser" + pointcut, "wildcardVariables", wildcardVariables);
            for (String tagForIndex : tagIndex.keySet())
                for (int i=0; i < tagIndex.get(tagForIndex); i++)
                    if (!wildcardVariables.containsKey(tagForIndex + i))
                        tagToBeRemoved.add(pointcut + "_" + tagForIndex + "*" + i);
        }
        // write transformation that deletes not special tag and ones related to not matching portion code
        String prologue, epilogue, template = "", transformationFile;
        transformationFile = destination.substring(0, destination.indexOf('.')) + "_wildcardTransformation.xsl";
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        for (String tagName : tagToBeRemoved){
            template += "\n<!-- removing " + tagName + "_start-->\n" +
                    "<xsl:template match = \"src:tag[src:name ='" + tagName.split("\\*")[0] + "_start" +
                    tagName.split("\\*")[1] + "']\"/>\n" +
                    "\n<!-- removing " + tagName + "_end-->\n" +
                    "<xsl:template match = \"src:tag[src:name ='" + tagName.split("\\*")[0] + "_end" +
                    tagName.split("\\*")[1] + "']\"/>\n";
        }
        Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);

        //tag related to wildcard re-enumerate in order to not be deleted by notMatchingAnymore
        transformationFile = transformationFile.substring(0, transformationFile.lastIndexOf('_')) +
                "_tagReEnumerating.xsl";
        Util.writeFile("DSLParser", transformationFile, prologue + templateToReEnumerate
                + epilogue);
        xmlFile = destination;
        destination = Util.getLastName(xmlFile)[1];
        Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);
        return destination;
    }

    private Map<String, List<Pair>> checkConstraints(String pointcut, Map<String, List<Pair>> wildcardVariables) {
        String wildcard2, operator, var1, var2;
        boolean satisfied;
        // checking tag constraintsPointcutAndTag
        for (String tag : wildcardVariables.keySet()){
            if (!constraintsPointcutAndTag.containsKey(tag.replaceAll("\\d+", "")))
                continue;
            satisfied = true;
            for (Pair<String, String> pair : wildcardVariables.get(tag)) {
                for (String constraint : constraintsPointcutAndTag.get(tag.replaceAll("\\d+", "")).
                        stream().filter(c -> parseConstraint(c)[0].equals(
                        pair.getKey().replaceAll("\\d+", ""))).collect(Collectors.toList())) {
                    operator = parseConstraint(constraint)[1];
                    wildcard2 = parseConstraint(constraint)[2];
                    var1 = pair.getValue();
                    // wildcard2 is a final string
                    if (wildcard2.startsWith("\""))
                        satisfied = constraintSatisfied(var1, wildcard2, operator);
                    // wildcard2 must be retrieve from wildcardVariables
                    else {
                        var2 = Util.getPairFromList(wildcard2, wildcardVariables.get(tag));
                        // it is enough that a value related to wildcard2 satisfies with var1 the constraint
                        satisfied = constraintSatisfied(var1, var2, operator);
                    }
                }
                // if the pair under control does not satisfies a constraint, this list must be removed from
                // wildcardVariables map and no more pairs from this list should be checked
                if (!satisfied)
                    break;
            }
            if (!satisfied)
                wildcardVariables.remove(tag);
        }
        // checking pointcut constraintsPointcutAndTag (no final string expected) using constraintsPointcut map
        satisfied = true;
        List<Pair> assTag1, assTag2;
        Map<String, List<String>> helperMap = new HashMap<>();
        Pair tags;
        String tag1, tag2;
        if (constraintsPointcut.containsKey(pointcut)) {
            for (Pair tagConstraintsList : constraintsPointcut.get(pointcut)) {
                tags = (Pair) tagConstraintsList.getKey();
                tag1 = (String) tags.getKey();
                tag2 = (String) tags.getValue();
                for (String tag1Indexed : wildcardVariables.keySet()) {
                    if (!tag1Indexed.replaceAll("\\d+", "").equals(tag1))
                        continue;
                    assTag1 = wildcardVariables.get(tag1Indexed);
                    for (String tag2Indexed : wildcardVariables.keySet()) {
                        if (!tag2Indexed.replaceAll("\\d+", "").equals(tag2))
                            continue;
                        assTag2 = wildcardVariables.get(tag2Indexed);
                        for (String constraint : (List<String>) tagConstraintsList.getValue()) {
                            var1 = Util.getPairFromList(parseConstraint(constraint)[0], assTag1);
                            var2 = Util.getPairFromList(parseConstraint(constraint)[2], assTag2);
                            satisfied = constraintSatisfied(var1, var2, parseConstraint(constraint)[1]);
                            // if constraint is not satisfied, this assTag1 and assTag2 cannot work together
                            if (!satisfied)
                                break;
                        }
                        if (satisfied) {
                            Util.addToMap(tag1Indexed, tag2Indexed, helperMap);
                            Util.addToMap(tag2Indexed, tag1Indexed, helperMap);
                        }
                    }
                }
            }
            for (Pair tagsCoupleConstraintsList : constraintsPointcut.get(pointcut)) {
                tags = (Pair) tagsCoupleConstraintsList.getKey();
                tag1 = (String) tags.getKey();
                tag2 = (String) tags.getValue();
                helperMap = removeNotChecking(tag1, tag2, helperMap);
            }
            // created in order to avoid javaConcurrentModificationException
            List<String> toBeRemoved = new ArrayList<>();
            // assignment list not in helper map but involved in any pointcut constraint, must be removed
            for (String tag : wildcardVariables.keySet()) {
                boolean constrained = false;
                // assignment not involved in any pointcut constraints must be kept in wildcardVariables map
                for (Pair<Pair, List> coupleList : constraintsPointcut.get(pointcut))
                    if (coupleList.getKey().getKey().equals(tag.replaceAll("\\d+", "")) ||
                            coupleList.getKey().getValue().equals(tag.replaceAll("\\d+", ""))) {
                        constrained = true;
                        break;
                    }
                if (!constrained)
                    continue;
                // tag involved in pointcut constraint but not in helperMap anymore
                if (!helperMap.keySet().contains(tag) || helperMap.get(tag).size() == 0)
                    toBeRemoved.add(tag);
            }
            for (String tag : toBeRemoved)
                wildcardVariables.remove(tag);
        }
        return wildcardVariables;
    }

    // method that removes from map and list, member not satisfying all constraints
    private Map<String, List<String>> removeNotChecking(String tag1, String tag2, Map<String, List<String>> map) {
        boolean found;
        for (String tagIndexed : map.keySet()){
            if (!tagIndexed.replaceAll("\\d+", "").equals(tag1) &&
                    !tagIndexed.replaceAll("\\d+", "").equals(tag2))
                continue;
            found = false;
            if (tagIndexed.replaceAll("\\d+", "").equals(tag1)) {
                for (String tag2Indexed : map.get(tagIndexed)) {
                    if (tag2Indexed.replaceAll("\\d+", "").equals(tag2)) {
                        found = true;
                        break;
                    }
                }
            }
            else if (tagIndexed.replaceAll("\\d+", "").equals(tag2)){
                for (String tag1Indexed : map.get(tagIndexed)){
                    if (tag1Indexed.replaceAll("\\d+", "").equals(tag1)){
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                map.remove(tagIndexed);
                for (String tag : map.keySet())
                    if (map.get(tag).remove(tagIndexed) && map.get(tag).size() == 0)
                        map.remove(tag);
            }
        }

        return map;
    }


    // method that returns true if constraint is satisfied, false otherwise
    private boolean constraintSatisfied(String var1, String var2, String operator) {
        int n1, n2;
        switch (operator){
            case "==":
                // var2 is a string
                if (var2.startsWith("\"")) {
                    var2 = var2.substring(1, var2.length()-1);
                    if (var2.contains("*")) {
                        if (var2.endsWith("*"))
                            return var1.startsWith(var2.substring(0, var2.length() - 1));
                        else if (var2.startsWith("*"))
                            return var1.endsWith(var2.substring(1));
                        else {
                            for (String subVar : var2.split("\\*"))
                                if (!var1.contains(subVar))
                                    return false;
                            return true;
                        }
                    }
                }
                // var2 is a variable
                return var1.equals(var2);
            case "<":
                n1 = Integer.parseInt(var1);
                n2 = Integer.parseInt(var2);
                return n1 < n2;
            case "<=":
                n1 = Integer.parseInt(var1);
                n2 = Integer.parseInt(var2);
                return n1 <= n2;
            case ">":
                n1 = Integer.parseInt(var1);
                n2 = Integer.parseInt(var2);
                return n1 > n2;
            case ">=":
                n1 = Integer.parseInt(var1);
                n2 = Integer.parseInt(var2);
                return n1 >= n2;
            case "!=":
                if (var2.startsWith("\""))
                    var2 = var2.substring(1, var2.length()-1);
                return (!var1.equals(var2));
            default: return false;

        }
    }

    // method that parses constraint and returns wildcard and operator involved
    private String[] parseConstraint(String constraint) {
        Pattern constraintPat = Pattern.compile("([^=<>!]+)\\s*([=<>!]+)\\s*(.*)");
        Matcher constraintMatcher = constraintPat.matcher(constraint);
        String[] constraintExpr = new String[3];
        while (constraintMatcher.find()) {
            constraintExpr[0] = constraintMatcher.group(1);
            constraintExpr[1] = constraintMatcher.group(2);
            constraintExpr[2] = constraintMatcher.group(3);
        }
        return constraintExpr;
    }

    private void readFile() {
        System.out.println("[DSLParser]Reading transformation file " + transformationFile);
        File file = new File(transformationFile);
        String content = null;
        try {
            content = new String(Files.readAllBytes(file.toPath())).replaceAll("\t+", "");
        } catch (IOException e) {
            System.out.println("[DSLParser]Unable to read file " + file.getAbsolutePath());
            System.exit(0);
        }
        for (String pointcutDefinition : content.split("\\n\\n")) {
            if (pointcutDefinition.length() == 0) break;
            String[] pointcutOrAdviceDefinition = pointcutDefinition.split("\\n");
            if (pointcutOrAdviceDefinition[0].startsWith("composite_pattern"))
                pointcutInformation(pointcutDefinition);
            else if (pointcutOrAdviceDefinition[0].startsWith("when")) {
                String adviceString = "";
                for (String advice : pointcutOrAdviceDefinition)
                    adviceString += advice;
                adviceInformation(adviceString);
            }
            else System.out.println("[DSLParser]Wrong line written in " + transformationFile);

        }

        manageConstraints();
        Util.printMap("DSLParser", "pointcutExpression", pointcutExpression);
        Util.printMap("DSLParser", "specialNode", specialNode);
        Util.printMap("DSLParser", "pointcutCodeTag", pointcutCodeTag);
        Util.printMap("DSLParser", "portionCode", portionCode);
        Util.printMap("DSLParser", "constraintsPointcutAndTag", constraintsPointcutAndTag);
        Util.printMap("DSLParser", "constraintsPointcut", constraintsPointcut);
        Util.printMap("DSLParser", "variables", variables);
        Util.printMap("DSLParser", "within", within);
        Util.printMap("DSLParser", "notWithin", notWithin);
        Util.printMap("DSLParser", "pointcutVariablesTagCouple", pointcutVariablesTagCouple);
        Util.printMap("DSLParser", "advice", advice);
    }

    // method that interpolates constraintsStored map in order to populate constraintsPointcut map
    private void manageConstraints() {
        String tag1, tag2, wildcard1, wildcard2;
        // only the constraintsPointcut related to pointcuts are interesting for this method
        for (String pointcut : pointcutCodeTag.keySet().stream().
                filter(p -> constraintsPointcutAndTag.keySet().contains(p)).collect(Collectors.toList())) {
            Map<Pair, List<String>> coupleConstraint = new HashMap();
            for (String constraint : constraintsPointcutAndTag.get(pointcut)) {
                tag1 = tag2 = null;
                wildcard1 = parseConstraint(constraint)[0];
                wildcard2 = parseConstraint(constraint)[2];
                for (Pair tagWildcardList : pointcutVariablesTagCouple.get(pointcut)) {
                    if (tag1 == null && ((List) tagWildcardList.getValue()).contains(wildcard1))
                        tag1 = (String) tagWildcardList.getKey();
                    if (tag2 == null && ((List) tagWildcardList.getValue()).contains(wildcard2))
                        tag2 = (String) tagWildcardList.getKey();
                    if (tag1 != null && tag2 != null)
                        break;
                }
                Util.addToMap(new Pair(tag1, tag2), constraint, coupleConstraint);
            }
            Util.printMap("DSLParser", "coupleConstraint", coupleConstraint);
            for (Pair tags : coupleConstraint.keySet())
                Util.addToMap(pointcut, new Pair(tags, coupleConstraint.get(tags)), constraintsPointcut);
        }

    }

    private void pointcutInformation(String pointcutDefinition) {
        String[] pointcutDefinitionArray = pointcutDefinition.split("\n");
        String pointcutName = extractPointcutName(pointcutDefinitionArray[0]);
        String expression = extractGoalNodes(pointcutName, pointcutDefinitionArray[1].replaceAll("(\\s+)\\s", "$1"));
        pointcutExpression.put(pointcutName, expression.replaceAll("\\s+\\s", " "));
        if (!expression.equals("all"))
            addToCodeTagAndPortionCode(pointcutName, String.join("\n", Arrays.asList(pointcutDefinition.split("\n")).
                subList(2, pointcutDefinition.split("\n").length)));
        else
             Util.addToMap(pointcutName, "all", advice);
    }


    private void adviceInformation(String adviceDefinition) {
        System.out.println("AAA" + adviceDefinition + "BBB");
        Pattern advicePat = Pattern.compile("^when\\s+([^:]+)\\s*:\\s*\\n?([^$]+)\\s*$");
        Matcher matcher = advicePat.matcher(adviceDefinition);
        if (matcher.find()) {
            System.out.println("AAA" + matcher.group(1).replaceAll("\\s", "") + "\nBBB" + matcher.group(2));
            Util.addToMap(matcher.group(1).trim(), matcher.group(2), advice);
        }
    }

    // method that extracts tag and portion code (in order to populate portionCode and portionCodeTag maps)
    // and variable and constraint (in order to populate variables and constraintsPointcutAndTag maps)
    private void addToCodeTagAndPortionCode(String pointcutName,
                                            String pointcutDefinition) {
        // if some node inline definition has already been stored in pointcutCodeTag map
        List tagList, values;
        tagList = pointcutCodeTag.containsKey(pointcutName) ?
                pointcutCodeTag.get(pointcutName) : new ArrayList();
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile("([A-Za-z]+)\\s*:\\s*\\«([^\\»]*)\\»\\s*" +
                "(\\[variables\\s*:\\s*\\{([^}]+)\\}(,\\s*constraints\\s*:\\s*\\{([^}]+)\\})?\\s*\\])?");
        matcher = pattern.matcher(pointcutDefinition);
        while (matcher.find()) {
            String tag = matcher.group(1);
            tagList.add(tag);
            portionCode.put(tag, matcher.group(2));
            if (matcher.group(4) != null) {
                values = (matcher.group(4).contains(",")) ? Arrays.asList(matcher.group(4).
                        replaceAll("(\"[\\s+]\")|(\\s+)", "$1").split(",")) :
                        Arrays.asList(new String[]{matcher.group(4).replaceAll("(\"[\\s+]\")|(\\s+)", "$1")});
                variables.put(tag, values);
                Util.addToMap(pointcutName, new Pair(tag, values), pointcutVariablesTagCouple);
            }
            if (matcher.group(6) != null) {
                values = (matcher.group(6).contains(",")) ? Arrays.asList(matcher.group(6).
                        replaceAll("(\"[\\s+]\")|(\\s+)", "$1").split(",")) :
                        Arrays.asList(new String[]{matcher.group(6).replaceAll("(\"[\\s+]\")|(\\s+)", "$1")});
                constraintsPointcutAndTag.put(tag, values);
            }
        }
        pointcutCodeTag.put(pointcutName, tagList);
    }

    // method that extract pointcut names from pointcut definition line
    private String extractPointcutName(String pointcutDefinitionString) {
        Pattern pointcutPat = Pattern.compile("^composite_pattern\\s([a-z_]+)([\\s+](!within|within)[\\s]*\\(([a-zA-Z0-9.,\\s/_]+)\\))?:");
        Matcher pointcutMatcher = pointcutPat.matcher(pointcutDefinitionString);
        String pointcutName = null;
        List<String> withinOrNot;
        if (pointcutMatcher.find()) {
            pointcutName = pointcutMatcher.group(1);
            if (pointcutMatcher.group(3) != null){
                withinOrNot = pointcutMatcher.group(4).contains(",") ?
                        Arrays.asList(pointcutMatcher.group(4).replaceAll("\\s", "").split(",")) :
                        Arrays.asList(pointcutMatcher.group(4).replaceAll("\\s", ""));
                if (pointcutMatcher.group(3).equals("within"))
                    within.put(pointcutName, withinOrNot);
                else
                    notWithin.put(pointcutName, withinOrNot);
            }
        }
        return pointcutName;
    }

    /**
     * This method makes several actions:
     * 1. pointcut's constraintsPointcutAndTag extraction
     * 2. inline defined nodes' variables and constraintsPointcutAndTag extraction
     * 3. inline defined nodes extraction
     * 4. special node detection
     */
    // method that extracts important nodes from each pointcut expression and replaces
    // node written inline with node tags. Further, this method populates portionCode and pointcutCodeTag maps.
    private String extractGoalNodes(String pointcut, String expression) {
        Pattern pattern;
        Matcher matcher;
        String match, newExpression;
        List values;
        if (expression.equals("all"))
            return expression;
        // pointcut's constraintsPointcutAndTag
        pattern = Pattern.compile("\\[([^\\]]+)\\]\\s*$");
        matcher = pattern.matcher(expression);
        if (matcher.find())
            constraintsPointcutAndTag.put(pointcut, Arrays.asList(matcher.group(1).replaceAll("(\"[\\s+]\")|(\\s+)", "$1")
                    .split(",")));
        // inline defined nodes' constraintsPointcutAndTag extraction
        pattern = Pattern.compile("variables\\s*:\\s*\\{([^}]+)\\}(,\\s*constraints\\s*:\\s*\\{([^}]+)\\})?\\s*\\](?!$)");
        matcher = pattern.matcher(expression);
        char node = 'A';
        while (matcher.find()) {
            match = matcher.group(1);
            values = (match.contains(",")) ? Arrays.asList(match.replaceAll("(\"[\\s+]\")|(\\s+)", "$1").
                    split(",")) :
                    Arrays.asList(new String[]{match.replaceAll("(\"[\\s+]\")|(\\s+)", "$1")});
            variables.put(pointcut + "-" + node, values);
            Util.addToMap(pointcut, new Pair(pointcut + "-" + node, values), pointcutVariablesTagCouple);
            // constraintsPointcutAndTag' declaration is not mandatory
            if ((matcher.groupCount() > 2) && (match = matcher.group(3)) != null) {
                values = (match.contains(",")) ? Arrays.asList(match.replaceAll("(\"[\\s+]\")|(\\s+)", "$1").
                        split(",")) :
                        Arrays.asList(new String[]{match.replaceAll("(\"[\\s+]\")|(\\s+)", "$1")});
                constraintsPointcutAndTag.put(pointcut + "-" + node, values);
            }
            node++;
        }
        expression = expression.replaceAll("\\[[^\\]]+\\]", "");
        // inline defined nodes' extraction
        pattern = Pattern.compile("«[^»]+»");
        matcher = pattern.matcher(expression);
        int matchStart, matchEnd, index = 0;
        newExpression = "";
        List tagList = new ArrayList();
        node = 'A';
        while (matcher.find()) {
            match = matcher.group();
            matchStart = matcher.start();
            matchEnd = matcher.end();
            newExpression += expression.substring(index, matchStart) + expression.substring(matchStart, matchEnd).
                    replace(match, pointcut + "-" + node);
            index = matchEnd;
            // tag must be inserted in pointcutCodeTag map
            tagList.add(pointcut + "-" + node);
            // code found within «» must be inserted in codePortionTag map
            portionCode.put(pointcut + "-" + node, match.substring(1, match.length() - 1));
            node++;
        }
        expression = (newExpression.equals("")) ? expression : newExpression + expression.substring(index);
        pointcutCodeTag.put(pointcut, tagList);
        pattern = Pattern.compile("•([A-Za-z\\-]+)");
        matcher = pattern.matcher(expression);
        List goals = new ArrayList();
        while (matcher.find())
            goals.add(matcher.group(1));
        specialNode.put(pointcut, goals);
        return expression.replace("•", "");
    }

    // method that removes all pointcuts created by transformations involving wildcards
    private String extractWildcards(String[] xmlFileDestination) {
        String xmlFile, destination, prologue, template = "", epilogue, transformationFile,
                tagStart, tagEnd, countString, precedingString;
        xmlFile = xmlFileDestination[0];
        destination = xmlFile.substring(0, xmlFile.indexOf('.')) + "_";
        transformationFile = xmlFile.substring(0, xmlFile.indexOf('.')) + "_";
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        epilogue = "\n</xsl:stylesheet>";
        Map<String, Integer> pointcutIndex = new HashMap<>();
        Stream<String> xmlLines = Util.readFile("DSLParser", xmlFile);
        Pattern tagNamePat = Pattern.compile("\\<tag\\>\\@\\<name\\>([a-zA-Z]+\\_[a-zA-Z-]+)_(start|end)(\\d+)");
        Matcher tagNameMatcher;
        for (String line : xmlLines.collect(Collectors.toList())){
            tagNameMatcher = tagNamePat.matcher(line);
            while (tagNameMatcher.find()) {
                String name = tagNameMatcher.group(1);
                int index = Integer.parseInt(tagNameMatcher.group(3));
                if (!pointcutIndex.keySet().contains(name) || pointcutIndex.get(name) < index)
                    pointcutIndex.put(name, index);
            }
        }
        // an xslt transformation will be executed for each tag with wildcards
        for (String pointcut : pointcutCodeTag.keySet()) {
            if (!applyingPointcut(pointcut))
                continue;
            for (String tag : pointcutCodeTag.get(pointcut))
                if (hasWildcards(tag) && pointcutIndex.keySet().contains(pointcut + "_" + tag)) {
                    destination = destination.substring(0, destination.lastIndexOf('/')) +
                            destination.substring(destination.lastIndexOf('/'), destination.indexOf('_')) +
                            "_" + pointcut + "_" + tag + "_removeWildcards.xml";
                    transformationFile = transformationFile.substring(0, transformationFile.lastIndexOf('/')) +
                            transformationFile.substring(transformationFile.lastIndexOf('/'), transformationFile.indexOf('_')) +
                            "_" + pointcut + "_" + tag + "_removeWildcards.xsl";
                    template = "\n\n<!-- removing all comment in the code -->\n" +
                            "<xsl:template name=\"removingComment\" match=\"src:comment\"/>";
                    // adding a template for each pointcut_tag_start|end_number
                    for (int index = 0; index <= pointcutIndex.get(pointcut + "_" + tag); index++) {
                        tagStart = "src:tag[src:name[.='" + pointcut + "_" + tag + "_start" + index + "']]";
                        tagEnd = "src:tag[src:name[.='" + pointcut + "_" + tag + "_end" + index + "']]";
                        countString = "count(following::" + tagStart + ") &lt; count(following::" + tagEnd + ")";
                        precedingString = "preceding::" + tagStart + " and following::" + tagEnd + " and " +
                                countString;
                        template += "\n\n<!-- retrieving line between @" + pointcut + "_" + tag + " tag -->\n" +
                                "<xsl:template name=\"" + pointcut + "_" + tag + "_retrieving" + index + "\" match=\"/\">" +
                                "\n<unit>\n" +
                                "<xsl:for-each select=\"//*[not(self::src:comment) and not(self::src:tag) and " + precedingString + " and not (ancestor::*[" +
                                precedingString + "])]\">\n" +
                                "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                "</xsl:for-each>\n" +
                                "</unit>" +
                                "</xsl:template>\n";
                        Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
                        Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);
                        try {
                            Process p = Runtime.getRuntime().exec("srcml " + destination);//.replace(".xml", "_removeTags.xml"));
                            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                            String lines, javaCode = "";
                            while ((lines = stdInput.readLine()) != null) {
                                lines = lines.replaceAll("(\"[^\"]*\")|\\@[^\\s]+\\s", "$1")
                                        .replaceAll("[\t]+", "");
                                // removing lines when match involves all the loop or if block
                                javaCode += lines;
                            }
                            javaCode = javaCode.replaceAll("\n+", "");
                            if (javaCode.startsWith("while") || javaCode.startsWith("if") || javaCode.startsWith("for"))
                                javaCode = javaCode.replaceAll("((while|if|for)\\s+\\([^\\)]+\\)+).*", "$1");
                            if (!javaCode.equals(""))
                                Util.addToMap(tag, javaCode, wildcardMatches);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }
                }
        }
        return (destination.endsWith("_")) ? xmlFile : destination;
    }

    // method that creates an XSLTTransformation that removes unmatched start tag
    private String removeUnmatched(String[] xmlFileDestination){
        String xmlFile, destination, prologue, template = "", epilogue, transformationFile;
        xmlFile = xmlFileDestination[0];
        destination = xmlFileDestination[1];
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/>" +
                "</xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        for (String pointcut : pointcutCodeTag.keySet())
            for (String tag : pointcutCodeTag.get(pointcut)){
                template += "\n\n<!-- remove @" + pointcut + "_" + tag + "_end* not preceded by " +
                        pointcut + "_" + tag + "_start* -->\n" +
                        "<xsl:template match=\"src:tag[starts-with(src:name, '" + pointcut + "_" + tag + "_end')]\">\n" +
                        "\n<xsl:if test = \"preceding-sibling::src:tag[src:name=concat('" + pointcut + "_" + tag + "_start', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag + "_end'))]\">\n" +
                        "\n<xsl:copy-of select=\".\"/>" +
                        "\n</xsl:if>\n</xsl:template>";
            }

        transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/')) +
                xmlFile.substring(xmlFile.lastIndexOf('/')).replace(".xml", "removingUnmatched.xsl");
        Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);
        return destination;
    }

    // method that creates an XSLTTransformation that removes tag not matched anymore because the tags to
    // which they were related have been removed
    private String removeNotMatchedAnyMore(String[] xmlFileDestination){
        String xmlFile, destination, prologue, template, epilogue, transformationFile, tag1, tag2;
        Pair pair;
        xmlFile = destination = null;
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        epilogue = "\n\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/>" +
                "</xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        for (String pointcut : pointcutTagCouple.keySet()) {
            if (!applyingPointcut(pointcut))
                continue;
            for (int i = pointcutTagCouple.get(pointcut).size() - 1; i >= 0; i--) {
                xmlFile = (xmlFile == null) ? xmlFileDestination[0] : destination;
                destination = (destination == null) ? xmlFileDestination[1] : Util.getLastName(xmlFile)[1];
                pair = pointcutTagCouple.get(pointcut).get(i);
                tag1 = (String) pair.getKey();
                tag2 = (String) pair.getValue();
                template = "\n\n<!-- remove @" + pointcut + "_" + tag1 + "_start* if does not exist " +
                        pointcut + "_" + tag2 + "_end* -->\n" +
                        "<xsl:template match=\"src:tag[starts-with(src:name, '" + pointcut + "_" + tag1 + "_start')]\">" +
                        "\n<xsl:if test = \"following::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_start'))] or" +
                        "preceding::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_start'))] or" +
                        "ancestor::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_start'))] or" +
                        "descendant::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_start'))]\">" +
                        "\n<xsl:copy-of select=\".\"/>" +
                        "\n</xsl:if>\n</xsl:template>" +
                        "\n\n<!-- remove @" + pointcut + "_" + tag1 + "_end* if does not exist " +
                        pointcut + "_" + tag2 + "_end* -->\n" +
                        "<xsl:template match=\"src:tag[starts-with(src:name, '" + pointcut + "_" + tag1 + "_end')]\">" +
                        "\n<xsl:if test = \"following::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_end'))] or" +
                        "preceding::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_end'))] or" +
                        "ancestor::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_end'))] or" +
                        "descendant::src:tag[src:name=concat('" + pointcut + "_" + tag2 + "_end', " +
                        "substring-after(current()/src:name, '" + pointcut + "_" + tag1 + "_end'))]\">" +
                        "\n<xsl:copy-of select=\".\"/>" +
                        "\n</xsl:if>\n</xsl:template>";
                transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/')) +
                        xmlFile.substring(xmlFile.lastIndexOf('/')).replace(".xml", tag1 + "_" + tag2 +
                                "_removingNotMatchedAnyMore.xsl");
                Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
                Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);
            }
        }
        destination = (destination == null) ? xmlFileDestination[0] : destination;
        return destination;

    }

    // method that creates an XSLTTransformation that removes comments and tags not special
    private String removeNotSpecials(String[] xmlFileDestination) {
        String xmlFile, destination, prologue = null, template = "", epilogue = null, transformationFile = null;
        xmlFile = xmlFileDestination[0];
        destination = xmlFileDestination[1];
        for (String pointcut : pointcutCodeTag.keySet()) {
            if (!applyingPointcut(pointcut))
                continue;
            for (String tag : pointcutCodeTag.get(pointcut))
                if (!specialNode.get(pointcut).contains(tag)) {
                    if (prologue == null) {
                        prologue = "<xsl:stylesheet\n" +
                                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                                "exclude-result-prefixes=\"src\"\n" +
                                "version=\"1.0\">\n";
                        epilogue = "\n<!-- identity copy -->\n" +
                                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/>" +
                                "</xsl:copy></xsl:template>\n" +
                                "\n</xsl:stylesheet>";
                    }
                    template += "\n\n<!-- remove @" + pointcut + "_" + tag + " tag -->\n" +
                            "<xsl:template name=\"" + pointcut + "_" + tag + "RemovingNotSpecial" +
                            "\" match=\"src:tag[contains" +
                            "(src:name, '" + pointcut + "_" + tag + "_')]\">\n</xsl:template>\n";
                }
        }
        if (prologue != null) {
            transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/')) +
                    xmlFile.substring(xmlFile.lastIndexOf('/')).replace(".xml", ".xsl");
            Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        }
        if (transformationFile != null)
            Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);
        return (transformationFile == null) ? xmlFile : destination;
    }

    // after advice application, all tags must be removed
    private String removingTags(String[] xmlFileDestination) {
        String xmlFile, destination, prologue, epilogue, template = "", transformationFile;
        xmlFile = xmlFileDestination[0];
        destination = xmlFileDestination[1];
        transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/')) +
                xmlFile.substring(xmlFile.lastIndexOf('/')).replace(".xml", "_removeTags.xsl");
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        template += "\n\n<!-- removing tag -->\n" +
                "<xsl:template name=\"removingTags\" " +
                "match=\"src:tag\">\n</xsl:template>\n";
        Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        if (!template.equals(""))
            Util.xsltTransformation("DSLParser", transformationFile, xmlFile, destination);

        return destination;
    }

    private void applyAdvices(String pointcut, String xmlFile) {
        for (String advice : advice.get(pointcut)) {
            if (advice.startsWith("abstract"))
                applyAbstract(pointcut, xmlFile, advice);
            if (advice.startsWith("refine"))
                applyRefine(pointcut, xmlFile, advice);
            if (advice.equals("all") && applyingPointcut(pointcut))
                applyAll(pointcut, xmlFile, advice);
        }
    }

    private void applyAll(String pointcut, String xmlFile, String advice) {
        String[] sourceDestination = Util.getLastName(xmlFile);
        String prologue, template = "", epilogue, transformationFile;
        transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/') + 1) +
                this.transformationFile.substring(this.transformationFile.lastIndexOf('/') + 1,
                        this.transformationFile.indexOf('.')) + "_" + pointcut + "AllAdviceRemovingAnnotations.xsl";
            prologue = "<xsl:stylesheet\n" +
                    "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                    "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                    "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                    "exclude-result-prefixes=\"src\"\n" +
                    "version=\"1.0\">\n";
            template = "\n<!-- Removing annotations -->" +
                    "<xsl:template name=\"removeAllAnnotationsFunc\" " +
                    "match=\"src:function[count(descendant::src:return)&lt;2 and count(descendant::src:throw) = 0]//src:annotation\">\n" +
                    "</xsl:template>\n\n" +
                    "<xsl:template name=\"removeAllAnnotationsCons\" match=\"src:constructor" +
                    "[count(descendant::src:return)&lt;2 and count(descendant::src:throw) = 0]//src:annotation\">\n" +
                    "</xsl:template>\n";
            epilogue = "\n<!-- identity copy -->\n" +
                    "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                    "\n</xsl:stylesheet>";
        if (!Files.exists(new File(transformationFile).toPath())) {
            Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        }
            Util.xsltTransformation("DSLParser", transformationFile, sourceDestination[0], sourceDestination[1]);
            sourceDestination = Util.getLastName(xmlFile);
            transformationFile = transformationFile.replace("_" + pointcut + "AllAdviceRemovingAnnotations.xsl",
                    "_" + pointcut + "AllAdviceAddingAnnotation.xsl");
        if (!Files.exists(new File(transformationFile).toPath())) {
            template = "\n<!-- Adding CallAction before first function element child -->\n" +
                    "<xsl:template name=\"addCallActionFunc\" match=\"src:function[count(descendant::src:return)&lt;2 " +
                    "and count(descendant::src:throw)=0 and not(descendant::src:try[descendant::src:return])]/src:block[1][count(child::*[not(self::src:comment)])>1]" +
                    "/*[1][not(self::src:return)]\">\n" +
                    "\t<annotation>@<name>CallAction</name><argument_list>(" +
                    "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">&quot;all_" + pointcut +
                    "&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "\t\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "</xsl:template>\n";
            template += "\n<!-- Adding CallActionEnd after last function element child -->\n" +
                    "<xsl:template name=\"addCallActionEndFunc\" match=\"src:function[count(descendant::src:return)&lt;2 and " +
                    "count(descendant::src:throw) = 0 and not(descendant::src:try[descendant::src:return])]/src:block[1][count(child::*[not(self::src:comment)])>1]" +
                    "/*[last()][not(self::src:return)]\">\n" +
                    "\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "\t\t<xsl:text>&#xa;</xsl:text><annotation>@<name>CallActionEnd</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "</xsl:template>\n" +
                    "<xsl:template name=\"addCallActionEndFuncOneStatement\" match=\"src:function[count(descendant::src:return)&lt;2 and " +
                    "count(descendant::src:throw) = 0 and not(descendant::src:try[descendant::src:return])]/src:block[1][count(child::*[not(self::src:comment)])=1]" +
                    "/*[last()][not(self::src:return)]\">\n" +
                    "\t<annotation>@<name>CallAction</name><argument_list>(" +
                    "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">&quot;all_" + pointcut +
                    "&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "\t\t<xsl:text>&#xa;</xsl:text><annotation>@<name>CallActionEnd</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "</xsl:template>\n" +
                    "\n\n<xsl:template name=\"addCallActionEndFuncReturn\" match=\"src:function[count(descendant::src:return)&lt;2 and " +
                    "count(descendant::src:throw) = 0 and not(descendant::src:try[descendant::src:return])]/src:block[1][count(child::*[not(self::src:comment)])>1]" +
                    "/*[last()][(self::src:return)]\">\n" +
                    "\t\t<xsl:text>&#xa;</xsl:text><annotation>@<name>CallActionEnd</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "\t\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "</xsl:template>\n";
            template += "\n<!-- Adding CallAction before first constructor element child -->\n" +
                    "<xsl:template name=\"addCallActionCons\" match=\"src:constructor[count(descendant::src:throw)=0]/src:block[1][count(child::*[not(self::src:comment)])>1]" +
                    "/*[1][not(self::src:expr_stmt" +
                    "[src:expr[src:call[src:name='this']]]) and not(self::src:expr_stmt[src:expr[src:call[src:name='super']]])]\">\n" +
                    "\t<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all_" + pointcut + "&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "\t\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "</xsl:template>\n" +
                    "<xsl:template name=\"addCallActionConsThis\" match=\"src:constructor[count(descendant::src:throw)=0]" +
                    "/src:block[1][count(child::*[not(self::src:comment)])>1]/*[1][(self::src:expr_stmt" +
                    "[src:expr[src:call[src:name='this']]] or self::src:expr_stmt[src:expr[src:call[src:name='super']]])]\">\n" +
                    "\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "\t\t<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all_" + pointcut + "&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "</xsl:template>\n";
            template += "\n<!-- Adding CallActionEnd after last constructor element child -->\n" +
                    "<xsl:template name=\"addCallActionEndCons\" match=\"src:constructor[count(descendant::src:throw) = 0]/src:block[1]" +
                    "[count(child::*[not(self::src:comment)])>1]" +
                    "/*[last()][not(self::src:expr_stmt" +
                    "[src:expr[src:call[src:name='this']]]) and not(self::src:expr_stmt[src:expr[src:call[src:name='super']]])]\">\n" +
                    "\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "\t\t<xsl:text>&#xa;</xsl:text><annotation>@<name>CallActionEnd</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "</xsl:template>" +
                    "<xsl:template name=\"addCallActionEndConsOneStatement\" match=\"src:constructor[count(descendant::src:throw) = 0]/src:block[1]" +
                    "[count(child::*[not(self::src:comment)])=1]" +
                    "/*[last()][not (self::src:expr_stmt" +
                    "[src:expr[src:call[src:name='this']]]) and not(self::src:expr_stmt[src:expr[src:call[src:name='super']]])]\">\n" +
                    "\t<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all_" + pointcut + "&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "\t<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "\t\t<xsl:text>&#xa;</xsl:text><annotation>@<name>CallActionEnd</name><argument_list>(<argument><expr><name>id</name>" +
                    "<operator>=</operator><literal type=\"string\">&quot;all&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "</xsl:template>";
            Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        }
        Util.xsltTransformation("DSLParser", transformationFile, sourceDestination[0], sourceDestination[1]);

    }

    private void applyAbstract(String pointcut, String xmlFile, String advice) {
        String[] sourceDestination = Util.getLastName(xmlFile);
        String prologue, template = "", epilogue, transformationFile, countString, tagStart, tagEnd,
                annotationId = null, annotationIdXml;
        Pattern idPat = Pattern.compile("\"([^\"]+)\"");
        Matcher idMatcher = idPat.matcher(advice);
        if (idMatcher.find())
            annotationId = idMatcher.group(1);
        transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/') + 1) +
                this.transformationFile.substring(this.transformationFile.lastIndexOf('/') + 1,
                        this.transformationFile.indexOf('.')) + "_" + pointcut + "AbstractAdvice.xsl";
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        for (String tag : specialNode.get(pointcut)) {
            tagStart = "src:tag[starts-with(src:name, '" + pointcut + "_" + tag + "_start')]";
            tagEnd = "src:tag[starts-with(src:name, '" + pointcut + "_" + tag + "_end')]";
            annotationIdXml = "concat('" + annotationId + "', count(preceding::" +
                    "src:tag[starts-with(src:name, '" + pointcut + "_') and contains(src:name, '_start')]))";
            countString = "count(following::" + tagStart + ") &lt; count(following::" + tagEnd + ")";
            template += "\n<!-- Applying abstract advice to " + pointcut + "_" + " start -->";
            template += "\n<xsl:template name=\"addingCallAction" + "_" + pointcut + "_" + tag +
                    "\" match=\"" + tagStart + "\">\n" +
                    "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id" +
                    "</name><operator>=</operator><literal type=\"string\">&quot;<xsl:value-of select=\"" +
                    annotationIdXml + "\"/>&quot;</literal></expr></argument>)</argument_list></annotation>\n" +
                    "</xsl:template>\n";
            template += "\n<!-- Removing annotations between tags -->";
            template += "\n<xsl:template name=\"removeAnnotations" + "_" + pointcut + "_" + tag +
                    "\" match=\"src:annotation[preceding::" +
                    tagStart + " and following::" + tagEnd + " and " +
                    countString + "]\">\n</xsl:template>\n";
            template += "\n<!-- Closing abstract applied annotations -->";
            template += "\n<xsl:template name=\"closingAbstractAnnotations" + "_" + pointcut + "_" + tag +
                    "\" match=\"" + tagEnd + "\">" +
                    "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                    "<annotation>@<name>CallActionEnd</name></annotation>" +
                    "</xsl:template>\n";
        }
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        Util.xsltTransformation("DSLParser", transformationFile, sourceDestination[0], sourceDestination[1]);
    }

    private void applyRefine(String pointcut, String xmlFile, String advice) {
        String[] sourceDestination = Util.getLastName(xmlFile);
        String prologue, template = "", epilogue, transformationFile, match,
                idToBeRemoved = null, annotationId = null, annotationIdXml, tagStart, tagEnd, countString;
        Pattern idPat = Pattern.compile("\"([^\"]+)\",\\s*\"([^\"]+)\"");
        Matcher idMatcher = idPat.matcher(advice);
        if (idMatcher.find()) {
            idToBeRemoved = idMatcher.group(1);
            annotationId = idMatcher.group(2);
        }
        transformationFile = xmlFile.substring(0, xmlFile.lastIndexOf('/') + 1) +
                this.transformationFile.substring(this.transformationFile.lastIndexOf('/') + 1,
                        this.transformationFile.indexOf('.')) + "_" + pointcut + "RefineAdvice.xsl";
        prologue = "<xsl:stylesheet\n" +
                "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                "exclude-result-prefixes=\"src\"\n" +
                "version=\"1.0\">\n";
        for (String tag : specialNode.get(pointcut)) {
            tagStart = "src:tag[contains(src:name, '" + pointcut + "_" + tag + "_start')]";
            tagEnd = "src:tag[contains(src:name, '" + pointcut + "_" + tag + "_end')]";
            countString = "count(following::" + tagStart + ") &lt; count(following::" + tagEnd + ")";
            match = "src:annotation[preceding::" +
                    tagStart + " and following::" + tagEnd + " and " +
                    countString + "]/src:argument_list/src:argument/src:expr" +
                    "[src:name='id']/src:literal[starts-with(., '&quot;" + idToBeRemoved + "')]";
            annotationIdXml = "concat('" + annotationId + "', count(preceding::src:annotation[preceding::" +
                    "src:tag[contains(src:name, '" + pointcut + "_') and contains(src:name, '_start')]" +
                    " and following::src:tag[contains(src:name, '" + pointcut + "_') and contains(src:name, '_end')] and " +
                    countString + "]/src:argument_list/src:argument/src:expr" +
                    "[src:name='id']/src:literal[starts-with(., '&quot;" + idToBeRemoved + "')]))";
            template += "\n<!-- Applying refine advice to " + pointcut + "_" + tag + " -->";
            template += "\n<xsl:template name=\"renamingAnnotation_" + pointcut + "_" + tag +
                    "\" match=\"" + match + "\">\n" +
                    "<literal type=\"string \">&quot;<xsl:value-of select=\"" + annotationIdXml + "\"/>" +
                    "&quot;</literal>\n</xsl:template>\n";
        }
        epilogue = "\n<!-- identity copy -->\n" +
                "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                "\n</xsl:stylesheet>";
        Util.writeFile("DSLParser", transformationFile, prologue + template + epilogue);
        Util.xsltTransformation("DSLParser", transformationFile, sourceDestination[0], sourceDestination[1]);
    }


    private static int[][] lcs(String s, String t) {
        int m, n;
        m = s.length();
        n = t.length();
        int[][] table = new int[m + 1][n + 1];
        for (int i = 0; i < m + 1; i++)
            for (int j = 0; j < n + 1; j++)
                table[i][j] = 0;
        for (int i = 1; i < m + 1; i++)
            for (int j = 1; j < n + 1; j++)
                if (s.charAt(i - 1) == t.charAt(j - 1))
                    table[i][j] = table[i - 1][j - 1] + 1;
                else
                    table[i][j] = Math.max(table[i][j - 1], table[i - 1][j]);
        return table;
    }

    private static List<List<String>> getDiffs(int[][] table, String s, String t, int i, int j,
                                               int indexS, int indexT, List<List<String>> diffs) {
        List<String> sList, tList;
        sList = diffs.get(0);
        tList = diffs.get(1);
        if (i > 0 && j > 0 && (s.charAt(i - 1) == t.charAt(j - 1)))
            return getDiffs(table, s, t, i - 1, j - 1, indexS, indexT, diffs);
        else if (i > 0 || j > 0) {
            if (i > 0 && (j == 0 || table[i][j - 1] < table[i - 1][j])) {
                if (i == indexS)
                    sList.set(sList.size() - 1, String.valueOf(s.charAt(i - 1)) + sList.get(sList.size() - 1));
                else
                    sList.add(String.valueOf(s.charAt(i - 1)));
                diffs.set(0, sList);
                return getDiffs(table, s, t, i - 1, j, i - 1, indexT, diffs);
            } else if (j > 0 && (i == 0 || table[i][j - 1] >= table[i - 1][j])) {
                if (j == indexT)
                    tList.set(tList.size() - 1, String.valueOf(t.charAt(j - 1)) + tList.get(tList.size() - 1));
                else
                    tList.add(String.valueOf(t.charAt(j - 1)));
                diffs.set(1, tList);
                return getDiffs(table, s, t, i, j - 1, indexS, j - 1, diffs);
            }
        }
        return diffs;
    }

    private static List<List<String>> getAllDiffs(String s, String t) {
        List<List<String>> diffs = new ArrayList<List<String>>();
        List<String> l1, l2;
        l1 = new ArrayList<>();
        l2 = new ArrayList<>();
        diffs.add(l1);
        diffs.add(l2);
        diffs = getDiffs(lcs(s, t), s, t, s.length(), t.length(), 0, 0, diffs);
        diffs.set(0, diffs.get(0).stream().map(var -> var.replaceAll("(\"[^\"]+\"|new\\s+)|\\s+", "$1")).
                filter(var -> !var.equals("")).collect(Collectors.toList()));
        diffs.set(1, diffs.get(1).stream().map(var -> var.replaceAll("(\"[^\"]+\"|new\\s+)|\\s+", "$1")).
                filter(var -> !var.equals("")).collect(Collectors.toList()));
        return diffs;
    }

}
