package it.unimi.reverser.dsl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com on 13/03/17.
 * This class parses expressions composed by String (representing tag of portion of code) and operators defined
 * as shown in OPERATORS map.
 * The expressions are received by DSLParser.
 */
public class ExpressionParser {
    // Operators
    private static final Map OPERATORS = new HashMap<String, String>();
    static {
        // Map<"token", []{precendence, associativity}>
        OPERATORS.put("<", "strict_sequence");
        OPERATORS.put("<<", "loose_sequence");
        OPERATORS.put("|-", "if_block");
        OPERATORS.put("||-", "loop_block");
        OPERATORS.put("*|-", "if_block_one");
        OPERATORS.put("*||-", "loop_block_one");
    }

    private  static final String goal = "•";
    private  static final String thenElse = "|";

    private String pointcut;
    private XPathParser xPathParser;

    public ExpressionParser(DSLParser dslParser) {
        this.xPathParser = new XPathParser(dslParser);
    }


    public void parse(String pointcut, String expression) {
        System.out.println("[ExpressionParser]Looking for pointcut " + pointcut + " in file "
                + xPathParser.getDslParser().getFileName());
        this.pointcut = pointcut;
        if (!expression.equals("all")) {
            String[] input = expression.split(" ");
            String[] output = infixToRPN(input);

            // Feed the RPN string to RPNToXpath to give result
            String result = RPNToXpath(output);
        }
    }

    // Tests if token is an operator
    private static boolean isOperator(String token) {
        return OPERATORS.containsKey(token);
    }

    // Converts infix expression format into reverse Polish notation
    private String[] infixToRPN(String[] inputTokens) {
        ArrayList<String> out = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();

        // For each token
        for (String token : inputTokens) {
            if (token.equals(thenElse))
                continue;
            // If token is an operator
            if (isOperator(token)) {
                // While stack not empty AND stack top element
                // is an operator
                while (!stack.empty() && isOperator(stack.peek()))
                        out.add(stack.pop());

                // Push the new operator on the stack
                stack.push(token);
            }
            // If token is a left bracket '('
            else if (token.equals("("))
                stack.push(token);  //

            // If token is a right bracket ')'
            else if (token.equals(")")) {
                while (!stack.empty() && !stack.peek().equals("("))
                    out.add(stack.pop());

                stack.pop();
            }
            // If token is a node
            else
                out.add(token);
        }
        while (!stack.empty())
            out.add(stack.pop());

        String[] output = new String[out.size()];
        return out.toArray(output);
    }

    private String RPNToXpath(String[] tokens) {
        Stack<String> stack = new Stack<String>();
        String tag1, tag2, tag3 = null;
        // For each token
        if (tokens.length == 1){
            xPathParser.singleStatement(pointcut, tokens[0]);
            String result = pointcut + "_" + tokens[0] + "_start";
            stack.push(result);
        }
        else {
            for (String token : tokens) {
                // If the token is a value push it onto the stack
                if (!isOperator(token))
                    stack.push(token);
                else {
                    // Token is an operator: pop top two entries
                    if (token.equals("|-") || token.equals("*|-"))
                        tag3 = stack.pop();

                    tag2 = stack.pop();
                    tag1 = stack.pop();

                    switch (token) {
                        case "<":
                            xPathParser.strictSequence(pointcut, tag1, tag2);
                            break;
                        case "<<":
                            xPathParser.looseSequence(pointcut, tag1, tag2);
                            break;
                        case "|-":
                            xPathParser.ifBlock(pointcut, tag1, tag2, tag3);
                            break;
                        case "*|-":
                            xPathParser.ifBlockLengthOne(pointcut, tag1, tag2, tag3);
                            break;
                        case "||-":
                            xPathParser.loopBlock(pointcut, tag1, tag2);
                            break;
                        case "*||-":
                            xPathParser.loopBlockLengthOne(pointcut, tag1, tag2);
                            break;
                    }

                    //Get the result
                    String result = pointcut + "_" + tag1 + "_start";

                    // Push result onto stack
                    stack.push(result);
                    tag3 = null;
                }
            }
        }

        return stack.pop();
    }

}
