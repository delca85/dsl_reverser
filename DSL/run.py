import sys, subprocess, os
from os import listdir, system

LIB="/Users/bibi/Documents/UniMi/TesiM/DSL/DSL/"
ASPECTJ= LIB + "lib/aspectjrt-1.8.10.jar"
MYASPECT="../src"
RESOURCESWOVENDIR= sys.argv[1] + "/src_beautified"
JAVAPARSER="../lib/javaparser-core-3.0.1.jar"
TOOLS = "/Library/Java/JavaVirtualMachines/jdk1.8.0_25.jdk/Contents/Home/lib/tools.jar"
DIR="/Users/bibi/Documents/UniMi/TesiM/DSL/DSL/binSrc"			#directory from where command will be executed
CLASS="it.unimi.reverser.dsl.AspectListener"

if __name__ == '__main__':
	classpathList = [ASPECTJ, MYASPECT, RESOURCESWOVENDIR, JAVAPARSER, TOOLS, "."]
	if len(sys.argv) >= 5:
		for f in listdir(sys.argv[4]):
			classpathList.append(sys.argv[4] + f)
	CLASSPATH= ':'.join(classpathList)	
	args = ['java', '-cp', CLASSPATH, CLASS ] + sys.argv[1:4]
	subprocess.call(args, cwd=DIR)
