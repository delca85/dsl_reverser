
<xsl:stylesheet
xmlns="http://www.srcML.org/srcML/src"
xmlns:src="http://www.srcML.org/srcML/src"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
exclude-result-prefixes="src"
version="1.0">
<!--
	defaultAnnotations.xsl

	Adds all the Reverser annotations used for ActivityDiagram at each element in the code.

	Bianca Del Carretto
	delcarrettobianca@gmail.com
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" standalone="yes"/>

<!-- add import at the beginning of the file -->

<xsl:template name="addImportAnnotation" match="src:import[1]">
<xsl:text>import reverser.annotations.activitydiagram.*;&#10;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<xsl:template name="addImportAnnotationNoImportInFile" match="src:class">
<xsl:if test="not(preceding::src:import)">
<xsl:text>import reverser.annotations.activitydiagram.*;</xsl:text>
<xsl:text>import reverser.annotations.commons.*;</xsl:text>
</xsl:if>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- @ActivityDiagram -->

<!-- add @ActivityDiagram annotation before a method's implementation -->
<xsl:template name="addActivityAnnotation" match="src:function">
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:type"/><xsl:text>[</xsl:text>
<xsl:for-each select="child::src:parameter_list/src:parameter">
<xsl:value-of select="src:decl/src:type/src:name"/>
<xsl:if test="src:decl/src:name/src:index">[]</xsl:if>
<xsl:text>-</xsl:text>
</xsl:for-each>
<xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- add @ActivityDiagram annotation before a constructor's implementation -->
<xsl:template name="addActivityAnnotationConstructor" match="src:constructor">   
<annotation>@<name>ActivityDiagram</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>.</xsl:text><xsl:value-of select="child::src:name"/><xsl:text>.</xsl:text><xsl:value-of select="ancestor::src:class/src:name"/><xsl:text>[</xsl:text>
<xsl:for-each select="child::src:parameter_list/src:parameter">
<xsl:value-of select="src:decl/src:type/src:name"/><xsl:text>-</xsl:text>
<xsl:if test="src:decl/src:name/src:index">[]</xsl:if>
</xsl:for-each>
<xsl:text>]</xsl:text>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- @CallAction -->

<!-- adds @CallAction annotation around all method invocation expression -->
<xsl:template name="callActionExpr" match="src:function//src:expr[not(ancestor::src:catch)and count(src:call)=1 and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and (ancestor::src:argument_list)]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:variable name="methodName" select="src:call/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName,  '-callActionExpr', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction0End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all method invocation expression in constructor imp -->
<xsl:template name="callActionConstExpr" match="src:constructor//src:expr[not(ancestor::src:catch)and count(src:call)=1 and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and (ancestor::src:argument_list)]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:variable name="methodName" select="src:call/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName,  '-callActionExpr', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction1End</name></annotation></xsl:template>

<!-- adds @CallAction annotation around all constructor invocation before method execution -->
<xsl:template name="callActionExprInitArg" match="src:function//src:expr[not(ancestor::src:catch) and (child::*[1][self::src:operator[.='(']]) and (child::*[3][self::src:operator[.='=']]) and (child::*[4][self::src:operator[.='new'][not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']])]]) and (child::*[6][self::src:operator[.=')']]) and (child::*[7][self::src:operator[.='.']])]/src:call[1]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:variable name="methodName" select="src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionExprInitArg', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<operator>new</operator><xsl:text> </xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction2End</name></annotation>
</xsl:template>

<!-- removing "new" operator caught in previous template -->
<xsl:template match="src:function//src:expr[not(ancestor::src:catch) and (child::*[1][self::src:operator[.='(']]) and (child::*[3][self::src:operator[.='=']]) and (child::*[4][self::src:operator[.='new']]) and (child::*[6][self::src:operator[.=')']]) and (child::*[7][self::src:operator[.='.']])]/src:operator[.='new'][not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']])]">
</xsl:template>

<!-- adds @CallAction annotation around all constructor invocation before method execution in constructor implementation -->
<xsl:template name="callActionExprInitArgConst" match="src:constructor//src:expr[not(ancestor::src:catch) and (child::*[1][self::src:operator[.='(']]) and (child::*[3][self::src:operator[.='=']]) and (child::*[4][self::src:operator[.='new'][not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']])]]) and (child::*[6][self::src:operator[.=')']]) and (child::*[7][self::src:operator[.='.']])]/src:call[1]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:variable name="methodName" select="src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionExprInitArg', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<operator>new</operator><xsl:text> </xsl:text>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction5End</name></annotation>
</xsl:template>

<!-- removing "new" operator caught in previous template -->
<xsl:template match="src:constructor//src:expr[not(ancestor::src:catch) and (child::*[1][self::src:operator[.='(']]) and (child::*[3][self::src:operator[.='=']]) and (child::*[4][self::src:operator[.='new']]) and (child::*[6][self::src:operator[.=')']]) and (child::*[7][self::src:operator[.='.']])]/src:operator[.='new'][not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']])]">
</xsl:template>

<!-- adds @CallAction annotation around all constructor invocation before method invocation in case of array initialization -->
<xsl:template name="callActionExprInitArgBeginArray" match="src:function//src:expr[not(ancestor::src:catch) and not(ancestor::src:init) and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and (child::src:operator[.='='])]/src:operator[.='new' and not(preceding-sibling::*[1][src:name='Chain' or src:name='CallBehavior']) and (following-sibling::*[1][self::src:name])]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:variable name="methodName" select="following-sibling::*[1]/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionExprInitArg', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- completes @CallAction annotation around all constructor invocation before method invocation in case of array initialization-->
<xsl:template name="callActionExprInitArgEndArray" match="src:function//src:expr[not(ancestor::src:catch) and not(ancestor::src:init) and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and (child::src:operator[.='new']) and (child::src:operator[.='='])]/src:name[preceding-sibling::*[1][.='new'] and child::src:index]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction3End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all constructor invocation before method invocation in case of array initialization, in constructor implementation -->
<xsl:template name="callActionExprInitArgBeginArrayConstructor" match="src:constructor//src:expr[not(ancestor::src:catch) and not(ancestor::src:init) and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and (child::src:operator[.='='])]/src:operator[.='new' and not(preceding-sibling::*[1][src:name='Chain' or src:name='CallBehavior']) and (following-sibling::*[1][self::src:name])]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:variable name="methodName" select="following-sibling::*[1]/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionExprInitArg', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- completes @CallAction annotation around all constructor invocation before method invocation in constructor if a new array is created -->
<xsl:template name="callActionExprArrayInitArgEndArrayConstructor" match="src:constructor//src:expr[not(ancestor::src:catch) and not(ancestor::src:init) and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and (child::src:operator[.='new']) and (child::src:operator[.='='])]/src:name[preceding-sibling::*[1][.='new'] and child::src:index]">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction4End</name></annotation>
</xsl:template>


<!-- adds @CallAction annotation around all method invocation expression when there is an initialization-->
<xsl:template name="callActionExprInit" match="src:function//src:init/src:expr[count(src:call)=1 and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and not(ancestor::src:catch or ancestor::src:while or parent::src:for)]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:variable name="methodName" select="src:call/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionExprInit', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction7End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all method invocation expression when there is an initialization in constructor implementation-->
<xsl:template name="callActionExprInitConst" match="src:constructor//src:init/src:expr[count(src:call)=1 and not(preceding-sibling::*[1][self::src:annotation[src:name='Chain' or src:name='CallBehavior']]) and not(ancestor::src:catch or ancestor::src:while or parent::src:for)]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:variable name="methodName" select="src:call/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionExprInit', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction8End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all method invocation block -->
<xsl:template name="callActionBlock" match="src:function//src:expr_stmt[not(preceding-sibling::*[1][src:name='Chain' or src:name='CallBehavior']) and src:expr[not(ancestor::src:catch) and count(src:call)=1]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<xsl:variable name="methodName" select="src:expr/src:call/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionBlock', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction9End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all method invocation block in constructor implementation -->
<xsl:template name="callActionBlockCons" match="src:constructor//src:expr_stmt[not(preceding-sibling::*[1][src:name='Chain' or src:name='CallBehavior']) and src:expr[not(src:call[src:name='super']) and not(ancestor::src:catch) and count(src:call)=1] and not(src:expr/src:call[src:name='this'])]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<xsl:variable name="methodName" select="src:expr/src:call/src:name"/>
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', $methodName, '-callActionBlockCons', count(preceding::src:call[src:name=$methodName]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction10End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a method's impl -->
<xsl:template name="declStatementArray" match="src:function//src:decl_stmt[src:decl[src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array-', count(preceding::src:decl[src:name/src:index]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction11End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a method's impl (different array declaration) -->
<xsl:template name="declStatementArraySecondWay" match="src:function//src:decl_stmt[src:decl[src:type/src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array1-', count(preceding::src:decl[src:type/src:name/src:index][src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction12End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all array declarations and initialization statements in a constructor's impl -->
<xsl:template name="declStatementArrayConstructor" match="src:constructor//src:decl_stmt[src:decl[src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array-', count(preceding::src:decl[src:name/src:index]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction13End</name></annotation>
</xsl:template>

<!-- @CallAction annotation around all array declarations and initialization statements in a constructor's impl (different array declaration) --> 
<xsl:template name="declStatementArraySecondWayConstructor" match="src:constructor//src:decl_stmt[src:decl[src:type/src:name/src:index][src:init]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-array1-', count(preceding::src:decl[src:type/src:name/src:index][src:init]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction14End</name></annotation>
</xsl:template>

<!-- added to avoid @CallAnnotation application inside catch blocks -->
<xsl:template match="src:catch/*">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

<!-- adds @CallAction annotation around line with assignment and no method invocation in constructor-->
<xsl:template name="callActionAssignmentCons" match="src:constructor//src:expr_stmt[not(preceding-sibling::*[1][src:name='Chain' or src:name='CallBehavior']) and src:expr[not(src:call) and not(ancestor::src:catch)]]" >  
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', '-callActionAssignment', count(preceding::src:expr[not(src:call)]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction15End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around line with assignment and no method invocation in method-->
<xsl:template name="callActionAssignmentFunc" match="src:function//src:expr_stmt[not(preceding-sibling::*[1][src:name='Chain' or src:name='CallBehavior']) and src:expr[not(src:call) and not(ancestor::src:catch)]]" >  
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-', '-callActionAssignment', count(preceding::src:expr[not(src:call)]))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction16End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all assignment in constructor -->
<xsl:template name="declStatementVarCons" match="src:constructor//src:decl_stmt[src:decl[src:name][src:init[not(child::*[1][self::src:annotation[src:name='CallBehavior']]) and not(child::*[1][self::src:annotation[src:name='Chain']])][not(src:expr[src:operator[.='new']])]]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-var-', count(preceding::src:decl[src:name[not(descendant::src:index)]]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction17End</name></annotation>
</xsl:template>

<!-- adds @CallAction annotation around all assignment in method -->
<xsl:template name="declStatementVarFunc" match="src:function//src:decl_stmt[src:decl[src:name][src:init[not(child::*[1][self::src:annotation[src:name='CallBehavior']]) and not(child::*[1][self::src:annotation[src:name='Chain']])][not(src:expr[src:operator[.='new']])]]]" >   
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>CallAction</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:text>call-</xsl:text><xsl:value-of select="concat($ancestorName, '-decl_init-var-', count(preceding::src:decl[src:name[not(descendant::src:index)]]/src:init))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>CallAction17End</name></annotation>
</xsl:template>
<!-- @Decision -->

<!-- adds @Decision annotation around if block in method's impl (not closing if the block is inside an else if body with else branch too ) -->
<xsl:template name="addDecisionAnnotationIf" match="src:function//src:if[not(ancestor::*[1][self::src:elseif]) and not(descendant::src:throw) and not(descendant::src:return)]">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Decision</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-if', count(preceding::src:if) + count(ancestor::src:if))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<!-- <xsl:if test="not(ancestor::src:elseif/following-sibling::*[1][self::src:else])">
 --><annotation>@<name>DecisionEnd</name></annotation>
<!-- </xsl:if> -->
</xsl:template>

<!-- adds @Decision annotation around if block in method's impl (not closing if the block is inside an else if body with else branch too ) -->
<xsl:template name="addDecisionAnnotationIfCons" match="src:constructor//src:if[not(ancestor::*[1][self::src:elseif]) and not(descendant::src:throw) and not(descendant::src:return)]">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Decision</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-if', count(preceding::src:if) + count(ancestor::src:if))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>DecisionEnd</name></annotation>
</xsl:template>

<!-- add @IfEval annotation -->
<xsl:template name="addIfEval" match="src:if[not(ancestor::*[1][self::src:elseif]) and not(descendant::src:throw) and not(descendant::src:return)]/src:condition">
<xsl:text>(</xsl:text><annotation>@<name>IfEval</name></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>IfEvalEnd</name></annotation>
<xsl:text>)</xsl:text>
</xsl:template>

<!-- @Loop -->
<!-- adds @While annotation for while loop in method's impl -->
<xsl:template name="addWhileAnnotation" match="src:function//src:while[not(descendant::src:throw) and not(descendant::src:return) and not(ancestor::src:for) and not(ancestor::src:while)]">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>LoopEnd</name></annotation>
</xsl:template>

<!-- adds @While annotation for while loop in constructor's impl -->
<xsl:template name="addWhileAnnotationConstructor" match="src:constructor//src:while[not(descendant::src:throw) and not(descendant::src:return) and not(ancestor::src:for) and not(ancestor::src:while)]">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-while', count(preceding::src:while) + count(ancestor::src:while))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>LoopEnd</name></annotation>
</xsl:template>

<!-- adds @Loop annotation in method's impl -->
<xsl:template name="addForAnnotation" match="src:function//src:for[not(descendant::src:throw) and not(descendant::src:return) and not(ancestor::src:for) and not(ancestor::src:while)]">
<xsl:variable name="ancestorName" select="ancestor::src:function/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>LoopEnd</name></annotation>
</xsl:template>

<!-- adds @Loop annotation in constructor's impl -->
<xsl:template name="addForAnnotationConstructor" match="src:constructor//src:for[not(descendant::src:throw) and not(descendant::src:return) and not(ancestor::src:for) and not(ancestor::src:while)]">
<xsl:variable name="ancestorName" select="ancestor::src:constructor/src:name" />
<annotation>@<name>Loop</name><argument_list>(<argument><expr><name>id</name><operator>=</operator><literal type="string">&quot;<xsl:value-of select="concat($ancestorName, '-for', count(preceding::src:for) + count(ancestor::src:for))"/>&quot;</literal></expr></argument>)</argument_list></annotation>
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
<annotation>@<name>LoopEnd</name></annotation>
</xsl:template>


<!-- identity copy -->
<xsl:template match="@*|node()">
<xsl:copy><xsl:apply-templates select="@*|node()"/></xsl:copy>
</xsl:template>

</xsl:stylesheet>