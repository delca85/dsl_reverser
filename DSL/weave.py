from os import listdir, system
from os.path import isfile, join, isdir
import sys, subprocess

ASPECTJLIB = "lib/aspectjrt-1.8.10.jar"
ASPECTFILE = "src/it/unimi/reverser/dsl/aspects/ActivityDiagramAspect.aj"
BINSRC = "binSrc"
DIR = "/Users/bibi/Documents/UniMi/CloudStation/TesiM/DSL/DSL/"   #dir where ajc is executed

def list_file(path):
	for f in listdir(path):
		if isfile(join(path, f)) and f.endswith('java'):
			yield (join(path, f))
		if isdir(join(path,f)):
			for subf in list_file(join(path, f)):
				yield subf

if __name__ == '__main__':
	classpathList = ['.', ASPECTJLIB, BINSRC]
	if len(sys.argv) >= 3:
		for f in listdir(sys.argv[2]):
			classpathList.append(sys.argv[2] + f)
	classpath = ':'.join(classpathList)
	onlyfiles = [i for i in list_file(sys.argv[1]+"/src_beautified")]
	print "Compiling..."
	args = ['ajc', '-cp', classpath, '-1.8', ASPECTFILE] + onlyfiles
	subprocess.call(args, cwd=DIR)