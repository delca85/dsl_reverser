#!/bin/bash

ATASPECTJ=/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/ataspectj.jar
ATJAVA=/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/atjava.jar
BCEL=/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/bcel-5.2.jar
ASPECTJ=/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/aspectjrt.jar
REVERSER=/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/reverser.jar
COMMONS=/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/commons-logging-1.1.2.jar:/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/commons-configuration-1.9.jar:/home/reverser_guest/sharedFolder/sf_Tesi/updatedSVN/trunk/reverserRSA/pkg_installation_scripts/lib/commons-lang-2.6.jar

BOOTCP=.:$COMMONS:$ATJAVA:$ATASPECTJ

CLASSPATH=$CLASSPATH:$BOOTCP:$BCEL:$ASPECTJ:$REVERSER
java -Xbootclasspath/p:$BOOTCP ataspectj.AtAspectjMain -verbose -debug -Xbootclasspath/p:$BOOTCP $*