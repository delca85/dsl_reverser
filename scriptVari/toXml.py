#!/usr/bin/python

# convert all java files in input directory, to xml format through srcml

import os, sys

def main(argv):
	if len(argv) < 2:
		print 'usage: toXml <relativeInputDirectoryName> <relativeOutputDirectoryName>'
		return
	inputDir = argv[0]
	outputDir = argv[1]
	for file in os.listdir(inputDir):
	    if file.endswith(".java"):
	        print('processing file ' + file + '...')
	        command = 'srcml ' + inputDir + file + ' -o ' + outputDir + file[:-4] + 'xml'
	        os.system(command)


if __name__ == "__main__":
   main(sys.argv[1:])