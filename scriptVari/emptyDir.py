#!/usr/bin/python

# remove all files in a directory and its subdir
import os, sys

def main(argv):
	if len(argv) < 1:
		print 'usage: emptyDir <AbsoluteBPathOfDirToBeEmpty>'
		return
	emptyDir = argv[0]
	for subdir in os.listdir(emptyDir):
	    if subdir in ['beautified', 'transformed', 'xml']:
	        response = raw_input('removing file in ' + emptyDir + subdir + '/: ok? ')
	        if response == 'y':
	        	command = 'rm -r ' + emptyDir + subdir
	        	os.system(command)
	    if subdir == '.DS_Store':
			command = 'rm ' + emptyDir + subdir
			os.system(command)	    	


if __name__ == "__main__":
   main(sys.argv[1:])