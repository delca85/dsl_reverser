package it.unimi.reverser.dsl;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import it.unimi.reverser.dsl.util.MethodsChainingExtractor;
import it.unimi.reverser.dsl.util.Util;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com
 * This class, set as listener of ActivityDiagramAspect, makes several actions: stores all method declared by each class
 * in an hashmap and invokes the class with main arg passed as argument and, through the aspect,
 * populates its HashMap field in order to store all the methodsAndConstructors invocation bound to the variable invoking them.
 * This script assumes to be executed after Beautifier and when the aspect has been already woven.
 * This assumption is used when the classLoader is instantiated, in using reflection to retrieve declared methodsAndConstructors
 * and in running main class with aspect woven.
 * Once the main class has been invoked, this script must parse java source code, in order to retrieve local variable
 * names. The goal is bounding class of each variable, local variable name and method called.
 */
public class AspectListener {
    private URLClassLoader classLoader = null;
    private String srcJavaDir;
    private String xsltTransformation;

    /*
       map to store methodsAndConstructors invoked during classes execution:
       keys are files*lineNumber, values are strings lists, formatted like this 1*2:
       1. class of the instance that executes method / constructor
       2. method / constructor executed signature
     */
    public Map<String, List<String>> methodsAndConstructors = new HashMap<>();


    /*
     map where to store all the methodsAndConstructors declared per class compiled:
     keys are classes name, values
      */
    private HashMap<String, List<String>> activityDiagramMethods = new HashMap<>();

    /*
        map where to store methodsAndConstructors invocation to be annotated by @CallBehavior annotation.
        keys are filenames, values are list of invocation strings.
     */
    public HashMap<String, List<String>> singleMethodInvocations = new HashMap<>();

    /*
    map where methodsAndConstructors chain will be stored. keys are filename*line and values
    beginChainIndex*endChainIndex*chainBeforeLastMethod*methodName
    info in this map, will be analyzed with info in methodsAndConstructors map in order to decide how to apply @Chain annotation
     */
    public HashMap<String, List<String>> chainMethods = new HashMap<>();

    //classLoader is set
    public AspectListener(String javaSrc, String xsltTransformation){
        File dir = new File(javaSrc + "/" + Beautifier.src_beautified);
        this.srcJavaDir = new File(javaSrc + "/" + Beautifier.src_beautified).getAbsolutePath();
        this.xsltTransformation = xsltTransformation;
        System.out.println("[AspectListener]Dir with java source code: " + this.srcJavaDir);
        System.out.println("[AspectListener]Dir where XSLTTranformations will be write: " + this.xsltTransformation);
        try {
            URL[] cp = {dir.toURI().toURL()};
            classLoader = URLClassLoader.newInstance(cp);
        } catch (MalformedURLException e) {
            System.out.println("[AspectListener]Dir " + dir + " not found.");
            e.printStackTrace();
        }
        setContext();
    }


    // method that invokes itself
    private void setContext(){
        try{
            Class<?> c=Class.forName("it.unimi.reverser.dsl.aspects.ActivityDiagramAspect");
            Method m=c.getMethod("setContext", AspectListener.class);
            m.invoke(null,this);
        }catch(Exception e){
            System.out.println("[AspectListener]Method setContext not found in class " +
                    "it.unimi.reverser.dsl.aspects.ActivityDiagramAspect");
            e.printStackTrace();
        }
    }

    /*
     method performing these actions:
     1. creating a list per class of all the methodsAndConstructors declared. These lists will populate activityDiagramMethods
     2. invoking main method of the class passed as argument
      */
    public static void main(String[] args){
        if (args.length != 3){
            System.out.println("[AspectListener]Usage: it.unimi.reverser.dsl.AspectListener srcJavaDir XSLTtransformationDir MainJavaClass");
            System.exit(0);
        }
        AspectListener aspectListener = new AspectListener(args[0], args[1]);
        aspectListener.addMethodsToActivityDiagram(new File(aspectListener.srcJavaDir));
        Util.printMap("AspectListener", "activityDiagramMethods", aspectListener.activityDiagramMethods);
        try {
            aspectListener.invokeMain(args[2], Util.copyOfRange(args, 3, args.length));
        } catch(RuntimeException e){
            e.printStackTrace();
        }
        aspectListener.filterMethods(new File(aspectListener.srcJavaDir), new TreeSet(aspectListener.methodsAndConstructors.keySet()));
        Util.printMap("AspectListener", "methodsAndConstructors", aspectListener.methodsAndConstructors);
        aspectListener.fillMethodsChainingAndSingleInvocations(new File(aspectListener.srcJavaDir));
        aspectListener.filterToCallBehavior(new File(aspectListener.srcJavaDir));
        Util.printMap("AspectListener", "singleMethodInvocations", aspectListener.singleMethodInvocations);
        Util.printMap("AspectListener", "chainMethods", aspectListener.chainMethods);

    }

    // add methodsAndConstructors of each class in binPath to the hashmap activityDiagrams
    private void addMethodsToActivityDiagram(File binPath) {
        String packageName;
        for (File f: binPath.listFiles()){
            if (f.isDirectory())
                this.addMethodsToActivityDiagram(f);
            else {
                String fileName = (f.getName().contains("$")) ? f.getName().substring(0, f.getName().indexOf("$")) +
                        f.getName().substring(f.getName().lastIndexOf(".")) : f.getName();
                packageName = f.getAbsolutePath().replace(f.getName(), "").
                        replace(srcJavaDir, "").replace("/", ".").replaceFirst("\\.", "");
                if (fileName.endsWith("class")) {
                    String className = packageName + fileName.replace(".class", "");
                    try {
                        File dir = new File(srcJavaDir);
                        URL[] cp = {dir.toURI().toURL()};
                        URLClassLoader classLoader = URLClassLoader.newInstance(cp);
                        Class clazz = classLoader.loadClass(className);
                        Method methods[] = clazz.getDeclaredMethods();
                        List<Method> methodsList = Arrays.asList(methods);
                        activityDiagramMethods.put(className, methodsList.stream().filter(method ->
                                !method.toGenericString().contains("ajc$"))
                                .map(method -> method.toGenericString().replaceAll("\\sthrows\\s.*", "").
                                        replace(Modifier.toString(method.getModifiers()), "").substring(1)
                                .replace(className+".", "")).collect(Collectors.toList()));
                        Constructor constructors[] = clazz.getDeclaredConstructors();
                        List<Constructor> constructorList = Arrays.stream(constructors).
                                filter(c -> c.getParameterCount() > 0 || !Util.isDefaultConstructor(c,
                                        f.getAbsolutePath().substring(0, f.getAbsolutePath().lastIndexOf("/") + 1)
                                                + fileName)).collect(Collectors.toList());
                        activityDiagramMethods.get(className).addAll(constructorList.stream().map(constructor ->
                                constructor.toGenericString().replaceAll("\\sthrows\\s.*", "")
                                .replace(Modifier.toString(constructor.getModifiers()), "").substring(1)
                                        .replace(className+".", "")).collect(Collectors.toList()));
                    } catch (MalformedURLException e) {
                        System.out.println("[AspectListener]Path " + srcJavaDir + " not found.");
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        System.out.println("[AspectListener]Class " + className + " not found in path " + srcJavaDir);
                    }
                }
            }

        }
    }

    // method that stores in methodsAndConstructors map info retrieved by aspect when a method has been invoked
    public void addMethod(String clazz, String method, String fileNameLineNumber){
        String value = clazz + "*" + method;
        Util.addToMap(fileNameLineNumber, value, methodsAndConstructors);
    }

    // method that invokes main of class passed as first argument
    private void invokeMain(String mainClass, String[] args) {
        try{
            Class<?> c=getClass(mainClass);
            Method main=c.getMethod("main",String[].class);
            main.invoke(null,new Object[]{args});
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // classloader loads class known the name
    public Class<?> getClass(String className) {
        Class<?> c;
        try {
            c = classLoader.loadClass(className);
        }catch (ClassNotFoundException e) {
            throw new RuntimeException("Class Not Found : " + e.getMessage());
        }
        return c;
    }

    // given a class name, this tells you if this class is among the ones analyzed
    public boolean isInResources(String className){
        return activityDiagramMethods.containsKey(className);
    }

    // this invokes an instance of util.MethodsChainingExtractor, in order to populate methodsChaining and
    // singleMethodInvocations maps
    private void fillMethodsChainingAndSingleInvocations(File path) {
        for (File f : path.listFiles()) {
            if (f.isDirectory())
                fillMethodsChainingAndSingleInvocations(f);
            else if (f.getName().endsWith(".java")) {
                try {
                    FileInputStream in = new FileInputStream(path + "/" + f.getName());
                    CompilationUnit cu = JavaParser.parse(in);
                    MethodsChainingExtractor methodVisitor = new MethodsChainingExtractor(path.getCanonicalPath()
                            .replace(srcJavaDir+"/", "") +"/"+f.getName(), this);
                    methodVisitor.visit(cu, null);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.out.println("[AspectListener]Unable to open file " + f.getName());
                    System.exit(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    // this filters and order methodsAndConstructors in methodsAndConstructors map
    private void filterMethods(File path, Set<String> key) {
        for (File f : path.listFiles()) {
            if (f.isDirectory())
                filterMethods(f, key);
            else if (f.getName().endsWith(".java")) {
                int lineNumber;
                String line, method, methodSig, clazz;
                List<String> newValues;
                for (String fileLine : key) {
                    String pathPrefix = path.toString().replace(srcJavaDir, "").length() > 1 ?
                            path.toString().replace(srcJavaDir, "").substring(1) + "/" : "/";
                    if (!fileLine.contains(pathPrefix + f.getName()))
                        continue;
                    try {
                        lineNumber = Integer.parseInt(fileLine.split("\\*")[1]);
                        line = Files.lines(Paths.get(path + "/" + f.getName())).skip(lineNumber - 1).findFirst().get();
                        List<Integer> methodsIndexes = new ArrayList<>();
                        newValues = new ArrayList<>();
                        for (String classMethod : methodsAndConstructors.get(fileLine)) {
                            clazz = classMethod.split("\\*")[0];
                            methodSig = classMethod.split("\\*")[1];
                            method = (methodSig.substring(methodSig.indexOf(" ") + 1,
                                    methodSig.lastIndexOf('(')));
                            if (clazz.equals("java.lang.reflect.Constructor")) {
                                clazz = method;
                                method = method.split("\\.")[method.split("\\.").length - 1];
                                methodSig = methodSig.contains(" ") ? methodSig.split("\\s")[1] : methodSig;
                            }
                            else
                                clazz = Util.findClassWithDeclaredMethod(this, clazz, methodSig);
                            int indexInLine = line.length();
                            while (line.substring(0, indexInLine).lastIndexOf(method + "(") >= 0
                                    && methodsIndexes.contains(line.substring(0, indexInLine).lastIndexOf(method + "(")))
                                indexInLine = line.substring(0, indexInLine).lastIndexOf(method + "(");
                            if (line.substring(0, indexInLine).lastIndexOf(method + "(") < 0) continue;
                            methodsIndexes.add(line.substring(0, indexInLine).lastIndexOf(method + "("));
                            newValues.add(clazz+"*"+methodSig+"*"+line.substring(0, indexInLine).lastIndexOf(method + "("));
                        }
                        methodsAndConstructors.put(fileLine, newValues);
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("[AspectListener]Error in reading file " + f.getName());
                        System.exit(0);
                    }
                }
            }

        }
    }

    // method that extracts info from singleMethodInvocations map, compare it with the one in methodsAndConstructors and,
    // if the method is activityDiagramMethods, launch xsl transformations file writing
    private void filterToCallBehavior(File path){
        for (File f : path.listFiles()){
            if (f.isDirectory())
                filterToCallBehavior(f);
            else if (f.getName().endsWith(".java")){
                List<String> varMethodListSingle, varMethodListChain;
                varMethodListSingle = new ArrayList<>();
                String var, method, clazz, methodSig, prologue, epilogue;
                String pathPrefix = path.toString().replace(srcJavaDir, "").length() > 1 ?
                        path.toString().replace(srcJavaDir, "").substring(1) + "/" : "/";
                for (String key : singleMethodInvocations.keySet().stream()
                        .filter(fileLine -> fileLine.contains(pathPrefix + f.getName())).collect(Collectors.toList())) {
                    for (String varMethod : singleMethodInvocations.get(key)) {
                        var = varMethod.split("\\*")[1];
                        // any comments inside var
                        var = var.contains("\n") ? var.split("\n")[var.split("\n").length-1] : var;
                        method = varMethod.split("\\*")[2].split("@")[0];
                        method = method.replaceAll(",\\s+", ",");
                        if (method.startsWith("new ")) {
                            method = method.split("\\s")[1].replaceAll("\\(+[^)]*\\)+", "");
                            var = "new";
                        }
                        String keyForMethodsAndConstructors = key.replace(srcJavaDir+"/", "");
                        if (methodsAndConstructors.containsKey(keyForMethodsAndConstructors))
                            for (String classMethodMapAspect : methodsAndConstructors.get(keyForMethodsAndConstructors)) {
                                List<Integer> indexAlreadyTaken = new ArrayList<>();
                                clazz = classMethodMapAspect.split("\\*")[0];
                                methodSig = classMethodMapAspect.split("\\*")[1];
                                if (activityDiagramMethods.containsKey(clazz) && ((var.equals("new") &&
                                        methodSig.contains(clazz+"("))
                                        || (!var.equals("new") && methodSig.contains(" " + method + "(")))
                                        && activityDiagramMethods.get(clazz).contains(methodSig)) {
                                    if (!indexAlreadyTaken.contains(activityDiagramMethods.get(clazz).indexOf(methodSig))){
                                        indexAlreadyTaken.add(activityDiagramMethods.get(clazz).indexOf(methodSig));
                                        varMethodListSingle.add(var + "*" + method + "*" + clazz + "*" + methodSig);
                                        break;
                                    }
                                }
                            }
                    }
                }
                varMethodListChain = new ArrayList<>();
                for (String key : chainMethods.keySet().stream()
                        .filter(fileLine -> fileLine.contains(pathPrefix + f.getName())).collect(Collectors.toList())){
                    for (String chain : chainMethods.get(key)){
                        var = chain.split("\\*")[2];
                        // any comments inside var
                        var = var.contains("\n") ? var.split("\n")[var.split("\n").length-1] : var;
                        String methodChain = chain.split("\\*")[3];
                        String[] chainMethods = methodChain.split("\\/");
                        int indexInChain = Integer.parseInt(chain.split("\\*")[0])+1;       //first method in chain index
                        String chainString = var;
                        if (methodsAndConstructors.containsKey(key)) {
                            for (String chainMethod : chainMethods){
                                for (String classMethodMapAspect : methodsAndConstructors.get(key)){
                                    if (Integer.parseInt(classMethodMapAspect.split("\\*")[2]) == indexInChain){
                                        clazz = classMethodMapAspect.split("\\*")[0];
                                        methodSig = classMethodMapAspect.split("\\*")[1];
                                        chainString += "*" + methodSig;
                                        if (activityDiagramMethods.containsKey(clazz) &&
                                                activityDiagramMethods.get(clazz).contains(methodSig))
                                            chainString += "@" + clazz;
                                        indexInChain = Integer.parseInt(chainMethod.split("\\@")[1]) +1;
                                        break;
                                    }
                                }
                            }
                            varMethodListChain.add(chainString);
                        }
                    }
                }
                if (varMethodListSingle.size() > 0 || varMethodListChain.size() > 0){
                    String transformationFile = "callbehaviorChain_" + f.getAbsolutePath().replace(srcJavaDir+"/", "").
                            replace("/", "_").replace(".java", ".xsl");
                    Writer writer = null;
                    prologue = "<xsl:stylesheet\n" +
                            "xmlns=\"http://www.srcML.org/srcML/src\"\n" +
                            "xmlns:src=\"http://www.srcML.org/srcML/src\"\n" +
                            "xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" \n" +
                            "exclude-result-prefixes=\"src\"\n" +
                            "version=\"1.0\">\n";
                    epilogue = "\n<!-- identity copy -->\n" +
                            "<xsl:template match=\"@*|node()\"><xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy></xsl:template>\n" +
                            "\n" + "</xsl:stylesheet>";
                    try {
                        writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(xsltTransformation+"/"+ transformationFile),
                                "utf-8"));
                        writer.write(prologue);
                        if (varMethodListSingle.size() > 0)
                            writeCallBehaviorFile(path + "/" + f.getName(), varMethodListSingle, writer);
                        if (varMethodListChain.size() > 0)
                            writeChainFile(path+"/" + f.getName(), varMethodListChain, writer);
                        writer.append(epilogue);
                        writer.close();
                        System.out.println("[AspectListener]Transformation file " + xsltTransformation+"/"+ transformationFile + " written");
                    } catch (IOException e) {
                        e.printStackTrace();
                        System.out.println("[AspectListener]Error writing file " + transformationFile);
                        System.exit(0);
                    }
                }
            }
        }
    }


    /*
     method that retrieves information needed by callBehavior.xsl, writes that file and launches srcml in order to:
     1. convert java source code to xml
     2. apply xslt transformation
     3. convert back to java source code
      */
    private void writeCallBehaviorFile(String fileName, List<String> varMethodList, Writer writer) {
        System.out.println("[AspectListener]Writing callBehavior " + fileName);
        List<String> varNameList = new ArrayList<>();
        for (String singleInvocation : varMethodList) {
            String varName, methodName, clazz, behavior, methodSig, retType, args = "";
            varName = singleInvocation.split("\\*")[0];
            methodName = singleInvocation.split("\\*")[1].split("\\@")[0];
            clazz = singleInvocation.split("\\*")[2].substring(singleInvocation.split("\\*")[2].lastIndexOf(".")+1);
            if (varName.equals("new")) {
                methodSig = singleInvocation.split("\\*")[singleInvocation.split("\\*").length - 1];
                methodSig = methodSig.substring(methodSig.lastIndexOf('.'));
                retType = clazz;
            }
            else {
                methodSig = singleInvocation.split("\\*")[3];
                retType = methodSig.split("\\s")[0];
            }
            if (retType.contains("."))
                retType = retType.substring(retType.lastIndexOf(".")+1);
            if (methodSig.contains(","))        // two or more arguments
                for (String arg : methodSig.substring(methodSig.lastIndexOf("(")+1, methodSig.lastIndexOf(")")).split(",")){
                    if (arg.contains("."))
                        args += arg.substring(arg.lastIndexOf(".")+1) + "-";
                    else
                        args += arg + "-";
                }
            else {                  // one or zero arguments
                args = methodSig.substring(methodSig.lastIndexOf("(")+1, methodSig.lastIndexOf(")"));
                if (args.contains("."))
                    args = args.substring(args.lastIndexOf(".")+1);
                if (args.length()>0)
                    args += "-";
            }
            behavior = clazz + "." + methodName + "." + retType + "[" + args + "]";
            if (!varNameList.contains(varName+ "*" + methodName + "*" + behavior))
                varNameList.add(varName + "*" + methodName + "*" + behavior);
        }
        writeFile("callBehavior", writer, varNameList);

    }


    /*
     method that retrieves information needed by chin.xsl, writes that file and launches srcml in order to:
     1. convert java source code to xml
     2. apply xslt transformation
     3. convert back to java source code
      */
    private void writeChainFile(String fileName, List<String> chainListArg, Writer writer){
        System.out.println("[AspectListener]Writing chain " + fileName);
        List<String> chainList = new ArrayList<>();
        for (String chain : chainListArg){
            String varName, methodName, clazz, behavior="{", retType, args="", listMember;
            varName = chain.split("\\*")[0];
            listMember = varName;
            for (String methodSig : Arrays.asList(chain.split("\\*")).subList(1, chain.split("\\*").length)) {
                if (methodSig.contains(" "))
                    methodName = methodSig.split("@")[0].split("\\s")[1].replaceAll("\\([^\\)]*\\)", "");
                else
                    methodName = methodSig.split("\\@")[0].split("\\.")
                            [methodSig.split("\\@")[0].split("\\.").length - 1].replaceAll("\\([^\\)]*\\)", "");
                if (methodSig.contains("@")) {                  //method in activityDiagramMethods map
                    clazz = methodSig.split("\\@")[1].substring(methodSig.split("\\@")[1].lastIndexOf(".") + 1);
                    methodSig = methodSig.replace("@", "");
                    retType = methodSig.contains(" ")? methodSig.split("\\s")[0] : methodName;
                    if (retType.contains("."))
                        retType = retType.substring(retType.lastIndexOf(".") + 1);
                    if (methodSig.contains(","))        // two or more arguments
                        for (String arg : methodSig.substring(methodSig.lastIndexOf("(") + 1, methodSig.lastIndexOf(")")).split(",")) {
                            if (arg.contains("."))
                                args += arg.substring(arg.lastIndexOf(".") + 1) + "-";
                            else
                                args += arg + "-";
                        }
                    else {                  // one or zero arguments
                        args = methodSig.substring(methodSig.lastIndexOf("(")+1, methodSig.lastIndexOf(")"));
                        if (args.contains("."))
                            args = args.substring(args.lastIndexOf(".")+1);
                        if (args.length()>0)
                            args += "-";
                    }

                    behavior += "\"" + clazz + "." + methodName + "." + retType + "[" + args + "]" + "\", ";
                }
                else behavior += "\"\", ";
                listMember += "*" + methodName;
            }
            behavior = behavior.substring(0, behavior.length()-2) + "}";
            chainList.add(listMember + "*" + behavior);
        }
        if (chainList.size() > 0)
            writeFile("chain", writer, chainList);
    }


    // creates transformation file, according to variable passed as arguments

    private void writeFile(String type, Writer writer, List<String> varNameList) {
        String template0, id0, template1, id1, id, varName, methodName, behavior, argumentCounterString="";
        // needed to avoid identical templates in transformation file
        List<String> alreadyWrote = new ArrayList<>();
        int argumentCounter;
        Pattern argumentCounterPat = Pattern.compile("\\[([^-]+(-))*\\]");
        Matcher argumentCounterMatcher;
        try{
            switch (type){
                case "callBehavior":
                    for (int i = 0; i < varNameList.size(); i++){
                        String info = varNameList.get(i);
                        varName = info.split("\\*")[0];
                        methodName = info.split("\\*")[1];
                        behavior = info.split("\\*")[2];
                        argumentCounterMatcher = argumentCounterPat.matcher(behavior);
                        if (argumentCounterMatcher.find()) {
                            argumentCounter = (int) argumentCounterMatcher.group().
                                    codePoints().filter(ch -> ch == '-').count();
                            argumentCounterString = "[src:argument_list[count(child::src:argument)=" + argumentCounter + "]]";
                        }
                        if (alreadyWrote.contains(varName+"*"+methodName+"*"+behavior))
                            continue;
                        alreadyWrote.add(varName+"*"+methodName+"*"+behavior);
                        id = "<xsl:value-of select=\"concat('"+ methodName +"', '-', count(preceding::src:name[.='"+ methodName +"']))\"/>";
                        if (varName.equals("new")){                     //constructor
                            template0 = "\n<!-- add @CallBehavior for constructor " + methodName + "-->\n";;
                            template0 += "<xsl:template name=\"constructorCall" + i + "expr" +
                                    "\" match=\"src:expr[src:operator='=' or ancestor::src:init and not(ancestor::src:catch)][src:call[src:name='" +
                                    methodName + "']" + argumentCounterString + "]/src:operator[.='new']\">";
                            id0 =   "<annotation>@<name>CallBehavior</name><argument_list>(" +
                                    "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">\"" +
                                    id + "-expr\"</literal></expr></argument>, " +
                                    "<argument><expr><name>behavior</name> <operator>=</operator> <literal type=\"string\">\"" +
                                    behavior + "\"</literal></expr></argument>)</argument_list></annotation>" +
                                    "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/>" +
                                    "</xsl:copy><xsl:text> </xsl:text><xsl:value-of select=\"following-sibling::src:call\"/>\n" +
                                    "<annotation>@<name>CallBehaviorEnd</name></annotation>\n" +
                                    "</xsl:template>";
                            template1 = "\n<!-- delete call to " + methodName + " in order to avoid duplicated-->\n";;
                            template1 += "<xsl:template name=\"constructorCallDelete" + varNameList.indexOf(info) + "\" match=\"" +
                                    "src:expr[src:operator='='or ancestor::src:init][src:operator[.='new']]/src:call[src:name[.='" + methodName +"']]" +
                                    argumentCounterString + "\">" +
                                    "<xsl:if test=\"(ancestor::src:catch)\">\n" +
                                    "<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n"+
                                    "</xsl:if>\n</xsl:template>";
                            id1 = "\n<!-- add @CallBehavior for constructor " + methodName + " block version-->\n";;
                            id1 += "<xsl:template name=\"constructorCall" + i + "block"+
                                    "\" match=\"src:expr_stmt[not(ancestor::src:catch)][src:expr[src:operator='new']" +
                                    "[src:call[src:name='" + methodName + "']" + argumentCounterString + "]]\">" +
                                    "<annotation>@<name>CallBehavior</name><argument_list>(" +
                                    "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">\"" +
                                    id + "-block\"</literal></expr></argument>, " +
                                    "<argument><expr><name>behavior</name> <operator>=</operator> <literal type=\"string\">\"" +
                                    behavior + "\"</literal></expr></argument>)</argument_list></annotation>" +
                                    "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/>" +
                                    "</xsl:copy>\n<annotation>@<name>CallBehaviorEnd</name></annotation>\n" +
                                    "\n</xsl:template>";
                        }
                        else {
                            if (varName.contains("new") || varName.contains("((")){             // (Object obj = new Object(..)).method(..) or cast
                                int callNumber = 1;
                                if (varName.contains("new")) {
                                    varName = varName.split("=")[0].substring(1).trim();
                                    callNumber = 2;
                                }
                                else
                                    varName = varName.split("\\)")[varName.split("\\)").length-1].trim();
                                template0 = "\n<!-- add @CallBehavior for " + varName + "*" + methodName + " expression version-->\n";
                                template0 += "<xsl:template name=\"methodCall" + i + "expr" +
                                        "\" match=\"src:expr[count(src:call)="+ callNumber +" and count(src:operator[.='+' or .='/' or .='-' or .='*'])=0]" +
                                        "[(ancestor::src:argument_list) or (ancestor::src:init) and not(ancestor::src:catch)]" +
                                        "[src:name='" + varName + "'][src:call[src:name='" +
                                        methodName + "']" + argumentCounterString + "]\">";
                                id0 =   "<annotation>@<name>CallBehavior</name><argument_list>(" +
                                        "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">\"" +
                                        varName + "-" + id + "\"</literal></expr></argument>, " +
                                        "<argument><expr><name>behavior</name> <operator>=</operator> <literal type=\"string\">\"" +
                                        behavior + "\"</literal></expr></argument>)</argument_list></annotation>" +
                                        "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                        "<annotation>@<name>CallBehaviorEnd</name></annotation>\n" +
                                        "</xsl:template>";
                                template1 = "\n<!-- add @CallBehavior for " + varName + "*" + methodName + " block version -->\n";
                                template1 += "<xsl:template name=\"methodCall" + i + "block" +
                                        "\" match=\"src:expr_stmt[not(ancestor::src:catch)][src:expr[count(src:call)=" + callNumber + "]" +
                                        "[src:name='" + varName +"'][src:call[src:name='" + methodName + "']" + argumentCounterString + "]]\">";
                                id1 =   "<annotation>@<name>CallBehavior</name><argument_list>(" +
                                        "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">\"" +
                                        varName + "-" + id + "\"</literal></expr></argument>, " +
                                        "<argument><expr><name>behavior</name> <operator>=</operator> <literal type=\"string\">\"" +
                                        behavior + "\"</literal></expr></argument>)</argument_list></annotation>" +
                                        "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                        "<annotation>@<name>CallBehaviorEnd</name></annotation>\n" +
                                        "</xsl:template>";
                            }
                            else {                          // classic var.method
                                template0 = "\n<!-- add @CallBehavior for " + varName + "*" + methodName + " expression version-->\n";
                                if (varName.equals(""))
                                    template0 += "<xsl:template name=\"methodCall" + i + "expr" +
                                            "\" match=\"src:expr[count(src:operator[.='+' or .='/' or .='-' or .='*'])=0][" +
                                            "(ancestor::src:argument_list)" +
                                            "or (ancestor::src:init) and not(ancestor::src:catch)][src:call[src:name='" + methodName + "']"
                                            + argumentCounterString + "]\">";
                                else
                                    template0 += "<xsl:template name=\"methodCall" + i + "expr" +
                                        "\" match=\"src:expr[count(src:operator[.='+' or .='/' or .='-' or .='*'])=0][" +
                                            "(ancestor::src:argument_list)" +
                                        "or (ancestor::src:init) and not(ancestor::src:catch)][src:call[src:name[src:name='" +
                                        varName + "'][src:name='" + methodName + "']]" + argumentCounterString + "]\">";
                                id0 =   "<annotation>@<name>CallBehavior</name><argument_list>(" +
                                        "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">\"" +
                                        varName + "-" + id + "\"</literal></expr></argument>, " +
                                        "<argument><expr><name>behavior</name> <operator>=</operator> <literal type=\"string\">\"" +
                                        behavior + "\"</literal></expr></argument>)</argument_list></annotation>" +
                                        "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                        "<annotation>@<name>CallBehaviorEnd</name></annotation>\n" +
                                        "</xsl:template>";
                                template1 = "\n<!-- add @CallBehavior for " + varName + "*" + methodName + " block version -->\n";
                                if (varName.equals(""))
                                    template1 = "<xsl:template name=\"methodCall" + i + "block" +
                                            "\" match=\"src:expr_stmt[not(ancestor::src:catch)][src:expr" +
                                            "[count(src:operator[.='+' or .='/' or .='-' or .='*'])=0][" +
                                            "src:call[src:name='" + methodName + "']" + argumentCounterString + "]]\">";
                                else
                                    template1 += "<xsl:template name=\"methodCall" + i + "block" +
                                        "\" match=\"src:expr_stmt[not(ancestor::src:catch)][src:expr" +
                                        "[count(src:operator[.='+' or .='/' or .='-' or .='*'])=0][src:call[src:name[src:name='" +
                                        varName + "'][src:name='" + methodName + "']]" + argumentCounterString + "]]\">";
                                id1 =   "<annotation>@<name>CallBehavior</name><argument_list>(" +
                                        "<argument><expr><name>id</name><operator>=</operator><literal type=\"string\">\"" +
                                        varName + "-" + id + "\"</literal></expr></argument>, " +
                                        "<argument><expr><name>behavior</name> <operator>=</operator> <literal type=\"string\">\"" +
                                        behavior + "\"</literal></expr></argument>)</argument_list></annotation>" +
                                        "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                        "<annotation>@<name>CallBehaviorEnd</name></annotation>\n" +
                                        "</xsl:template>";
                            }
                        }
                        writer.append("\n" + template0 + "\n" + id0 + "\n" + template1 + "\n" + id1);
                    }
                    break;
                case "chain":
                    for (int i =0; i < varNameList.size(); i++){
                        String info, varFirstMethodLine, variableActivityName, activityName;
                        info = varNameList.get(i);
                        varName = info.split("\\*")[0];
                        if (alreadyWrote.contains(info))
                            continue;;
                        alreadyWrote.add(info);
                        if (varName.startsWith("new") && !varName.contains("="))                    //constructor in methods chain
                            varFirstMethodLine = "[src:call[src:name='" + varName.replace("new ", "").replaceAll("\\([^\\)]*\\)", "") + "']]";
                        else
                            if (varName.contains("new") && varName.contains("=")){                  //constructor with object inialization in methods chain
                                String var = varName.split("\\=")[0].trim().replace("(", "");
                                String method = varName.split("\\=")[1].replaceAll("\\s*new\\s+", "").replaceAll("\\([^\\)]*\\){2}", "");
                                varFirstMethodLine = "[src:name='" + var + "'][src:call[src:name='" + method + "']]" +
                                        "[src:call[src:name='" + info.split("\\*")[1] + "']]";
                            }
                        else
                            if (!varName.contains("\""))                // needed if a method is executed by a String
                                varFirstMethodLine = "[src:call[src:name[src:name='" + varName + "'][src:name= '" + info.split("\\*")[1] + "']]]";
                            else
                                varFirstMethodLine = "[src:literal[contains(@type, 'string')][.='&quot;"+ varName.replace("\"", "") + "&quot;']]" +
                                    "[src:call[src:name='" + info.split("\\*")[1] + "']]";
                        behavior = info.split("\\*")[info.split("\\*").length-1];
                        variableActivityName = "\n<xsl:variable name=\"activityName\">\n" +
                                "<xsl:for-each select=\"ancestor::src:constructor/src:parameter_list/src:parameter | " +
                                "ancestor::src:function/src:parameter_list/src:parameter\">\n" +
                                "<xsl:value-of select =\"src:decl/src:type/src:name\"/><xsl:text>-</xsl:text>\n" +
                                "</xsl:for-each></xsl:variable>";
                        activityName = "<xsl:value-of select=\"concat(ancestor::src:class/src:name, '.', " +
                                "ancestor::src:function/src:name, ancestor::src:constructor/src:name, '.', " +
                                "ancestor::src:function/src:type/src:name, ancestor::src:constructor/src:name, '[', " +
                                "$activityName, ']')\"/>";
                        List<String> methodsChain = Arrays.asList(info.split("\\*")).subList(1, info.split("\\*").length-1);
                        id = "{";
                        for (int j=0; j<methodsChain.size(); j++) {
                            String method = methodsChain.get(j);
                            id += "<literal type=\"string\">\"";
                            id += "<xsl:value-of select=\"concat('" + method + "', count(preceding::src:name[.='" +
                                    method + "']), '_', '" + j + "')\"/>\"</literal>,";
                        }
                        id = id.substring(0, id.length()-1) + "}";
                        template0 = "\n<!-- add @Chain for " + info.replace(behavior, "") + " expr version -->\n";
                        template0 += "<xsl:template name=\"methodsChain" + i + "expr" +
                                "\" match=\"src:expr[count(src:call)>1 and count(src:operator[.='+' or .='/' or .='-' or .='*'])=0]" +
                                "[(ancestor::src:argument_list) or" +
                                "(ancestor::src:init) and not(ancestor::src:catch)]"
                                + varFirstMethodLine;
                        for (String methodInChain : methodsChain.subList(1, methodsChain.size()))
                            template0 += "[src:call[src:name='" + methodInChain + "']]";
                        template0 += "\">";
                        template0 += variableActivityName;
                        id0 =   "<annotation>@<name>Chain</name><argument_list>(" +
                                "<argument><expr><name>ad_id</name><operator>=</operator><literal type=\"string\">\"" +
                                activityName + "\"</literal></expr></argument>, " +
                                "<argument><expr><name>element_id</name><operator>=</operator>" +
                                id + "</expr></argument>, " +
                                "<argument><expr><name>element_behavior</name><operator>=</operator>" +
                                behavior + "</expr></argument>)</argument_list></annotation>" +
                                "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                "<annotation>@<name>ChainEnd</name></annotation>\n" +
                                "</xsl:template>";
                        template1 = "\n<!-- add @Chain for " + info.replace(behavior, "") + " block version -->\n";
                        template1 += "<xsl:template name=\"methodsChain" + i + "block" +
                                "\" match=\"src:expr_stmt[not(ancestor::src:catch)][src:expr[count(src:call)>1]" +
                                 varFirstMethodLine;
                        for (String methodInChain : methodsChain.subList(1, methodsChain.size()))
                            template1 += "[src:call[src:name='" + methodInChain + "']]";
                        template1 += "]\">";
                        template1 += variableActivityName;
                        id1 =   "<annotation>@<name>Chain</name><argument_list>(" +
                                "<argument><expr><name>ad_id</name><operator>=</operator><literal type=\"string\">\"" +
                                activityName + "\"</literal></expr></argument>, " +
                                "<argument><expr><name>element_id</name><operator>=</operator>" +
                                id + "</expr></argument>, " +
                                "<argument><expr><name>element_behavior</name><operator>=</operator>" +
                                behavior + "</expr></argument>)</argument_list></annotation>" +
                                "\n<xsl:copy><xsl:apply-templates select=\"@*|node()\"/></xsl:copy>\n" +
                                "<annotation>@<name>ChainEnd</name></annotation>\n" +
                                "</xsl:template>";

                        writer.append("\n" + template0 + "\n" + id0 + "\n" + template1 + "\n" + id1);
                    }
                    break;
                default: break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



    // this method iterates through methodsAndConstructors and add constructor call to varMethodList
    // if constructor is in activityDiagramMethods map
    private void addConstructor(String fileName, List<String> varMethodList) {
        String clazz, methodSig, method;
        for (String key : methodsAndConstructors.keySet().stream()
                .filter(fileLine -> fileLine.contains(fileName)).collect(Collectors.toList())) {
            for (String classMethodMapAspect : (List<String>) methodsAndConstructors.get(key)){
                clazz = classMethodMapAspect.split("\\*")[0];
                methodSig = classMethodMapAspect.split("\\*")[1];
                if (methodSig.contains(clazz) && activityDiagramMethods.containsKey(clazz)
                        && activityDiagramMethods.get(clazz).contains(methodSig)) {
                    method = methodSig.substring(methodSig.lastIndexOf(".") + 1);
                    if (!varMethodList.contains("new*"+ method + "*" + clazz + methodSig))
                        varMethodList.add("new*"+ method + "*" + clazz);
                }

            }
        }
    }


}
