package it.unimi.reverser.dsl.aspects;

import it.unimi.reverser.dsl.AspectListener;
import it.unimi.reverser.dsl.util.Util;
import org.aspectj.lang.reflect.ConstructorSignature;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com
 * Aspect to retrieve local variable name when a method invocation is caught
 */
public aspect ActivityDiagramAspect {
    private static AspectListener listener;

    public pointcut methodCall(): call(* *(..)) && !within(ActivityDiagramAspect);
    public pointcut constructorCall(): call(*.new(..)) && !within(ActivityDiagramAspect);

    before(): methodCall(){
        String pathToFile, clazz;
        pathToFile = thisJoinPoint.getSourceLocation().getWithinType().toString().replace("class ", "").replace(".", "/") + ".java";
        Method m = ((MethodSignature) thisJoinPoint.getSignature()).getMethod();
        if (thisJoinPoint.getTarget() != null)
            clazz = thisJoinPoint.getTarget().getClass().getCanonicalName();
        else
            clazz = m.getDeclaringClass().getCanonicalName();
        listener.addMethod(clazz, Util.methodToString(m),
                pathToFile + "*" + thisJoinPoint.getSourceLocation().getLine());

    }

    before(): constructorCall(){
        String pathToFile, clazz;
        pathToFile = thisJoinPoint.getSourceLocation().getWithinType().toString().replace("class ", "").replace(".", "/") + ".java";
        Constructor c = ((ConstructorSignature) thisJoinPoint.getSignature()).getConstructor();
        clazz = c.getClass().getCanonicalName();
        listener.addMethod(clazz, c.toGenericString(),
                pathToFile + "*" + thisJoinPoint.getSourceLocation().getLine());

    }

    public static void setContext(AspectListener aspectListener){
        listener = aspectListener;
        System.out.println("[ActivityDiagramAspect]Listener set.");
    }

}
