package it.unimi.reverser.dsl.util;

import it.unimi.reverser.dsl.AspectListener;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com
 * Class containing methodsAndConstructors used by core classes
 */

public class Util {

    // method retrieving standard output and error from executing process p
    public static void printOutputAndError(String command, Process p) {
        BufferedReader stdInput, stdError;
        String s;

        stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

        try {
            // read the output from the command
            if ((s = stdInput.readLine()) != null){
                System.out.println(command + " output:\n" + s);
                while ((s = stdInput.readLine()) != null)
                    System.out.println(s);
            }

            // read the error output from the command
            stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            if ((s = stdError.readLine()) != null) {
                if (command.contains("uncrustify"))
                    System.out.println(command + " msg:\n" + s);
                else
                    System.out.println(command + " error msg:\n" + s);
                while ((s = stdError.readLine()) != null)
                    System.out.println(s);
            }
        } catch (IOException e){
            e.printStackTrace();
        }

    }

    // method retrieving absolute path even if the one passed as argument is relative
    public static String toAbsolutePath(String maybeRelative) {
        Path path = Paths.get(maybeRelative);
        Path effectivePath = path;
        if (!path.isAbsolute()) {
            Path base = Paths.get("");
            effectivePath = base.resolve(path).toAbsolutePath();
        }
        return effectivePath.normalize().toString();
    }

    public static String[] copyOfRange(String[] array,int start,int end){
        String[] ret=new String[end-start];
        for(int i=0;i<end-start;i++)
            ret[i]=array[start+i];
        return ret;
    }

    public static <K, V> void addToMap(K key, V value, Map<K, List<V>> map) {
        if (!map.keySet().contains(key))
            map.put(key, new ArrayList());
        List values = map.get(key);
        values.add(value);
    }

    public static void addToMapNoRepetition(String key, Object value, Map<String, List<String>> map) {
        if (! map.containsKey(key))
            map.put(key, new ArrayList());
        List values = map.get(key);
        if (!values.contains(value))
            values.add(value);
    }

    public static void printMap(String prefix, String mapName, Map map) {
        if (map.size() == 0)
            System.out.println("["+ prefix + "]Map " + mapName + " is empty.");
        else
            for (Object key : map.keySet())
                System.out.println("["+ prefix + "]Added to " + mapName + " map: (" + key + ", " + map.get(key) + ").");
    }


    // method that looks for the class within the method with signature methodSig is declared.
    // it starts looking for that method from class clazz and then in its superclass
    public static String findClassWithDeclaredMethod(AspectListener aspectListener, String className, String methodSig) {
        String decMethod;
        if (!aspectListener.isInResources(className))
            return null;
        for (Method m : aspectListener.getClass(className).getDeclaredMethods()) {
            decMethod = methodToString(m);
            if (methodSig.equals(decMethod))
                return className;
        }
        String pathToClass;
        pathToClass = aspectListener.getClass(className).getSuperclass().getName();
        if (!aspectListener.isInResources(pathToClass))
            return null;
        else return findClassWithDeclaredMethod(aspectListener,
                aspectListener.getClass(className).getSuperclass().getName(), methodSig);
    }

    // method that parses method information retrieved by reflection
    public static String methodToString(Method m){
        String method = m.getReturnType().getCanonicalName() + " " + m.getName() + "(";
        if (m.getParameterTypes().length != 0) {
            for (Class p : m.getParameterTypes())
                method += p.getCanonicalName() + ",";
            method = method.substring(0, method.length() - 1) + ")";
        } else method +=")";
        return method;
    }

    public static void createDir(String path) {
        if (Files.notExists(Paths.get(path))){
            try{
                Process p = Runtime.getRuntime().exec("mkdir -p " + path);
                Util.printOutputAndError("mkdir", p);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // looks for constructor implementation in order to know if it is the default one constructor or not
    public static boolean isDefaultConstructor(Constructor c, String pathToFile) {
        try {
            String cons = "public " + c.toString().substring(c.toString().lastIndexOf(".")+1);
            List<String> lines = Files.lines(Paths.get(pathToFile.replace(".class", ".java"))).collect(Collectors.toList());
            for (String line : lines)
                if (line.contains(cons))
                    return false;
            return true;
        } catch (IOException e) {
            System.out.println("[Util]File " + pathToFile.replace(".class", ".java") + " not found.");
            e.printStackTrace();
        }
        return false;
    }

    // this receives a file with absolute path. In this path there is a series of file with same name followed by
    // a number. This finds the max number that follows filename
    public static String[] getLastName(String file) {
        if (Files.notExists(new File(file).toPath())){
            System.out.println("[Util]No file " + file + " found, so it has been created.");
            return new String[]{file, file.substring(0, file.indexOf('.')) + "*" + 0 + file.indexOf('.')};
        }
        String[] result = new String[2];
        File path = new File(file.substring(0, file.lastIndexOf('/')));
        Pattern numberPat = Pattern.compile("\\*(\\d+)");
        int maxCounter = -1, currentCounter;
        for (File f : path.listFiles())
            if (f.getName().replaceAll("\\*\\d+", "").
                    equals(file.substring(file.lastIndexOf("/") + 1).replaceAll("\\*\\d+", ""))) {
                Matcher numberMatcher = numberPat.matcher(f.getName());
                if (numberMatcher.find()) {
                    currentCounter = Integer.parseInt(numberMatcher.group(1));
                    if (currentCounter > maxCounter)
                        maxCounter = currentCounter;
                }
            }
        String fileName = file.substring(file.lastIndexOf('/')+1);
        result[0] = (maxCounter == -1) ? file : path + "/" + fileName.replaceAll("(\\*\\d+)?\\.xml", "*" + maxCounter + ".xml");
        result[1] = (maxCounter == -1) ? file.replace(".xml", "*0.xml") : path + result[0].substring(result[0].lastIndexOf('/')).
                replace("*" + maxCounter, "*" + (maxCounter+1));
        System.out.println("[Util]Last version of file " + file + " is " + result[0]);
        return result;
    }

    public static CharSequence removeApexesSibling(String node) {
        return node.replaceAll("\"[^\\]]+\\]+\\[following-sibling::*\\[(\\d)+\\]\\[[.]*\"", "");
    }

    // method that transforms source file to destination file through srcml
    public static void srcmlConversion(String prefix, String source, String destination) {
        try {
            Process p = Runtime.getRuntime().exec("srcml " + source + " -o " + destination);
            Util.printOutputAndError("srcml", p);
            System.out.println("[" + prefix
                    + "]File " + source + " converted to " + destination);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("[" + prefix + "]Error in transformation to " + source + " to " + destination);
            System.exit(0);
        }
    }

    // method that transforms source file to destination file through srcml and xsltTransformation
    public static void xsltTransformation(String prefix, String xsltTransformation,
                                          String source, String destination) {
        try {
            Process p = Runtime.getRuntime().exec("srcml --xslt " + xsltTransformation + " "
                    + source + " -o " +  destination);
            Util.printOutputAndError("srcml", p);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("[" + prefix +"]Error with srcml in transformation " + xsltTransformation + " applied to " + destination);
            System.exit(0);
        }
        System.out.println("[" + prefix +"]Xslt transformation " + xsltTransformation
                + " applied to " + source + ", obtaining: " + destination);
    }

    // method that beautifies source file to destination file through uncrustify and uncrustifyConf configuration file
    public static void uncrustify(String prefix, String uncrustifyConf,
                                  String source, String destination) {
        try{
            Process p = Runtime.getRuntime().exec("uncrustify -c " + uncrustifyConf + " -f " +
                    source + " -o " + destination);
            Util.printOutputAndError("uncrustify", p);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("[" + prefix + "]Error with uncrustify beautifying file " + source);
            System.exit(0);
        }
        System.out.println("[" + prefix+ "]Java file " + source + " beautified to " + destination);
    }

    public static void writeFile(String prefix, String fileName, String content) {
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileName), "utf-8"));
            writer.write(content);
            writer.close();
            System.out.println("[" + prefix + "]File " + fileName + " written.");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("[" + prefix + "]Unable to find file " + fileName);
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("[" + prefix + "]Unable to write to file " + fileName);
            System.exit(0);
        }
    }

    public static Stream<String> readFile(String prefix, String destination) {
        try {
            Stream<String> lines = Files.lines(Paths.get(destination));
            return lines;
        } catch (IOException e) {
            System.out.println("["+ prefix + "]Unable to read " + destination + " content.");
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    // extracts value of pair with key from list pairList
    public static String getPairFromList(String key, List<Pair> pairList) {
        for (Pair pair : pairList)
            if (pair.getKey().equals(key))
                return (String) pair.getValue();
        return null;
    }
}
