package it.unimi.reverser.dsl.util;

import com.github.javaparser.Range;
import com.github.javaparser.ast.expr.*;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import it.unimi.reverser.dsl.AspectListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Bianca del Carretto - delcarrettobianca@gmail.com on 28/02/17.
 * This class uses javaparser in order to extract all methodsAndConstructors chains and single invocation, populating then,
 * corresponding maps in AspectListener
 */
public class MethodsChainingExtractor extends VoidVisitorAdapter{
    private AspectListener aspectListener;
    private String fileName;

    public MethodsChainingExtractor(String fileName, AspectListener aspectListener) {
        this.aspectListener = aspectListener;
        this.fileName = fileName;
    }

    @Override
    public void visit(MethodCallExpr methodCall, Object arg) {
        String key, value;
        key = fileName + "*" + methodCall.getRange().get().begin.line;
        boolean alreadyInChains = false;
        if (methodCall.getScope().isPresent()) {
            if (methodCall.getScope().get() instanceof EnclosedExpr &&
                    ((EnclosedExpr) methodCall.getScope().get()).getInner().get() instanceof ObjectCreationExpr) {
                ObjectCreationExpr objCreation = (ObjectCreationExpr) ((EnclosedExpr) methodCall.getScope().get()).getInner().get();
                visit(objCreation, objCreation.getArguments());
                int beginIndex = ((EnclosedExpr) methodCall.getScope().get()).getInner().get().getRange().get().begin.column + 2;
                value = beginIndex + "*" + methodCall.getRange().get().end.column + "*" +
                        ((EnclosedExpr) methodCall.getScope().get()).getInner().get() + "*";
                for (String single : aspectListener.singleMethodInvocations.get(key))
                    if (single.startsWith(String.valueOf(beginIndex + "*"))) {
                        value += single.split("\\*")[2] + "/" + methodCall.getName() + "@" +
                                methodCall.getRange().get().end.column;
                        aspectListener.singleMethodInvocations.get(key).remove(single);
                        break;
                    }

                if (!aspectListener.chainMethods.containsKey(key)) {
                    List<String> methodsInLine = new ArrayList<>();
                    aspectListener.chainMethods.put(key, methodsInLine);
                }
                List<String> chains = aspectListener.chainMethods.get(key);
                chains.add(value);

            }
            else if (methodCall.getScope().get() instanceof EnclosedExpr &&
                    ((EnclosedExpr) methodCall.getScope().get()).getInner().get() instanceof AssignExpr){
                AssignExpr assignExpr = (AssignExpr) ((EnclosedExpr) methodCall.getScope().get()).getInner().get();
                value = assignExpr.getValue().getRange().get().begin.column + "*" +
                        assignExpr.getValue() + "*" + assignExpr.getValue() + "@" + assignExpr.getRange().get().end.column;

                if (!aspectListener.singleMethodInvocations.containsKey(key)) {
                    List<String> singleInLine = new ArrayList<>();
                    aspectListener.singleMethodInvocations.put(key, singleInLine);
                }
                List<String> singles = aspectListener.singleMethodInvocations.get(key);
                singles.add(value);
                singles.add(methodCall.getScope().get().getRange().get().end.column + "*" +
                        methodCall.getScope().get() + "*" + methodCall.getName() + "@" +
                        methodCall.getRange().get().end.column);
            }

            else if (methodCall.getScope().get() instanceof MethodCallExpr) {
                super.visit(methodCall, arg);

                Expression scopeExpr = methodCall.getScope().get();
                while (scopeExpr instanceof MethodCallExpr) {
                    if (!((MethodCallExpr) scopeExpr).getScope().isPresent())
                        break;
                    scopeExpr = ((MethodCallExpr) scopeExpr).getScope().get();
                }

                int beginIndex = scopeExpr.getRange().isPresent() ? scopeExpr.getRange().get().end.column : 0;
                value = beginIndex + "*" + methodCall.getRange().get().end.column + "*" + scopeExpr + "*";

                if (!aspectListener.chainMethods.containsKey(key)) {
                    List<String> methodsInLine = new ArrayList<>();
                    aspectListener.chainMethods.put(key, methodsInLine);
                }
                List<String> chains = aspectListener.chainMethods.get(key);

                for (String methodsChain : chains) {
                    if (methodsChain.startsWith(String.valueOf(beginIndex + "*"))) {
                        value += methodsChain.split("\\*")[3] + "/" + methodCall.getName() + "@" +
                                methodCall.getRange().get().end.column;
                        chains.remove(methodsChain);
                        alreadyInChains = true;
                        break;
                    }
                }

                if (!alreadyInChains)
                    if (aspectListener.singleMethodInvocations.containsKey(key))
                        for (String single : aspectListener.singleMethodInvocations.get(key))
                            if (single.startsWith(String.valueOf(beginIndex + "*"))) {
                                value += single.split("\\*")[2] + "/" + methodCall.getName() + "@" +
                                        methodCall.getRange().get().end.column;
                                aspectListener.singleMethodInvocations.get(key).remove(single);
                                break;
                            }

                chains.add(value);

            }
            else {
                value = methodCall.getScope().get().getRange().get().end.column + "*" +
                        methodCall.getScope().get() + "*" + methodCall.getName() + "@" + methodCall.getRange().get().end.column;

                if (!aspectListener.singleMethodInvocations.containsKey(key)) {
                    List<String> singleInLine = new ArrayList<>();
                    aspectListener.singleMethodInvocations.put(key, singleInLine);
                }
                List<String> singles = aspectListener.singleMethodInvocations.get(key);
                for (String single : singles) {
                    if (single.startsWith(String.valueOf(methodCall.getScope().get().getRange().get().end.column))) {
                        singles.remove(single);
                        break;
                    }
                }
                singles.add(value);
            }
        }
        else{
            value = methodCall.getRange().get().end.column + "**" +
                    methodCall.getName() + "@" + methodCall.getRange().get().end.column;

            if (!aspectListener.singleMethodInvocations.containsKey(key)) {
                List<String> singleInLine = new ArrayList<>();
                aspectListener.singleMethodInvocations.put(key, singleInLine);
            }
            List<String> singles = aspectListener.singleMethodInvocations.get(key);
            for (String single : singles) {
                if (single.startsWith(String.valueOf(methodCall.getRange().get().end.column))) {
                    singles.remove(single);
                    break;
                }
            }
            singles.add(value);
        }

        List<Expression> args = methodCall.getArguments();
        if (args != null)
            handleExpressions(args);
    }

    @Override
    public void visit(ObjectCreationExpr constrCall, Object arg){
        String key, value;
        key = fileName + "*" + constrCall.getRange().get().begin.line;
        if (!aspectListener.singleMethodInvocations.containsKey(key)){
            List<String> singleInLine = new ArrayList<>();
            aspectListener.singleMethodInvocations.put(key, singleInLine);
        }
        value = (constrCall.getRange().get().begin.column+2) + "*" +
                constrCall + "*" + constrCall + "@" + (constrCall.getRange().get().end.column+1);
        List<String> singles = aspectListener.singleMethodInvocations.get(key);
        singles.add(value);

        List<Expression> args = constrCall.getArguments();
        if (args != null)
            handleExpressions(args);

    }

    private void handleExpressions(List<Expression> expressions) {
        for (Expression expr : expressions)
        {
            if (expr instanceof MethodCallExpr)
                visit((MethodCallExpr) expr, null);
            else
                if (expr instanceof  EnclosedExpr)
                    visit((EnclosedExpr) expr, null);
                else if (expr instanceof BinaryExpr) {
                BinaryExpr binExpr = (BinaryExpr)expr;
                handleExpressions(Arrays.asList(binExpr.getLeft(), binExpr.getRight()));
            }
        }
    }


}
